all:
	$(MAKE) -C src all
	if [ ! -d bin ]; then mkdir bin; fi
	if [ ! -d bin/compiler ]; then mkdir bin/compiler; fi
	if [ ! -d bin/compiler/ast ]; then mkdir bin/compiler/ast; fi
	if [ ! -d bin/compiler/parser ]; then mkdir bin/compiler/parser; fi
	cp src/*.class bin/
	cp buildinfunctions.txt bin/
	$(MAKE) -C src clean
clean:
	$(MAKE) -C src clean
	-rm -rf bin
