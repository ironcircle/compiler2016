/**
 * Created by 章 on 2016/4/7.
 */
public class Exitcode {
    int code = 0;
    String name;

    boolean check(){
        if (code == 0)
            return true;
        outp();
        return false;
    }
    void outp(){
        if (code == 0)
            System.out.printf("Compile Successful...\n");
        if (code == 1)
            System.out.printf("Compile Error....Rename(" + name + ")\n");
        if (code == 2)
            System.out.printf("Compile Error....Missing Type/Function name:" + name + "\n");
        if (code == 3)
            System.out.printf("Compile Error....Type Mismatch " + name + "\n");
        if (code == 4)
            System.out.printf("Compile Error....Wrong JumpStatement" + name + "\n");
        if (code == 5)
            System.out.printf("Compile Error....Wrong FunctionCall" + name + "\n");
        if (code == 6)
            System.out.printf("Compile Error....Wrong Class member" + name + "\n");
        if (code == 7)
            System.out.printf("Compile Error....Lvalue Error" + name + "\n");
        if (code == 8)
            System.out.printf("Compile Error....New Error" + name + "\n");
        if (code == 9)
            System.out.printf("Compile Erro....main Function Error" + name + "\n");
    }
}
