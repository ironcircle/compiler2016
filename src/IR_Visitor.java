/**
 * Created by 章 on 2016/5/3.
 */
public abstract class IR_Visitor {
    void visit(IR e) {
        e.accept(this);
    }

    abstract void visit(IR_Block pointer);

    abstract void visit(IR_Label pointer);

    abstract void visit(IR_Binary pointer);

    abstract void visit(IR_Call pointer);

    abstract void visit(IR_Func pointer);

    abstract void visit(IR_Jump pointer);

    abstract void visit(IR_Branch pointer);

    abstract void visit(IR_Return pointer);

    abstract void visit(IR_Constant pointer);

    abstract void visit(IR_Argument pointer);

    abstract void visit(IR_String pointer);
}
