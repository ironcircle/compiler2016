/**
 * Created by 章 on 2016/4/7.
 */
public abstract class Visitor {
    void visit(IC_AST e) {
        e.accept(this);
    }

    abstract void visit(Program pointer);

    abstract void visit(Functional_Declaration pointer);

    abstract void visit(Class_Declaration pointer);

    abstract void visit(Selection_Statement pointer);

    abstract void visit(Block_Statement pointer);

    abstract void visit(For_Statement pointer);

    abstract void visit(While_Statement pointer);

    abstract void visit(Jump_Statement pointer);

    abstract void visit(Return_Statement pointer);

    abstract void visit(Argument pointer);

    abstract void visit(Declaration_Expression pointer);

    abstract void visit(Suffix_Expression pointer);

    abstract void visit(Functional_Expression pointer);

    abstract void visit(Subscript_Expression pointer);

    abstract void visit(Member_Expression pointer);

    abstract void visit(Prefix_Expression pointer);

    abstract void visit(Binary_Expression pointer);

    abstract void visit(Assign_Expression pointer);

    abstract void visit(Int_Expression pointer);

    abstract void visit(Bool_Expression pointer);

    abstract void visit(String_Expression pointer);

    abstract void visit(Empty_Expression pointer);

    abstract void visit(Null_Expression pointer);

    abstract void visit(Type pointer);

    abstract void visit(Identifier_Expression pointer);
}
