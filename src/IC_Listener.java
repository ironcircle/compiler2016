import org.antlr.v4.runtime.tree.ParseTreeProperty;

import java.lang.reflect.Member;
import java.util.ArrayList;
        import java.util.Iterator;
        import java.util.List;
        import java.util.function.Function;

/**
 * Created by 章 on 2016/4/3.
 */
public class IC_Listener extends ComplierBaseListener {
    public ParseTreeProperty<Object> values = new ParseTreeProperty<>();
    public Program root;

    @Override
    public void exitProgram(ComplierParser.ProgramContext ctx) {
        Program cur = new Program();
        values.put(ctx, cur);
        root = cur;
        for(int i = 0; i < ctx.program_detail().size(); i++)
            cur.add((IC_AST)values.get(ctx.program_detail(i)));
        //   super.exitProgram(ctx);
    }

    @Override
    public void exitProgram_detail(ComplierParser.Program_detailContext ctx) {
        if (ctx.declaration() != null) {
            values.put(ctx,values.get(ctx.declaration()));
        }
        else if  (ctx.class_declaration() != null) {
            values.put(ctx,values.get(ctx.class_declaration()));
        }
        else values.put(ctx,values.get(ctx.function_definition()));
        //super.exitProgram_detail(ctx);
    }



    //Class Declaration & Declaration & Function definition
    @Override
    public void exitClass_declaration(ComplierParser.Class_declarationContext ctx) {
        Class_Declaration cur = new Class_Declaration();
        values.put(ctx,cur);
        cur.name = ctx.Identifier().getText();
    //    System.out.printf(ctx.noneinit_declaration().size()+"");
        for (int i = 0; i <ctx.noneinit_declaration().size(); i++){
        //    System.out.printf(((List<Argument>)values.get(ctx.noneinit_declaration(i))).size() + "");
            cur.add((List<Argument>)values.get(ctx.noneinit_declaration(i)));
                    //  System.out.printf(cur.member.size()+" ");
        }
        //super.exitClass_declaration(ctx);
    }

    @Override
    public void exitDeclaration(ComplierParser.DeclarationContext ctx) {
        Declaration_Expression cur = new Declaration_Expression();
        values.put(ctx,cur);
        if(ctx.init_Identifiers() != null) cur.member = (ArrayList<Argument>)values.get(ctx.init_Identifiers());
        for (int i = 0; i < cur.member.size(); i++)
            cur.member.get(i).changetype((Type)values.get(ctx.type_specifier()));
        //super.exitDeclaration(ctx);
    }

    @Override
    public void exitFunction_definition(ComplierParser.Function_definitionContext ctx) {
        Functional_Declaration cur = new Functional_Declaration();
        values.put(ctx,cur);
        cur.type = (Type)values.get(ctx.type_specifier());
        cur.name = ctx.Identifier().getText();
        if (ctx.parameters() != null) cur.parameter = (ArrayList<Argument>) values.get(ctx.parameters());
        cur.context = (Block_Statement)values.get(ctx.compound_statement());
        //super.exitFunction_definition(ctx);
    }



    // Parameter Declaration .....
    @Override
    public void exitNoneinit_declaration(ComplierParser.Noneinit_declarationContext ctx) {
        List<Argument> cur = new ArrayList<Argument>();
        if (ctx.noneinit_identifiers() != null) cur = (ArrayList<Argument>)values.get(ctx.noneinit_identifiers());
        values.put(ctx,cur);
     //   System.out.printf(cur.size()+" ");
        for (int i = 0; i < cur.size(); i++)
            cur.get(i).changetype((Type)values.get(ctx.type_specifier()));
        //super.exitNoneinit_declaration(ctx);
    }

    @Override
    public void exitNoneinit_identifiers(ComplierParser.Noneinit_identifiersContext ctx) {
        //super.exitIdentifiers(ctx);
        List<Argument> cur = new ArrayList<Argument>();
        values.put(ctx,cur);
        for (int i = 0; i < ctx.Identifier().size();i++) {
            Argument now = new Argument();
            now.name = ctx.Identifier(i).getText();
            now.have_initial_value = false;
            cur.add(now);
        }
    }

    @Override
    public void exitParameters(ComplierParser.ParametersContext ctx) {
        List<Argument> cur = new ArrayList<Argument>();
        values.put(ctx,cur);
        for (int i = 0; i < ctx.parameter().size(); i++)
            cur.add((Argument)values.get(ctx.parameter(i)));
        //super.exitParameters(ctx);
    }

    @Override
    public void exitParameter(ComplierParser.ParameterContext ctx) {
        Argument cur = new Argument();
        values.put(ctx,cur);
        cur.type = (Type)values.get(ctx.type_specifier());
        cur.name = ctx.Identifier().getText();
        cur.have_initial_value = false;
        //super.exitParameter(ctx);
    }

    @Override
    public void exitInit_Identifiers(ComplierParser.Init_IdentifiersContext ctx) {
        List<Argument> cur = new ArrayList<Argument>();
        values.put(ctx,cur);
        for (int i = 0 ; i < ctx.init_Identifier().size(); i++)
            cur.add((Argument)values.get(ctx.init_Identifier(i)));
        //super.exitInit_Identifiers(ctx);
    }

    @Override
    public void exitInit_Identifier(ComplierParser.Init_IdentifierContext ctx) {
        Argument cur = new Argument();
        values.put(ctx,cur);
        cur.name = ctx.Identifier().getText();
        if (ctx.expression() != null) {
            cur.have_initial_value = true;
            cur.initial_value = (Expression) values.get(ctx.expression());
        } else cur.have_initial_value = false;
        //super.exitInit_Identifier(ctx);
    }

    @Override
    public void exitType_specifier(ComplierParser.Type_specifierContext ctx) {
        Type cur = new Type();
        values.put(ctx, cur);
        cur.name = ((Identifier_Expression)values.get(ctx.type())).name;
        cur.true_size = ctx.pair_Bracket().size();
        cur.used_size= 0;
        //super.exitType_specifier(ctx);
    }

    @Override
    public void exitType(ComplierParser.TypeContext ctx) {
        Identifier_Expression cur = new Identifier_Expression();
        values.put(ctx, cur);
        if (ctx.Identifier() != null)
            cur.name = ctx.Identifier().getText();
        else
            cur.name = ctx.t.getText();
        //super.exitType(ctx);
    }

    @Override
    public void exitStatement(ComplierParser.StatementContext ctx) {
        if (ctx.expression_statement() != null)
            values.put(ctx,values.get(ctx.expression_statement()));
        else if (ctx.compound_statement() != null)
            values.put(ctx,values.get(ctx.compound_statement()));
        else if (ctx.selection_statement() != null)
            values.put(ctx,values.get(ctx.selection_statement()));
        else if (ctx.iteration_statement() != null)
            values.put(ctx,values.get(ctx.iteration_statement()));
        else if (ctx.jump_statement() != null)
            values.put(ctx, values.get(ctx.jump_statement()));
        else
            values.put(ctx,values.get(ctx.declaration()));
        //super.exitStatement(ctx);
    }

    @Override
    public void exitSimple_statement(ComplierParser.Simple_statementContext ctx) {
        if (ctx.expression_statement() != null)
            values.put(ctx,values.get(ctx.expression_statement()));
        else
            values.put(ctx,values.get(ctx.declaration()));
        //super.exitSimple_statement(ctx);
    }

    @Override
    public void exitExpression_statement(ComplierParser.Expression_statementContext ctx) {
        if (ctx.expression() != null)
            values.put(ctx,values.get(ctx.expression()));
        else
            values.put(ctx,new Empty_Expression());
        //super.exitExpression_statement(ctx);
    }

    @Override
    public void exitCompound_statement(ComplierParser.Compound_statementContext ctx) {
        Block_Statement cur = new Block_Statement();
        values.put(ctx, cur);
        for (int i = 0; i < ctx.statement().size(); i++)
            cur.list.add((Statement)values.get(ctx.statement(i)));
        //super.exitCompound_statement(ctx);
    }

    @Override
    public void exitSelection_statement(ComplierParser.Selection_statementContext ctx) {
        Selection_Statement cur = new Selection_Statement();
        values.put(ctx, cur);
        cur.condition = (Expression) values.get(ctx.expression());
        cur.then_context = (Statement)values.get(ctx.a);
        if (ctx.b != null)
            cur.else_context = (Statement)values.get(ctx.b);
        else
            cur.else_context = new Empty_Expression();
        //super.exitSelection_statement(ctx);
    }

    @Override
    public void exitWhile_statement(ComplierParser.While_statementContext ctx) {
        While_Statement cur = new While_Statement();
        values.put(ctx, cur);
        cur.condition = (Expression) values.get(ctx.expression());
        cur.context = (Statement) values.get(ctx.statement());
        //super.exitWhile_statement(ctx);
    }

    @Override
    public void exitFor_statement(ComplierParser.For_statementContext ctx) {
        For_Statement cur = new For_Statement();
        values.put(ctx, cur);
        cur.initial = (Statement)values.get(ctx.simple_statement());
        cur.condition = (Statement)values.get(ctx.expression_statement());
        if (ctx.expression() != null)
            cur.update = (Statement)values.get(ctx.expression());
        else
            cur.update = new Empty_Expression();
        cur.context = (Statement)values.get(ctx.statement());
        //super.exitFor_statement(ctx);
    }

    @Override
    public void exitJump_statement(ComplierParser.Jump_statementContext ctx) {
        if (ctx.t.getText().equals("return")){
            Return_Statement cur = new Return_Statement();
            values.put(ctx, cur);
            if (ctx.expression() != null)
                cur.context = (Expression)values.get(ctx.expression());
            else
                cur.context = new Empty_Expression();
        }
        else {
            Jump_Statement cur = new Jump_Statement();
            values.put(ctx, cur);
            cur.meaning = ctx.t.getText();
        }
        //super.exitJump_statement(ctx);
    }



    //expression
    @Override
    public void exitBracket_expression(ComplierParser.Bracket_expressionContext ctx) {
        values.put(ctx,values.get(ctx.expression()));
        //super.exitBracket_expression(ctx);
    }

    @Override
    public void exitSuffix_expression(ComplierParser.Suffix_expressionContext ctx) {
        Suffix_Expression cur = new Suffix_Expression();
        values.put(ctx, cur);
        cur.oper = ctx.t.getText();
        cur.expression = (Expression) values.get(ctx.expression());
        //super.exitSuffix_expression(ctx);
    }

    @Override
    public void exitFunctional_expression(ComplierParser.Functional_expressionContext ctx) {


        if (values.get(ctx.expression()) instanceof Identifier_Expression){
            String str = ((Identifier_Expression) values.get(ctx.expression())).name;
            if ((str.equals("println") || str.equals("print")) && values.get(ctx.arguments().expression(0)) instanceof Binary_Expression){
                Block_Statement st = new Block_Statement();
                Binary_Expression Q = (Binary_Expression)values.get(ctx.arguments().expression(0));
                while (Q.oper.equals("+")){
                    Functional_Expression cur = new Functional_Expression();
                    Identifier_Expression t = new Identifier_Expression();
                    t.name = "print";
                    cur.expression = t;
                    cur.argument.add(Q.right_exp);
                    st.list.add(0, cur);
                    if (Q.left_exp instanceof Binary_Expression) Q = (Binary_Expression) Q.left_exp; else break;
                }
                Functional_Expression cur = new Functional_Expression();
                Identifier_Expression t = new Identifier_Expression();
                t.name = "print";
                cur.expression = t;
                cur.argument.add(Q.left_exp);
                st.list.add(0, cur);
                if (str.equals("println")) ((Identifier_Expression)((Functional_Expression)st.list.get(st.list.size() - 1)).expression).name = "println";
                values.put(ctx,st);
                return;
            }
        }
        Functional_Expression cur = new Functional_Expression();
        values.put(ctx, cur);
        if(ctx.arguments() != null) cur.argument = (ArrayList<Expression>)values.get(ctx.arguments());
        cur.expression = (Expression) values.get(ctx.expression());
        //super.exitFunctional_expression(ctx);
    }

    @Override
    public void exitSubscript_expression(ComplierParser.Subscript_expressionContext ctx) {
        Subscript_Expression cur = new Subscript_Expression();
        values.put(ctx, cur);
        cur.expression = (Expression)values.get(ctx.expression());
        cur.true_size = ctx.bracket_clude_expression().size();
    //    System.out.printf(cur.true_size+"");
        cur.used_size = cur.true_size;
        cur.argument = new ArrayList<Expression>();
        for (int i = 0; i < ctx.bracket_clude_expression().size(); i++) {
            cur.argument.add((Expression) values.get(ctx.bracket_clude_expression(i)));
            if (cur.argument.get(i) instanceof Empty_Expression) cur.used_size--;
        }

        //super.exitSubscript_expression(ctx);
    }

    @Override
    public void exitMember_expression(ComplierParser.Member_expressionContext ctx) {
        Member_Expression cur = new Member_Expression();
        values.put(ctx, cur);
        cur.expression = (Expression)values.get(ctx.a);
        cur.member =  (Expression)values.get(ctx.b);
        //super.exitMember_expression(ctx);
    }

    @Override
    public void exitPrefix_expression(ComplierParser.Prefix_expressionContext ctx) {
        Prefix_Expression cur = new Prefix_Expression();
        values.put(ctx, cur);
        cur.oper = ctx.t.getText();
        cur.expression = (Expression)values.get(ctx.expression());
        //super.exitPrefix_expression(ctx);
    }

    @Override
    public void exitBinary_expression(ComplierParser.Binary_expressionContext ctx) {
        Binary_Expression cur = new Binary_Expression();
        cur.oper = ctx.t.getText();
        cur.left_exp = (Expression) values.get(ctx.l);
        cur.right_exp = (Expression) values.get(ctx.r);
        values.put(ctx, cur);
        //super.exitBinary_expression(ctx);
    }

    @Override
    public void exitAssign_expression(ComplierParser.Assign_expressionContext ctx) {
        Assign_Expression cur = new Assign_Expression();
        values.put(ctx,cur);
        cur.left_exp = (Expression) values.get(ctx.l);
        cur.right_exp = (Expression) values.get(ctx.r);
        //super.exitAssign_expression(ctx);
    }

    @Override
    public void exitArguments(ComplierParser.ArgumentsContext ctx) {
        List<Expression> cur = new ArrayList<Expression>();
        values.put(ctx, cur);
        for (int i = 0; i < ctx.expression().size();i++)
            cur.add((Expression)values.get(ctx.expression(i)));
        //super.exitArguments(ctx);
    }

    @Override
    public void exitValue_expression(ComplierParser.Value_expressionContext ctx) {
        values.put(ctx,values.get(ctx.value()));
        //super.exitValue_expression(ctx);
    }

    @Override
    public void exitInt_value(ComplierParser.Int_valueContext ctx) {
        Int_Expression cur = new Int_Expression();
        values.put(ctx, cur);
        cur.value =  Integer.parseInt(ctx.Int().getText());
        if (ctx.Sign() != null && ctx.Sign().getText().equals("-")) cur.value = -cur.value;
        //super.exitInt_value(ctx);
    }

    @Override
    public void exitBool_value(ComplierParser.Bool_valueContext ctx) {
        Bool_Expression cur = new Bool_Expression();
        values.put(ctx, cur);;
        if (ctx.Bool().getText().equals("true"))
            cur.value = true;
        else
            cur.value = false;
        //super.exitBool_value(ctx);
    }

    @Override
    public void exitString_value(ComplierParser.String_valueContext ctx) {
        String_Expression cur = new String_Expression();
        values.put(ctx, cur);
        cur.value = ctx.String().getText();
        //super.exitString_value(ctx);
    }

    @Override
    public void exitType_value(ComplierParser.Type_valueContext ctx) {
        values.put(ctx, values.get(ctx.type()));
        //super.exitType_value(ctx);
    }

    @Override
    public void exitIdentifier_value(ComplierParser.Identifier_valueContext ctx) {
        Identifier_Expression cur = new Identifier_Expression();
        values.put(ctx,cur);
        cur.name = ctx.Identifier().getText();
        //super.exitIdentifier_value(ctx);
    }

    @Override
    public void exitBracket_clude_expression(ComplierParser.Bracket_clude_expressionContext ctx) {
        if (ctx.expression() != null)
            values.put(ctx, (Expression)values.get(ctx.expression()));
        else
            values.put(ctx, new Empty_Expression());
        //super.exitBracket_clude_expression(ctx);
    }

    @Override
    public void exitPair_Bracket(ComplierParser.Pair_BracketContext ctx) {
        super.exitPair_Bracket(ctx);
    }

    @Override
    public void exitNull_value(ComplierParser.Null_valueContext ctx) {
        values.put(ctx, new Null_Expression());
        //super.exitNull_value(ctx);
    }
}