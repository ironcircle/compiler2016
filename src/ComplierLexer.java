// Generated from Complier.g4 by ANTLR 4.5
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ComplierLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, Line_Comment=6, Kw_Bool=7, Kw_Int=8, 
		Kw_String=9, Kw_Null=10, Kw_Void=11, Kw_If=12, Kw_For=13, Kw_While=14, 
		Kw_Break=15, Kw_Continue=16, Kw_Return=17, Kw_New=18, Kw_Class=19, Bool=20, 
		Add=21, Sub=22, Mul=23, Div=24, Mod=25, Less=26, Great=27, Leq=28, Geq=29, 
		Equal=30, NotEqual=31, And=32, Or=33, Tilde=34, Caret=35, LShift=36, RShift=37, 
		Not=38, AndAnd=39, OrOr=40, Assignment=41, SelfInc=42, SelfDec=43, Dot=44, 
		LParen=45, RParen=46, LBracket=47, RBracket=48, LBrace=49, RBrace=50, 
		Int=51, Identifier=52, Sign=53, String=54, ESC=55, WS=56;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "Line_Comment", "Kw_Bool", "Kw_Int", 
		"Kw_String", "Kw_Null", "Kw_Void", "Kw_True", "Kw_False", "Kw_If", "Kw_For", 
		"Kw_While", "Kw_Break", "Kw_Continue", "Kw_Return", "Kw_New", "Kw_Class", 
		"Bool", "Add", "Sub", "Mul", "Div", "Mod", "Less", "Great", "Leq", "Geq", 
		"Equal", "NotEqual", "And", "Or", "Tilde", "Caret", "LShift", "RShift", 
		"Not", "AndAnd", "OrOr", "Assignment", "SelfInc", "SelfDec", "Dot", "LParen", 
		"RParen", "LBracket", "RBracket", "LBrace", "RBrace", "Letter", "Digit", 
		"Int", "Identifier", "Sign", "String", "ESC", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "';'", "','", "'else'", "'while'", "'continue'", null, "'bool'", 
		"'int'", "'string'", "'null'", "'void'", "'if'", "'for'", "'While'", "'break'", 
		"'Continue'", "'return'", "'new'", "'class'", null, "'+'", "'-'", "'*'", 
		"'/'", "'%'", "'<'", "'>'", "'<='", "'>='", "'=='", "'!='", "'&'", "'|'", 
		"'~'", "'^'", "'<<'", "'>>'", "'!'", "'&&'", "'||'", "'='", "'++'", "'--'", 
		"'.'", "'('", "')'", "'['", "']'", "'{'", "'}'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, "Line_Comment", "Kw_Bool", "Kw_Int", 
		"Kw_String", "Kw_Null", "Kw_Void", "Kw_If", "Kw_For", "Kw_While", "Kw_Break", 
		"Kw_Continue", "Kw_Return", "Kw_New", "Kw_Class", "Bool", "Add", "Sub", 
		"Mul", "Div", "Mod", "Less", "Great", "Leq", "Geq", "Equal", "NotEqual", 
		"And", "Or", "Tilde", "Caret", "LShift", "RShift", "Not", "AndAnd", "OrOr", 
		"Assignment", "SelfInc", "SelfDec", "Dot", "LParen", "RParen", "LBracket", 
		"RBracket", "LBrace", "RBrace", "Int", "Identifier", "Sign", "String", 
		"ESC", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public ComplierLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Complier.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2:\u0167\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\3\2\3\2\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\7\7\u0098\n\7\f\7\16\7\u009b"+
		"\13\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\r\3\r"+
		"\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\20\3\20\3"+
		"\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3"+
		"\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3"+
		"\24\3\24\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3"+
		"\27\3\27\3\27\3\27\3\27\3\27\3\27\5\27\u00fc\n\27\3\30\3\30\3\31\3\31"+
		"\3\32\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3\37\3 \3"+
		" \3 \3!\3!\3!\3\"\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3\'\3\'\3\'\3(\3(\3"+
		"(\3)\3)\3*\3*\3*\3+\3+\3+\3,\3,\3-\3-\3-\3.\3.\3.\3/\3/\3\60\3\60\3\61"+
		"\3\61\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65\3\66\3\66\3\67\3\67\38\6"+
		"8\u0149\n8\r8\168\u014a\39\39\39\79\u0150\n9\f9\169\u0153\139\3:\3:\3"+
		";\3;\3;\7;\u015a\n;\f;\16;\u015d\13;\3;\3;\3<\3<\3<\3=\3=\3=\3=\4\u0099"+
		"\u015b\2>\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\2\33\2"+
		"\35\16\37\17!\20#\21%\22\'\23)\24+\25-\26/\27\61\30\63\31\65\32\67\33"+
		"9\34;\35=\36?\37A C!E\"G#I$K%M&O\'Q(S)U*W+Y,[-]._/a\60c\61e\62g\63i\64"+
		"k\2m\2o\65q\66s\67u8w9y:\3\2\7\5\2C\\aac|\3\2\62;\4\2--//\b\2$$^^ddpp"+
		"ttvv\5\2\13\f\17\17\"\"\u0169\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t"+
		"\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2"+
		"\2\2\25\3\2\2\2\2\27\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3"+
		"\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2"+
		"\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2"+
		";\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3"+
		"\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2"+
		"\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2"+
		"a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2o\3\2\2\2\2q\3"+
		"\2\2\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\3{\3\2\2\2\5}\3\2\2"+
		"\2\7\177\3\2\2\2\t\u0084\3\2\2\2\13\u008a\3\2\2\2\r\u0093\3\2\2\2\17\u00a0"+
		"\3\2\2\2\21\u00a5\3\2\2\2\23\u00a9\3\2\2\2\25\u00b0\3\2\2\2\27\u00b5\3"+
		"\2\2\2\31\u00ba\3\2\2\2\33\u00bf\3\2\2\2\35\u00c5\3\2\2\2\37\u00c8\3\2"+
		"\2\2!\u00cc\3\2\2\2#\u00d2\3\2\2\2%\u00d8\3\2\2\2\'\u00e1\3\2\2\2)\u00e8"+
		"\3\2\2\2+\u00ec\3\2\2\2-\u00fb\3\2\2\2/\u00fd\3\2\2\2\61\u00ff\3\2\2\2"+
		"\63\u0101\3\2\2\2\65\u0103\3\2\2\2\67\u0105\3\2\2\29\u0107\3\2\2\2;\u0109"+
		"\3\2\2\2=\u010b\3\2\2\2?\u010e\3\2\2\2A\u0111\3\2\2\2C\u0114\3\2\2\2E"+
		"\u0117\3\2\2\2G\u0119\3\2\2\2I\u011b\3\2\2\2K\u011d\3\2\2\2M\u011f\3\2"+
		"\2\2O\u0122\3\2\2\2Q\u0125\3\2\2\2S\u0127\3\2\2\2U\u012a\3\2\2\2W\u012d"+
		"\3\2\2\2Y\u012f\3\2\2\2[\u0132\3\2\2\2]\u0135\3\2\2\2_\u0137\3\2\2\2a"+
		"\u0139\3\2\2\2c\u013b\3\2\2\2e\u013d\3\2\2\2g\u013f\3\2\2\2i\u0141\3\2"+
		"\2\2k\u0143\3\2\2\2m\u0145\3\2\2\2o\u0148\3\2\2\2q\u014c\3\2\2\2s\u0154"+
		"\3\2\2\2u\u0156\3\2\2\2w\u0160\3\2\2\2y\u0163\3\2\2\2{|\7=\2\2|\4\3\2"+
		"\2\2}~\7.\2\2~\6\3\2\2\2\177\u0080\7g\2\2\u0080\u0081\7n\2\2\u0081\u0082"+
		"\7u\2\2\u0082\u0083\7g\2\2\u0083\b\3\2\2\2\u0084\u0085\7y\2\2\u0085\u0086"+
		"\7j\2\2\u0086\u0087\7k\2\2\u0087\u0088\7n\2\2\u0088\u0089\7g\2\2\u0089"+
		"\n\3\2\2\2\u008a\u008b\7e\2\2\u008b\u008c\7q\2\2\u008c\u008d\7p\2\2\u008d"+
		"\u008e\7v\2\2\u008e\u008f\7k\2\2\u008f\u0090\7p\2\2\u0090\u0091\7w\2\2"+
		"\u0091\u0092\7g\2\2\u0092\f\3\2\2\2\u0093\u0094\7\61\2\2\u0094\u0095\7"+
		"\61\2\2\u0095\u0099\3\2\2\2\u0096\u0098\13\2\2\2\u0097\u0096\3\2\2\2\u0098"+
		"\u009b\3\2\2\2\u0099\u009a\3\2\2\2\u0099\u0097\3\2\2\2\u009a\u009c\3\2"+
		"\2\2\u009b\u0099\3\2\2\2\u009c\u009d\7\f\2\2\u009d\u009e\3\2\2\2\u009e"+
		"\u009f\b\7\2\2\u009f\16\3\2\2\2\u00a0\u00a1\7d\2\2\u00a1\u00a2\7q\2\2"+
		"\u00a2\u00a3\7q\2\2\u00a3\u00a4\7n\2\2\u00a4\20\3\2\2\2\u00a5\u00a6\7"+
		"k\2\2\u00a6\u00a7\7p\2\2\u00a7\u00a8\7v\2\2\u00a8\22\3\2\2\2\u00a9\u00aa"+
		"\7u\2\2\u00aa\u00ab\7v\2\2\u00ab\u00ac\7t\2\2\u00ac\u00ad\7k\2\2\u00ad"+
		"\u00ae\7p\2\2\u00ae\u00af\7i\2\2\u00af\24\3\2\2\2\u00b0\u00b1\7p\2\2\u00b1"+
		"\u00b2\7w\2\2\u00b2\u00b3\7n\2\2\u00b3\u00b4\7n\2\2\u00b4\26\3\2\2\2\u00b5"+
		"\u00b6\7x\2\2\u00b6\u00b7\7q\2\2\u00b7\u00b8\7k\2\2\u00b8\u00b9\7f\2\2"+
		"\u00b9\30\3\2\2\2\u00ba\u00bb\7v\2\2\u00bb\u00bc\7t\2\2\u00bc\u00bd\7"+
		"w\2\2\u00bd\u00be\7g\2\2\u00be\32\3\2\2\2\u00bf\u00c0\7h\2\2\u00c0\u00c1"+
		"\7c\2\2\u00c1\u00c2\7n\2\2\u00c2\u00c3\7u\2\2\u00c3\u00c4\7g\2\2\u00c4"+
		"\34\3\2\2\2\u00c5\u00c6\7k\2\2\u00c6\u00c7\7h\2\2\u00c7\36\3\2\2\2\u00c8"+
		"\u00c9\7h\2\2\u00c9\u00ca\7q\2\2\u00ca\u00cb\7t\2\2\u00cb \3\2\2\2\u00cc"+
		"\u00cd\7Y\2\2\u00cd\u00ce\7j\2\2\u00ce\u00cf\7k\2\2\u00cf\u00d0\7n\2\2"+
		"\u00d0\u00d1\7g\2\2\u00d1\"\3\2\2\2\u00d2\u00d3\7d\2\2\u00d3\u00d4\7t"+
		"\2\2\u00d4\u00d5\7g\2\2\u00d5\u00d6\7c\2\2\u00d6\u00d7\7m\2\2\u00d7$\3"+
		"\2\2\2\u00d8\u00d9\7E\2\2\u00d9\u00da\7q\2\2\u00da\u00db\7p\2\2\u00db"+
		"\u00dc\7v\2\2\u00dc\u00dd\7k\2\2\u00dd\u00de\7p\2\2\u00de\u00df\7w\2\2"+
		"\u00df\u00e0\7g\2\2\u00e0&\3\2\2\2\u00e1\u00e2\7t\2\2\u00e2\u00e3\7g\2"+
		"\2\u00e3\u00e4\7v\2\2\u00e4\u00e5\7w\2\2\u00e5\u00e6\7t\2\2\u00e6\u00e7"+
		"\7p\2\2\u00e7(\3\2\2\2\u00e8\u00e9\7p\2\2\u00e9\u00ea\7g\2\2\u00ea\u00eb"+
		"\7y\2\2\u00eb*\3\2\2\2\u00ec\u00ed\7e\2\2\u00ed\u00ee\7n\2\2\u00ee\u00ef"+
		"\7c\2\2\u00ef\u00f0\7u\2\2\u00f0\u00f1\7u\2\2\u00f1,\3\2\2\2\u00f2\u00f3"+
		"\7v\2\2\u00f3\u00f4\7t\2\2\u00f4\u00f5\7w\2\2\u00f5\u00fc\7g\2\2\u00f6"+
		"\u00f7\7h\2\2\u00f7\u00f8\7c\2\2\u00f8\u00f9\7n\2\2\u00f9\u00fa\7u\2\2"+
		"\u00fa\u00fc\7g\2\2\u00fb\u00f2\3\2\2\2\u00fb\u00f6\3\2\2\2\u00fc.\3\2"+
		"\2\2\u00fd\u00fe\7-\2\2\u00fe\60\3\2\2\2\u00ff\u0100\7/\2\2\u0100\62\3"+
		"\2\2\2\u0101\u0102\7,\2\2\u0102\64\3\2\2\2\u0103\u0104\7\61\2\2\u0104"+
		"\66\3\2\2\2\u0105\u0106\7\'\2\2\u01068\3\2\2\2\u0107\u0108\7>\2\2\u0108"+
		":\3\2\2\2\u0109\u010a\7@\2\2\u010a<\3\2\2\2\u010b\u010c\7>\2\2\u010c\u010d"+
		"\7?\2\2\u010d>\3\2\2\2\u010e\u010f\7@\2\2\u010f\u0110\7?\2\2\u0110@\3"+
		"\2\2\2\u0111\u0112\7?\2\2\u0112\u0113\7?\2\2\u0113B\3\2\2\2\u0114\u0115"+
		"\7#\2\2\u0115\u0116\7?\2\2\u0116D\3\2\2\2\u0117\u0118\7(\2\2\u0118F\3"+
		"\2\2\2\u0119\u011a\7~\2\2\u011aH\3\2\2\2\u011b\u011c\7\u0080\2\2\u011c"+
		"J\3\2\2\2\u011d\u011e\7`\2\2\u011eL\3\2\2\2\u011f\u0120\7>\2\2\u0120\u0121"+
		"\7>\2\2\u0121N\3\2\2\2\u0122\u0123\7@\2\2\u0123\u0124\7@\2\2\u0124P\3"+
		"\2\2\2\u0125\u0126\7#\2\2\u0126R\3\2\2\2\u0127\u0128\7(\2\2\u0128\u0129"+
		"\7(\2\2\u0129T\3\2\2\2\u012a\u012b\7~\2\2\u012b\u012c\7~\2\2\u012cV\3"+
		"\2\2\2\u012d\u012e\7?\2\2\u012eX\3\2\2\2\u012f\u0130\7-\2\2\u0130\u0131"+
		"\7-\2\2\u0131Z\3\2\2\2\u0132\u0133\7/\2\2\u0133\u0134\7/\2\2\u0134\\\3"+
		"\2\2\2\u0135\u0136\7\60\2\2\u0136^\3\2\2\2\u0137\u0138\7*\2\2\u0138`\3"+
		"\2\2\2\u0139\u013a\7+\2\2\u013ab\3\2\2\2\u013b\u013c\7]\2\2\u013cd\3\2"+
		"\2\2\u013d\u013e\7_\2\2\u013ef\3\2\2\2\u013f\u0140\7}\2\2\u0140h\3\2\2"+
		"\2\u0141\u0142\7\177\2\2\u0142j\3\2\2\2\u0143\u0144\t\2\2\2\u0144l\3\2"+
		"\2\2\u0145\u0146\t\3\2\2\u0146n\3\2\2\2\u0147\u0149\5m\67\2\u0148\u0147"+
		"\3\2\2\2\u0149\u014a\3\2\2\2\u014a\u0148\3\2\2\2\u014a\u014b\3\2\2\2\u014b"+
		"p\3\2\2\2\u014c\u0151\5k\66\2\u014d\u0150\5k\66\2\u014e\u0150\5m\67\2"+
		"\u014f\u014d\3\2\2\2\u014f\u014e\3\2\2\2\u0150\u0153\3\2\2\2\u0151\u014f"+
		"\3\2\2\2\u0151\u0152\3\2\2\2\u0152r\3\2\2\2\u0153\u0151\3\2\2\2\u0154"+
		"\u0155\t\4\2\2\u0155t\3\2\2\2\u0156\u015b\7$\2\2\u0157\u015a\5w<\2\u0158"+
		"\u015a\13\2\2\2\u0159\u0157\3\2\2\2\u0159\u0158\3\2\2\2\u015a\u015d\3"+
		"\2\2\2\u015b\u015c\3\2\2\2\u015b\u0159\3\2\2\2\u015c\u015e\3\2\2\2\u015d"+
		"\u015b\3\2\2\2\u015e\u015f\7$\2\2\u015fv\3\2\2\2\u0160\u0161\7^\2\2\u0161"+
		"\u0162\t\5\2\2\u0162x\3\2\2\2\u0163\u0164\t\6\2\2\u0164\u0165\3\2\2\2"+
		"\u0165\u0166\b=\2\2\u0166z\3\2\2\2\n\2\u0099\u00fb\u014a\u014f\u0151\u0159"+
		"\u015b\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}