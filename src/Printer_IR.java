import java.util.HashMap;

/**
 * Created by 章 on 2016/5/3.
 */
public class Printer_IR extends IR_Visitor {
    HashMap<String, IR_Func> func_map;
    void init(HashMap<String, IR_Func> t){
        func_map = t;
    }
    @Override
    void visit(IR_Block pointer) {
        System.out.printf("%%" + pointer.name.name + ":\n");
        for (int i = 0 ; i < pointer.expression.size(); i++)
            visit(pointer.expression.get(i));
        if (pointer.out != null)
            visit(pointer.out);
        else
            System.out.printf("Function end label\n");

        System.out.printf("\n");
    }

    @Override
    void visit(IR_Label pointer) {
        System.out.printf("%%" + pointer.name);
    }

    @Override
    void visit(IR_Binary pointer) {
        if (pointer instanceof IR_Call)
        {
            visit((IR_Call)pointer);
            return;
        }
        if (pointer.operator.equals("=")) {
            visit(pointer.target);
            System.out.printf(" = " );
            visit(pointer.t1);
            System.out.printf("\n");
        }
        else if(pointer.operator.equals("neg") || pointer.operator.equals("move")|| pointer.operator.equals("alloc") || pointer.operator.equals("link") || pointer.operator.equals("load")){
            visit(pointer.target);
            if (pointer.operator.equals("%"))
                System.out.printf(" = %% ");
            else
            System.out.printf(" = "+ pointer.operator + " ");
            visit(pointer.t1);
            System.out.printf("\n");
        }
        else if (pointer.operator.equals("store")) {
            System.out.printf("store ");
            visit(pointer.target);
            System.out.printf(" ");
            visit(pointer.t1);
            System.out.printf("\n");
        }else
        {
            visit(pointer.target);
            System.out.printf(" = " + pointer.operator + " ");
            visit(pointer.t1);
            System.out.printf(" ");
            visit(pointer.t2);
            System.out.printf("\n");
        }
    }

    @Override
    void visit(IR_Func pointer) {
        System.out.printf(pointer.name);
        for (int i = 0 ; i < pointer.parameter.size(); i++) {
            System.out.printf(" ");
            visit(pointer.parameter.get(i));
        }
        System.out.printf("\n");
    }

    @Override
    void visit(IR_Jump pointer) {
        System.out.printf("jump ");
        visit(pointer.position);
        System.out.printf("\n");
    }

    @Override
    void visit(IR_Branch pointer) {
        System.out.printf("branch ");
        visit(pointer.condition);
        System.out.printf(" ");
        visit(pointer.true_position);
        System.out.printf(" ");
        visit(pointer.false_position);
        System.out.printf("\n");
    }

    @Override
    void visit(IR_Return pointer) {
        System.out.printf("return ");
        visit(pointer.value);
        System.out.printf("\n");
    }

    @Override
    void visit(IR_Constant pointer) {
        System.out.printf(pointer.value + "");
    }

    @Override
    void visit(IR_Argument pointer) {
        System.out.printf("&" + pointer.name);
    }

    @Override
    void visit(IR_Call pointer) {
        visit(pointer.target);
        System.out.printf(" = call " + pointer.operator);
        for (int i = 0; i < pointer.parameter.size(); i++){
            System.out.printf(" ");
            visit(pointer.parameter.get(i));
        }
        if (func_map.get(pointer.operator) != null)
            System.out.printf("(Jump " + func_map.get(pointer.operator).start.name + ")");
        else
            System.out.printf("(Jump " + pointer.operator + ")");
        System.out.printf("\n");
    }

    @Override
    void visit(IR_String pointer) {
        System.out.printf(pointer.name);
    }
}
