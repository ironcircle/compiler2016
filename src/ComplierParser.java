// Generated from Complier.g4 by ANTLR 4.5
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ComplierParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, Line_Comment=6, Kw_Bool=7, Kw_Int=8, 
		Kw_String=9, Kw_Null=10, Kw_Void=11, Kw_If=12, Kw_For=13, Kw_While=14, 
		Kw_Break=15, Kw_Continue=16, Kw_Return=17, Kw_New=18, Kw_Class=19, Bool=20, 
		Add=21, Sub=22, Mul=23, Div=24, Mod=25, Less=26, Great=27, Leq=28, Geq=29, 
		Equal=30, NotEqual=31, And=32, Or=33, Tilde=34, Caret=35, LShift=36, RShift=37, 
		Not=38, AndAnd=39, OrOr=40, Assignment=41, SelfInc=42, SelfDec=43, Dot=44, 
		LParen=45, RParen=46, LBracket=47, RBracket=48, LBrace=49, RBrace=50, 
		Int=51, Identifier=52, Sign=53, String=54, ESC=55, WS=56;
	public static final int
		RULE_type = 0, RULE_program = 1, RULE_program_detail = 2, RULE_declaration = 3, 
		RULE_class_declaration = 4, RULE_noneinit_declaration = 5, RULE_noneinit_identifiers = 6, 
		RULE_function_definition = 7, RULE_parameters = 8, RULE_parameter = 9, 
		RULE_init_Identifiers = 10, RULE_init_Identifier = 11, RULE_type_specifier = 12, 
		RULE_pair_Bracket = 13, RULE_statement = 14, RULE_simple_statement = 15, 
		RULE_expression_statement = 16, RULE_compound_statement = 17, RULE_selection_statement = 18, 
		RULE_iteration_statement = 19, RULE_jump_statement = 20, RULE_expression = 21, 
		RULE_arguments = 22, RULE_value = 23, RULE_bracket_clude_expression = 24;
	public static final String[] ruleNames = {
		"type", "program", "program_detail", "declaration", "class_declaration", 
		"noneinit_declaration", "noneinit_identifiers", "function_definition", 
		"parameters", "parameter", "init_Identifiers", "init_Identifier", "type_specifier", 
		"pair_Bracket", "statement", "simple_statement", "expression_statement", 
		"compound_statement", "selection_statement", "iteration_statement", "jump_statement", 
		"expression", "arguments", "value", "bracket_clude_expression"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "';'", "','", "'else'", "'while'", "'continue'", null, "'bool'", 
		"'int'", "'string'", "'null'", "'void'", "'if'", "'for'", "'While'", "'break'", 
		"'Continue'", "'return'", "'new'", "'class'", null, "'+'", "'-'", "'*'", 
		"'/'", "'%'", "'<'", "'>'", "'<='", "'>='", "'=='", "'!='", "'&'", "'|'", 
		"'~'", "'^'", "'<<'", "'>>'", "'!'", "'&&'", "'||'", "'='", "'++'", "'--'", 
		"'.'", "'('", "')'", "'['", "']'", "'{'", "'}'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, "Line_Comment", "Kw_Bool", "Kw_Int", 
		"Kw_String", "Kw_Null", "Kw_Void", "Kw_If", "Kw_For", "Kw_While", "Kw_Break", 
		"Kw_Continue", "Kw_Return", "Kw_New", "Kw_Class", "Bool", "Add", "Sub", 
		"Mul", "Div", "Mod", "Less", "Great", "Leq", "Geq", "Equal", "NotEqual", 
		"And", "Or", "Tilde", "Caret", "LShift", "RShift", "Not", "AndAnd", "OrOr", 
		"Assignment", "SelfInc", "SelfDec", "Dot", "LParen", "RParen", "LBracket", 
		"RBracket", "LBrace", "RBrace", "Int", "Identifier", "Sign", "String", 
		"ESC", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Complier.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ComplierParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class TypeContext extends ParserRuleContext {
		public Token t;
		public TerminalNode Identifier() { return getToken(ComplierParser.Identifier, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_type);
		try {
			setState(55);
			switch (_input.LA(1)) {
			case Kw_Bool:
				enterOuterAlt(_localctx, 1);
				{
				setState(50);
				((TypeContext)_localctx).t = match(Kw_Bool);
				}
				break;
			case Kw_Int:
				enterOuterAlt(_localctx, 2);
				{
				setState(51);
				((TypeContext)_localctx).t = match(Kw_Int);
				}
				break;
			case Kw_String:
				enterOuterAlt(_localctx, 3);
				{
				setState(52);
				((TypeContext)_localctx).t = match(Kw_String);
				}
				break;
			case Kw_Void:
				enterOuterAlt(_localctx, 4);
				{
				setState(53);
				((TypeContext)_localctx).t = match(Kw_Void);
				}
				break;
			case Identifier:
				enterOuterAlt(_localctx, 5);
				{
				setState(54);
				match(Identifier);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramContext extends ParserRuleContext {
		public List<Program_detailContext> program_detail() {
			return getRuleContexts(Program_detailContext.class);
		}
		public Program_detailContext program_detail(int i) {
			return getRuleContext(Program_detailContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(60);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Kw_Bool) | (1L << Kw_Int) | (1L << Kw_String) | (1L << Kw_Void) | (1L << Kw_Class) | (1L << Identifier))) != 0)) {
				{
				{
				setState(57);
				program_detail();
				}
				}
				setState(62);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Program_detailContext extends ParserRuleContext {
		public DeclarationContext declaration() {
			return getRuleContext(DeclarationContext.class,0);
		}
		public Class_declarationContext class_declaration() {
			return getRuleContext(Class_declarationContext.class,0);
		}
		public Function_definitionContext function_definition() {
			return getRuleContext(Function_definitionContext.class,0);
		}
		public Program_detailContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program_detail; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterProgram_detail(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitProgram_detail(this);
		}
	}

	public final Program_detailContext program_detail() throws RecognitionException {
		Program_detailContext _localctx = new Program_detailContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_program_detail);
		try {
			setState(66);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(63);
				declaration();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(64);
				class_declaration();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(65);
				function_definition();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public Type_specifierContext type_specifier() {
			return getRuleContext(Type_specifierContext.class,0);
		}
		public Init_IdentifiersContext init_Identifiers() {
			return getRuleContext(Init_IdentifiersContext.class,0);
		}
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitDeclaration(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_declaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68);
			type_specifier();
			setState(69);
			init_Identifiers();
			setState(70);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_declarationContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(ComplierParser.Identifier, 0); }
		public List<Noneinit_declarationContext> noneinit_declaration() {
			return getRuleContexts(Noneinit_declarationContext.class);
		}
		public Noneinit_declarationContext noneinit_declaration(int i) {
			return getRuleContext(Noneinit_declarationContext.class,i);
		}
		public Class_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterClass_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitClass_declaration(this);
		}
	}

	public final Class_declarationContext class_declaration() throws RecognitionException {
		Class_declarationContext _localctx = new Class_declarationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_class_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(72);
			match(Kw_Class);
			setState(73);
			match(Identifier);
			setState(74);
			match(LBrace);
			setState(78);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Kw_Bool) | (1L << Kw_Int) | (1L << Kw_String) | (1L << Kw_Void) | (1L << Identifier))) != 0)) {
				{
				{
				setState(75);
				noneinit_declaration();
				}
				}
				setState(80);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(81);
			match(RBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Noneinit_declarationContext extends ParserRuleContext {
		public Type_specifierContext type_specifier() {
			return getRuleContext(Type_specifierContext.class,0);
		}
		public Noneinit_identifiersContext noneinit_identifiers() {
			return getRuleContext(Noneinit_identifiersContext.class,0);
		}
		public Noneinit_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_noneinit_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterNoneinit_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitNoneinit_declaration(this);
		}
	}

	public final Noneinit_declarationContext noneinit_declaration() throws RecognitionException {
		Noneinit_declarationContext _localctx = new Noneinit_declarationContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_noneinit_declaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(83);
			type_specifier();
			setState(84);
			noneinit_identifiers();
			setState(85);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Noneinit_identifiersContext extends ParserRuleContext {
		public List<TerminalNode> Identifier() { return getTokens(ComplierParser.Identifier); }
		public TerminalNode Identifier(int i) {
			return getToken(ComplierParser.Identifier, i);
		}
		public Noneinit_identifiersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_noneinit_identifiers; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterNoneinit_identifiers(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitNoneinit_identifiers(this);
		}
	}

	public final Noneinit_identifiersContext noneinit_identifiers() throws RecognitionException {
		Noneinit_identifiersContext _localctx = new Noneinit_identifiersContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_noneinit_identifiers);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			match(Identifier);
			setState(92);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(88);
				match(T__1);
				setState(89);
				match(Identifier);
				}
				}
				setState(94);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_definitionContext extends ParserRuleContext {
		public Type_specifierContext type_specifier() {
			return getRuleContext(Type_specifierContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(ComplierParser.Identifier, 0); }
		public Compound_statementContext compound_statement() {
			return getRuleContext(Compound_statementContext.class,0);
		}
		public ParametersContext parameters() {
			return getRuleContext(ParametersContext.class,0);
		}
		public Function_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterFunction_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitFunction_definition(this);
		}
	}

	public final Function_definitionContext function_definition() throws RecognitionException {
		Function_definitionContext _localctx = new Function_definitionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_function_definition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(95);
			type_specifier();
			setState(96);
			match(Identifier);
			setState(97);
			match(LParen);
			setState(99);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Kw_Bool) | (1L << Kw_Int) | (1L << Kw_String) | (1L << Kw_Void) | (1L << Identifier))) != 0)) {
				{
				setState(98);
				parameters();
				}
			}

			setState(101);
			match(RParen);
			setState(102);
			compound_statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametersContext extends ParserRuleContext {
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public ParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitParameters(this);
		}
	}

	public final ParametersContext parameters() throws RecognitionException {
		ParametersContext _localctx = new ParametersContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_parameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(104);
			parameter();
			setState(109);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(105);
				match(T__1);
				setState(106);
				parameter();
				}
				}
				setState(111);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public Type_specifierContext type_specifier() {
			return getRuleContext(Type_specifierContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(ComplierParser.Identifier, 0); }
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitParameter(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(112);
			type_specifier();
			setState(113);
			match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Init_IdentifiersContext extends ParserRuleContext {
		public List<Init_IdentifierContext> init_Identifier() {
			return getRuleContexts(Init_IdentifierContext.class);
		}
		public Init_IdentifierContext init_Identifier(int i) {
			return getRuleContext(Init_IdentifierContext.class,i);
		}
		public Init_IdentifiersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init_Identifiers; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterInit_Identifiers(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitInit_Identifiers(this);
		}
	}

	public final Init_IdentifiersContext init_Identifiers() throws RecognitionException {
		Init_IdentifiersContext _localctx = new Init_IdentifiersContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_init_Identifiers);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(115);
			init_Identifier();
			setState(120);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(116);
				match(T__1);
				setState(117);
				init_Identifier();
				}
				}
				setState(122);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Init_IdentifierContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(ComplierParser.Identifier, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Init_IdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init_Identifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterInit_Identifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitInit_Identifier(this);
		}
	}

	public final Init_IdentifierContext init_Identifier() throws RecognitionException {
		Init_IdentifierContext _localctx = new Init_IdentifierContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_init_Identifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(123);
			match(Identifier);
			setState(126);
			_la = _input.LA(1);
			if (_la==Assignment) {
				{
				setState(124);
				match(Assignment);
				setState(125);
				expression(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_specifierContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public List<Pair_BracketContext> pair_Bracket() {
			return getRuleContexts(Pair_BracketContext.class);
		}
		public Pair_BracketContext pair_Bracket(int i) {
			return getRuleContext(Pair_BracketContext.class,i);
		}
		public Type_specifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_specifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterType_specifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitType_specifier(this);
		}
	}

	public final Type_specifierContext type_specifier() throws RecognitionException {
		Type_specifierContext _localctx = new Type_specifierContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_type_specifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(128);
			type();
			setState(132);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LBracket) {
				{
				{
				setState(129);
				pair_Bracket();
				}
				}
				setState(134);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pair_BracketContext extends ParserRuleContext {
		public Pair_BracketContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pair_Bracket; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterPair_Bracket(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitPair_Bracket(this);
		}
	}

	public final Pair_BracketContext pair_Bracket() throws RecognitionException {
		Pair_BracketContext _localctx = new Pair_BracketContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_pair_Bracket);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(135);
			match(LBracket);
			setState(136);
			match(RBracket);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public Expression_statementContext expression_statement() {
			return getRuleContext(Expression_statementContext.class,0);
		}
		public Compound_statementContext compound_statement() {
			return getRuleContext(Compound_statementContext.class,0);
		}
		public Selection_statementContext selection_statement() {
			return getRuleContext(Selection_statementContext.class,0);
		}
		public Iteration_statementContext iteration_statement() {
			return getRuleContext(Iteration_statementContext.class,0);
		}
		public Jump_statementContext jump_statement() {
			return getRuleContext(Jump_statementContext.class,0);
		}
		public DeclarationContext declaration() {
			return getRuleContext(DeclarationContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_statement);
		try {
			setState(144);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(138);
				expression_statement();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(139);
				compound_statement();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(140);
				selection_statement();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(141);
				iteration_statement();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(142);
				jump_statement();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(143);
				declaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Simple_statementContext extends ParserRuleContext {
		public Expression_statementContext expression_statement() {
			return getRuleContext(Expression_statementContext.class,0);
		}
		public DeclarationContext declaration() {
			return getRuleContext(DeclarationContext.class,0);
		}
		public Simple_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simple_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterSimple_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitSimple_statement(this);
		}
	}

	public final Simple_statementContext simple_statement() throws RecognitionException {
		Simple_statementContext _localctx = new Simple_statementContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_simple_statement);
		try {
			setState(148);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(146);
				expression_statement();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(147);
				declaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expression_statementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Expression_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterExpression_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitExpression_statement(this);
		}
	}

	public final Expression_statementContext expression_statement() throws RecognitionException {
		Expression_statementContext _localctx = new Expression_statementContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_expression_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(151);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Kw_Bool) | (1L << Kw_Int) | (1L << Kw_String) | (1L << Kw_Null) | (1L << Kw_Void) | (1L << Kw_New) | (1L << Bool) | (1L << Add) | (1L << Sub) | (1L << Tilde) | (1L << Not) | (1L << SelfInc) | (1L << SelfDec) | (1L << LParen) | (1L << Int) | (1L << Identifier) | (1L << Sign) | (1L << String))) != 0)) {
				{
				setState(150);
				expression(0);
				}
			}

			setState(153);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Compound_statementContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public Compound_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compound_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterCompound_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitCompound_statement(this);
		}
	}

	public final Compound_statementContext compound_statement() throws RecognitionException {
		Compound_statementContext _localctx = new Compound_statementContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_compound_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(155);
			match(LBrace);
			setState(159);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__3) | (1L << T__4) | (1L << Kw_Bool) | (1L << Kw_Int) | (1L << Kw_String) | (1L << Kw_Null) | (1L << Kw_Void) | (1L << Kw_If) | (1L << Kw_For) | (1L << Kw_Break) | (1L << Kw_Return) | (1L << Kw_New) | (1L << Bool) | (1L << Add) | (1L << Sub) | (1L << Tilde) | (1L << Not) | (1L << SelfInc) | (1L << SelfDec) | (1L << LParen) | (1L << LBrace) | (1L << Int) | (1L << Identifier) | (1L << Sign) | (1L << String))) != 0)) {
				{
				{
				setState(156);
				statement();
				}
				}
				setState(161);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(162);
			match(RBrace);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Selection_statementContext extends ParserRuleContext {
		public StatementContext a;
		public StatementContext b;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public Selection_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selection_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterSelection_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitSelection_statement(this);
		}
	}

	public final Selection_statementContext selection_statement() throws RecognitionException {
		Selection_statementContext _localctx = new Selection_statementContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_selection_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(164);
			match(Kw_If);
			setState(165);
			match(LParen);
			setState(166);
			expression(0);
			setState(167);
			match(RParen);
			setState(168);
			((Selection_statementContext)_localctx).a = statement();
			setState(171);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				setState(169);
				match(T__2);
				setState(170);
				((Selection_statementContext)_localctx).b = statement();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Iteration_statementContext extends ParserRuleContext {
		public Iteration_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_iteration_statement; }
	 
		public Iteration_statementContext() { }
		public void copyFrom(Iteration_statementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class For_statementContext extends Iteration_statementContext {
		public Simple_statementContext simple_statement() {
			return getRuleContext(Simple_statementContext.class,0);
		}
		public Expression_statementContext expression_statement() {
			return getRuleContext(Expression_statementContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public For_statementContext(Iteration_statementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterFor_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitFor_statement(this);
		}
	}
	public static class While_statementContext extends Iteration_statementContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public While_statementContext(Iteration_statementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterWhile_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitWhile_statement(this);
		}
	}

	public final Iteration_statementContext iteration_statement() throws RecognitionException {
		Iteration_statementContext _localctx = new Iteration_statementContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_iteration_statement);
		int _la;
		try {
			setState(189);
			switch (_input.LA(1)) {
			case T__3:
				_localctx = new While_statementContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(173);
				match(T__3);
				setState(174);
				match(LParen);
				setState(175);
				expression(0);
				setState(176);
				match(RParen);
				setState(177);
				statement();
				}
				break;
			case Kw_For:
				_localctx = new For_statementContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(179);
				match(Kw_For);
				setState(180);
				match(LParen);
				setState(181);
				simple_statement();
				setState(182);
				expression_statement();
				setState(184);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Kw_Bool) | (1L << Kw_Int) | (1L << Kw_String) | (1L << Kw_Null) | (1L << Kw_Void) | (1L << Kw_New) | (1L << Bool) | (1L << Add) | (1L << Sub) | (1L << Tilde) | (1L << Not) | (1L << SelfInc) | (1L << SelfDec) | (1L << LParen) | (1L << Int) | (1L << Identifier) | (1L << Sign) | (1L << String))) != 0)) {
					{
					setState(183);
					expression(0);
					}
				}

				setState(186);
				match(RParen);
				setState(187);
				statement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Jump_statementContext extends ParserRuleContext {
		public Token t;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Jump_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jump_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterJump_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitJump_statement(this);
		}
	}

	public final Jump_statementContext jump_statement() throws RecognitionException {
		Jump_statementContext _localctx = new Jump_statementContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_jump_statement);
		int _la;
		try {
			setState(200);
			switch (_input.LA(1)) {
			case T__4:
				enterOuterAlt(_localctx, 1);
				{
				setState(191);
				((Jump_statementContext)_localctx).t = match(T__4);
				setState(192);
				match(T__0);
				}
				break;
			case Kw_Break:
				enterOuterAlt(_localctx, 2);
				{
				setState(193);
				((Jump_statementContext)_localctx).t = match(Kw_Break);
				setState(194);
				match(T__0);
				}
				break;
			case Kw_Return:
				enterOuterAlt(_localctx, 3);
				{
				setState(195);
				((Jump_statementContext)_localctx).t = match(Kw_Return);
				setState(197);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Kw_Bool) | (1L << Kw_Int) | (1L << Kw_String) | (1L << Kw_Null) | (1L << Kw_Void) | (1L << Kw_New) | (1L << Bool) | (1L << Add) | (1L << Sub) | (1L << Tilde) | (1L << Not) | (1L << SelfInc) | (1L << SelfDec) | (1L << LParen) | (1L << Int) | (1L << Identifier) | (1L << Sign) | (1L << String))) != 0)) {
					{
					setState(196);
					expression(0);
					}
				}

				setState(199);
				match(T__0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Binary_expressionContext extends ExpressionContext {
		public ExpressionContext l;
		public Token t;
		public ExpressionContext r;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public Binary_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterBinary_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitBinary_expression(this);
		}
	}
	public static class Functional_expressionContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ArgumentsContext arguments() {
			return getRuleContext(ArgumentsContext.class,0);
		}
		public Functional_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterFunctional_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitFunctional_expression(this);
		}
	}
	public static class Member_expressionContext extends ExpressionContext {
		public ExpressionContext a;
		public ExpressionContext b;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public Member_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterMember_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitMember_expression(this);
		}
	}
	public static class Value_expressionContext extends ExpressionContext {
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public Value_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterValue_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitValue_expression(this);
		}
	}
	public static class Bracket_expressionContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Bracket_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterBracket_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitBracket_expression(this);
		}
	}
	public static class Suffix_expressionContext extends ExpressionContext {
		public Token t;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Suffix_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterSuffix_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitSuffix_expression(this);
		}
	}
	public static class Subscript_expressionContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<Bracket_clude_expressionContext> bracket_clude_expression() {
			return getRuleContexts(Bracket_clude_expressionContext.class);
		}
		public Bracket_clude_expressionContext bracket_clude_expression(int i) {
			return getRuleContext(Bracket_clude_expressionContext.class,i);
		}
		public Subscript_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterSubscript_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitSubscript_expression(this);
		}
	}
	public static class Prefix_expressionContext extends ExpressionContext {
		public Token t;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Prefix_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterPrefix_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitPrefix_expression(this);
		}
	}
	public static class Assign_expressionContext extends ExpressionContext {
		public ExpressionContext l;
		public Token t;
		public ExpressionContext r;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public Assign_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterAssign_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitAssign_expression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 42;
		enterRecursionRule(_localctx, 42, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(210);
			switch (_input.LA(1)) {
			case Kw_New:
			case Add:
			case Sub:
			case Tilde:
			case Not:
			case SelfInc:
			case SelfDec:
				{
				_localctx = new Prefix_expressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(203);
				((Prefix_expressionContext)_localctx).t = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Kw_New) | (1L << Add) | (1L << Sub) | (1L << Tilde) | (1L << Not) | (1L << SelfInc) | (1L << SelfDec))) != 0)) ) {
					((Prefix_expressionContext)_localctx).t = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				setState(204);
				expression(13);
				}
				break;
			case LParen:
				{
				_localctx = new Bracket_expressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(205);
				match(LParen);
				setState(206);
				expression(0);
				setState(207);
				match(RParen);
				}
				break;
			case Kw_Bool:
			case Kw_Int:
			case Kw_String:
			case Kw_Null:
			case Kw_Void:
			case Bool:
			case Int:
			case Identifier:
			case Sign:
			case String:
				{
				_localctx = new Value_expressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(209);
				value();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(264);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(262);
					switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
					case 1:
						{
						_localctx = new Member_expressionContext(new ExpressionContext(_parentctx, _parentState));
						((Member_expressionContext)_localctx).a = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(212);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(213);
						match(Dot);
						setState(214);
						((Member_expressionContext)_localctx).b = expression(15);
						}
						break;
					case 2:
						{
						_localctx = new Binary_expressionContext(new ExpressionContext(_parentctx, _parentState));
						((Binary_expressionContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(215);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(216);
						((Binary_expressionContext)_localctx).t = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Mul) | (1L << Div) | (1L << Mod))) != 0)) ) {
							((Binary_expressionContext)_localctx).t = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(217);
						((Binary_expressionContext)_localctx).r = expression(13);
						}
						break;
					case 3:
						{
						_localctx = new Binary_expressionContext(new ExpressionContext(_parentctx, _parentState));
						((Binary_expressionContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(218);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(219);
						((Binary_expressionContext)_localctx).t = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==Add || _la==Sub) ) {
							((Binary_expressionContext)_localctx).t = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(220);
						((Binary_expressionContext)_localctx).r = expression(12);
						}
						break;
					case 4:
						{
						_localctx = new Binary_expressionContext(new ExpressionContext(_parentctx, _parentState));
						((Binary_expressionContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(221);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(222);
						((Binary_expressionContext)_localctx).t = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==LShift || _la==RShift) ) {
							((Binary_expressionContext)_localctx).t = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(223);
						((Binary_expressionContext)_localctx).r = expression(11);
						}
						break;
					case 5:
						{
						_localctx = new Binary_expressionContext(new ExpressionContext(_parentctx, _parentState));
						((Binary_expressionContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(224);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(225);
						((Binary_expressionContext)_localctx).t = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Less) | (1L << Great) | (1L << Leq) | (1L << Geq))) != 0)) ) {
							((Binary_expressionContext)_localctx).t = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(226);
						((Binary_expressionContext)_localctx).r = expression(10);
						}
						break;
					case 6:
						{
						_localctx = new Binary_expressionContext(new ExpressionContext(_parentctx, _parentState));
						((Binary_expressionContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(227);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(228);
						((Binary_expressionContext)_localctx).t = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==Equal || _la==NotEqual) ) {
							((Binary_expressionContext)_localctx).t = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						setState(229);
						((Binary_expressionContext)_localctx).r = expression(9);
						}
						break;
					case 7:
						{
						_localctx = new Binary_expressionContext(new ExpressionContext(_parentctx, _parentState));
						((Binary_expressionContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(230);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(231);
						((Binary_expressionContext)_localctx).t = match(And);
						setState(232);
						((Binary_expressionContext)_localctx).r = expression(8);
						}
						break;
					case 8:
						{
						_localctx = new Binary_expressionContext(new ExpressionContext(_parentctx, _parentState));
						((Binary_expressionContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(233);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(234);
						((Binary_expressionContext)_localctx).t = match(Caret);
						setState(235);
						((Binary_expressionContext)_localctx).r = expression(7);
						}
						break;
					case 9:
						{
						_localctx = new Binary_expressionContext(new ExpressionContext(_parentctx, _parentState));
						((Binary_expressionContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(236);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(237);
						((Binary_expressionContext)_localctx).t = match(Or);
						setState(238);
						((Binary_expressionContext)_localctx).r = expression(6);
						}
						break;
					case 10:
						{
						_localctx = new Binary_expressionContext(new ExpressionContext(_parentctx, _parentState));
						((Binary_expressionContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(239);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(240);
						((Binary_expressionContext)_localctx).t = match(AndAnd);
						setState(241);
						((Binary_expressionContext)_localctx).r = expression(4);
						}
						break;
					case 11:
						{
						_localctx = new Binary_expressionContext(new ExpressionContext(_parentctx, _parentState));
						((Binary_expressionContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(242);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(243);
						((Binary_expressionContext)_localctx).t = match(OrOr);
						setState(244);
						((Binary_expressionContext)_localctx).r = expression(3);
						}
						break;
					case 12:
						{
						_localctx = new Assign_expressionContext(new ExpressionContext(_parentctx, _parentState));
						((Assign_expressionContext)_localctx).l = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(245);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(246);
						((Assign_expressionContext)_localctx).t = match(Assignment);
						setState(247);
						((Assign_expressionContext)_localctx).r = expression(2);
						}
						break;
					case 13:
						{
						_localctx = new Suffix_expressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(248);
						if (!(precpred(_ctx, 17))) throw new FailedPredicateException(this, "precpred(_ctx, 17)");
						setState(249);
						((Suffix_expressionContext)_localctx).t = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==SelfInc || _la==SelfDec) ) {
							((Suffix_expressionContext)_localctx).t = (Token)_errHandler.recoverInline(this);
						} else {
							consume();
						}
						}
						break;
					case 14:
						{
						_localctx = new Functional_expressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(250);
						if (!(precpred(_ctx, 16))) throw new FailedPredicateException(this, "precpred(_ctx, 16)");
						setState(251);
						match(LParen);
						setState(253);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Kw_Bool) | (1L << Kw_Int) | (1L << Kw_String) | (1L << Kw_Null) | (1L << Kw_Void) | (1L << Kw_New) | (1L << Bool) | (1L << Add) | (1L << Sub) | (1L << Tilde) | (1L << Not) | (1L << SelfInc) | (1L << SelfDec) | (1L << LParen) | (1L << Int) | (1L << Identifier) | (1L << Sign) | (1L << String))) != 0)) {
							{
							setState(252);
							arguments();
							}
						}

						setState(255);
						match(RParen);
						}
						break;
					case 15:
						{
						_localctx = new Subscript_expressionContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(256);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(258); 
						_errHandler.sync(this);
						_alt = 1;
						do {
							switch (_alt) {
							case 1:
								{
								{
								setState(257);
								bracket_clude_expression();
								}
								}
								break;
							default:
								throw new NoViableAltException(this);
							}
							setState(260); 
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
						} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
						}
						break;
					}
					} 
				}
				setState(266);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ArgumentsContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ArgumentsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arguments; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterArguments(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitArguments(this);
		}
	}

	public final ArgumentsContext arguments() throws RecognitionException {
		ArgumentsContext _localctx = new ArgumentsContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_arguments);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(267);
			expression(0);
			setState(272);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(268);
				match(T__1);
				setState(269);
				expression(0);
				}
				}
				setState(274);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
	 
		public ValueContext() { }
		public void copyFrom(ValueContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class String_valueContext extends ValueContext {
		public TerminalNode String() { return getToken(ComplierParser.String, 0); }
		public String_valueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterString_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitString_value(this);
		}
	}
	public static class Identifier_valueContext extends ValueContext {
		public TerminalNode Identifier() { return getToken(ComplierParser.Identifier, 0); }
		public Identifier_valueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterIdentifier_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitIdentifier_value(this);
		}
	}
	public static class Null_valueContext extends ValueContext {
		public TerminalNode Kw_Null() { return getToken(ComplierParser.Kw_Null, 0); }
		public Null_valueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterNull_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitNull_value(this);
		}
	}
	public static class Bool_valueContext extends ValueContext {
		public TerminalNode Bool() { return getToken(ComplierParser.Bool, 0); }
		public Bool_valueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterBool_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitBool_value(this);
		}
	}
	public static class Type_valueContext extends ValueContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public Type_valueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterType_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitType_value(this);
		}
	}
	public static class Int_valueContext extends ValueContext {
		public TerminalNode Int() { return getToken(ComplierParser.Int, 0); }
		public TerminalNode Sign() { return getToken(ComplierParser.Sign, 0); }
		public Int_valueContext(ValueContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterInt_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitInt_value(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_value);
		int _la;
		try {
			setState(284);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				_localctx = new Int_valueContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(276);
				_la = _input.LA(1);
				if (_la==Sign) {
					{
					setState(275);
					match(Sign);
					}
				}

				setState(278);
				match(Int);
				}
				break;
			case 2:
				_localctx = new Bool_valueContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(279);
				match(Bool);
				}
				break;
			case 3:
				_localctx = new String_valueContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(280);
				match(String);
				}
				break;
			case 4:
				_localctx = new Null_valueContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(281);
				match(Kw_Null);
				}
				break;
			case 5:
				_localctx = new Identifier_valueContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(282);
				match(Identifier);
				}
				break;
			case 6:
				_localctx = new Type_valueContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(283);
				type();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bracket_clude_expressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Bracket_clude_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bracket_clude_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).enterBracket_clude_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ComplierListener ) ((ComplierListener)listener).exitBracket_clude_expression(this);
		}
	}

	public final Bracket_clude_expressionContext bracket_clude_expression() throws RecognitionException {
		Bracket_clude_expressionContext _localctx = new Bracket_clude_expressionContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_bracket_clude_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(286);
			match(LBracket);
			setState(288);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Kw_Bool) | (1L << Kw_Int) | (1L << Kw_String) | (1L << Kw_Null) | (1L << Kw_Void) | (1L << Kw_New) | (1L << Bool) | (1L << Add) | (1L << Sub) | (1L << Tilde) | (1L << Not) | (1L << SelfInc) | (1L << SelfDec) | (1L << LParen) | (1L << Int) | (1L << Identifier) | (1L << Sign) | (1L << String))) != 0)) {
				{
				setState(287);
				expression(0);
				}
			}

			setState(290);
			match(RBracket);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 21:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 14);
		case 1:
			return precpred(_ctx, 12);
		case 2:
			return precpred(_ctx, 11);
		case 3:
			return precpred(_ctx, 10);
		case 4:
			return precpred(_ctx, 9);
		case 5:
			return precpred(_ctx, 8);
		case 6:
			return precpred(_ctx, 7);
		case 7:
			return precpred(_ctx, 6);
		case 8:
			return precpred(_ctx, 5);
		case 9:
			return precpred(_ctx, 4);
		case 10:
			return precpred(_ctx, 3);
		case 11:
			return precpred(_ctx, 2);
		case 12:
			return precpred(_ctx, 17);
		case 13:
			return precpred(_ctx, 16);
		case 14:
			return precpred(_ctx, 15);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3:\u0127\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\3\2\3\2\3\2\3\2\3\2\5\2:\n\2\3\3\7\3=\n\3\f\3\16\3@\13\3\3"+
		"\4\3\4\3\4\5\4E\n\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\7\6O\n\6\f\6\16\6"+
		"R\13\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\7\b]\n\b\f\b\16\b`\13\b\3\t"+
		"\3\t\3\t\3\t\5\tf\n\t\3\t\3\t\3\t\3\n\3\n\3\n\7\nn\n\n\f\n\16\nq\13\n"+
		"\3\13\3\13\3\13\3\f\3\f\3\f\7\fy\n\f\f\f\16\f|\13\f\3\r\3\r\3\r\5\r\u0081"+
		"\n\r\3\16\3\16\7\16\u0085\n\16\f\16\16\16\u0088\13\16\3\17\3\17\3\17\3"+
		"\20\3\20\3\20\3\20\3\20\3\20\5\20\u0093\n\20\3\21\3\21\5\21\u0097\n\21"+
		"\3\22\5\22\u009a\n\22\3\22\3\22\3\23\3\23\7\23\u00a0\n\23\f\23\16\23\u00a3"+
		"\13\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u00ae\n\24\3"+
		"\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u00bb\n\25"+
		"\3\25\3\25\3\25\5\25\u00c0\n\25\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u00c8"+
		"\n\26\3\26\5\26\u00cb\n\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\5\27"+
		"\u00d5\n\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27"+
		"\3\27\5\27\u0100\n\27\3\27\3\27\3\27\6\27\u0105\n\27\r\27\16\27\u0106"+
		"\7\27\u0109\n\27\f\27\16\27\u010c\13\27\3\30\3\30\3\30\7\30\u0111\n\30"+
		"\f\30\16\30\u0114\13\30\3\31\5\31\u0117\n\31\3\31\3\31\3\31\3\31\3\31"+
		"\3\31\5\31\u011f\n\31\3\32\3\32\5\32\u0123\n\32\3\32\3\32\3\32\2\3,\33"+
		"\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\2\t\7\2\24\24\27"+
		"\30$$((,-\3\2\31\33\3\2\27\30\3\2&\'\3\2\34\37\3\2 !\3\2,-\u0144\29\3"+
		"\2\2\2\4>\3\2\2\2\6D\3\2\2\2\bF\3\2\2\2\nJ\3\2\2\2\fU\3\2\2\2\16Y\3\2"+
		"\2\2\20a\3\2\2\2\22j\3\2\2\2\24r\3\2\2\2\26u\3\2\2\2\30}\3\2\2\2\32\u0082"+
		"\3\2\2\2\34\u0089\3\2\2\2\36\u0092\3\2\2\2 \u0096\3\2\2\2\"\u0099\3\2"+
		"\2\2$\u009d\3\2\2\2&\u00a6\3\2\2\2(\u00bf\3\2\2\2*\u00ca\3\2\2\2,\u00d4"+
		"\3\2\2\2.\u010d\3\2\2\2\60\u011e\3\2\2\2\62\u0120\3\2\2\2\64:\7\t\2\2"+
		"\65:\7\n\2\2\66:\7\13\2\2\67:\7\r\2\28:\7\66\2\29\64\3\2\2\29\65\3\2\2"+
		"\29\66\3\2\2\29\67\3\2\2\298\3\2\2\2:\3\3\2\2\2;=\5\6\4\2<;\3\2\2\2=@"+
		"\3\2\2\2><\3\2\2\2>?\3\2\2\2?\5\3\2\2\2@>\3\2\2\2AE\5\b\5\2BE\5\n\6\2"+
		"CE\5\20\t\2DA\3\2\2\2DB\3\2\2\2DC\3\2\2\2E\7\3\2\2\2FG\5\32\16\2GH\5\26"+
		"\f\2HI\7\3\2\2I\t\3\2\2\2JK\7\25\2\2KL\7\66\2\2LP\7\63\2\2MO\5\f\7\2N"+
		"M\3\2\2\2OR\3\2\2\2PN\3\2\2\2PQ\3\2\2\2QS\3\2\2\2RP\3\2\2\2ST\7\64\2\2"+
		"T\13\3\2\2\2UV\5\32\16\2VW\5\16\b\2WX\7\3\2\2X\r\3\2\2\2Y^\7\66\2\2Z["+
		"\7\4\2\2[]\7\66\2\2\\Z\3\2\2\2]`\3\2\2\2^\\\3\2\2\2^_\3\2\2\2_\17\3\2"+
		"\2\2`^\3\2\2\2ab\5\32\16\2bc\7\66\2\2ce\7/\2\2df\5\22\n\2ed\3\2\2\2ef"+
		"\3\2\2\2fg\3\2\2\2gh\7\60\2\2hi\5$\23\2i\21\3\2\2\2jo\5\24\13\2kl\7\4"+
		"\2\2ln\5\24\13\2mk\3\2\2\2nq\3\2\2\2om\3\2\2\2op\3\2\2\2p\23\3\2\2\2q"+
		"o\3\2\2\2rs\5\32\16\2st\7\66\2\2t\25\3\2\2\2uz\5\30\r\2vw\7\4\2\2wy\5"+
		"\30\r\2xv\3\2\2\2y|\3\2\2\2zx\3\2\2\2z{\3\2\2\2{\27\3\2\2\2|z\3\2\2\2"+
		"}\u0080\7\66\2\2~\177\7+\2\2\177\u0081\5,\27\2\u0080~\3\2\2\2\u0080\u0081"+
		"\3\2\2\2\u0081\31\3\2\2\2\u0082\u0086\5\2\2\2\u0083\u0085\5\34\17\2\u0084"+
		"\u0083\3\2\2\2\u0085\u0088\3\2\2\2\u0086\u0084\3\2\2\2\u0086\u0087\3\2"+
		"\2\2\u0087\33\3\2\2\2\u0088\u0086\3\2\2\2\u0089\u008a\7\61\2\2\u008a\u008b"+
		"\7\62\2\2\u008b\35\3\2\2\2\u008c\u0093\5\"\22\2\u008d\u0093\5$\23\2\u008e"+
		"\u0093\5&\24\2\u008f\u0093\5(\25\2\u0090\u0093\5*\26\2\u0091\u0093\5\b"+
		"\5\2\u0092\u008c\3\2\2\2\u0092\u008d\3\2\2\2\u0092\u008e\3\2\2\2\u0092"+
		"\u008f\3\2\2\2\u0092\u0090\3\2\2\2\u0092\u0091\3\2\2\2\u0093\37\3\2\2"+
		"\2\u0094\u0097\5\"\22\2\u0095\u0097\5\b\5\2\u0096\u0094\3\2\2\2\u0096"+
		"\u0095\3\2\2\2\u0097!\3\2\2\2\u0098\u009a\5,\27\2\u0099\u0098\3\2\2\2"+
		"\u0099\u009a\3\2\2\2\u009a\u009b\3\2\2\2\u009b\u009c\7\3\2\2\u009c#\3"+
		"\2\2\2\u009d\u00a1\7\63\2\2\u009e\u00a0\5\36\20\2\u009f\u009e\3\2\2\2"+
		"\u00a0\u00a3\3\2\2\2\u00a1\u009f\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00a4"+
		"\3\2\2\2\u00a3\u00a1\3\2\2\2\u00a4\u00a5\7\64\2\2\u00a5%\3\2\2\2\u00a6"+
		"\u00a7\7\16\2\2\u00a7\u00a8\7/\2\2\u00a8\u00a9\5,\27\2\u00a9\u00aa\7\60"+
		"\2\2\u00aa\u00ad\5\36\20\2\u00ab\u00ac\7\5\2\2\u00ac\u00ae\5\36\20\2\u00ad"+
		"\u00ab\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\'\3\2\2\2\u00af\u00b0\7\6\2\2"+
		"\u00b0\u00b1\7/\2\2\u00b1\u00b2\5,\27\2\u00b2\u00b3\7\60\2\2\u00b3\u00b4"+
		"\5\36\20\2\u00b4\u00c0\3\2\2\2\u00b5\u00b6\7\17\2\2\u00b6\u00b7\7/\2\2"+
		"\u00b7\u00b8\5 \21\2\u00b8\u00ba\5\"\22\2\u00b9\u00bb\5,\27\2\u00ba\u00b9"+
		"\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc\u00bd\7\60\2\2"+
		"\u00bd\u00be\5\36\20\2\u00be\u00c0\3\2\2\2\u00bf\u00af\3\2\2\2\u00bf\u00b5"+
		"\3\2\2\2\u00c0)\3\2\2\2\u00c1\u00c2\7\7\2\2\u00c2\u00cb\7\3\2\2\u00c3"+
		"\u00c4\7\21\2\2\u00c4\u00cb\7\3\2\2\u00c5\u00c7\7\23\2\2\u00c6\u00c8\5"+
		",\27\2\u00c7\u00c6\3\2\2\2\u00c7\u00c8\3\2\2\2\u00c8\u00c9\3\2\2\2\u00c9"+
		"\u00cb\7\3\2\2\u00ca\u00c1\3\2\2\2\u00ca\u00c3\3\2\2\2\u00ca\u00c5\3\2"+
		"\2\2\u00cb+\3\2\2\2\u00cc\u00cd\b\27\1\2\u00cd\u00ce\t\2\2\2\u00ce\u00d5"+
		"\5,\27\17\u00cf\u00d0\7/\2\2\u00d0\u00d1\5,\27\2\u00d1\u00d2\7\60\2\2"+
		"\u00d2\u00d5\3\2\2\2\u00d3\u00d5\5\60\31\2\u00d4\u00cc\3\2\2\2\u00d4\u00cf"+
		"\3\2\2\2\u00d4\u00d3\3\2\2\2\u00d5\u010a\3\2\2\2\u00d6\u00d7\f\20\2\2"+
		"\u00d7\u00d8\7.\2\2\u00d8\u0109\5,\27\21\u00d9\u00da\f\16\2\2\u00da\u00db"+
		"\t\3\2\2\u00db\u0109\5,\27\17\u00dc\u00dd\f\r\2\2\u00dd\u00de\t\4\2\2"+
		"\u00de\u0109\5,\27\16\u00df\u00e0\f\f\2\2\u00e0\u00e1\t\5\2\2\u00e1\u0109"+
		"\5,\27\r\u00e2\u00e3\f\13\2\2\u00e3\u00e4\t\6\2\2\u00e4\u0109\5,\27\f"+
		"\u00e5\u00e6\f\n\2\2\u00e6\u00e7\t\7\2\2\u00e7\u0109\5,\27\13\u00e8\u00e9"+
		"\f\t\2\2\u00e9\u00ea\7\"\2\2\u00ea\u0109\5,\27\n\u00eb\u00ec\f\b\2\2\u00ec"+
		"\u00ed\7%\2\2\u00ed\u0109\5,\27\t\u00ee\u00ef\f\7\2\2\u00ef\u00f0\7#\2"+
		"\2\u00f0\u0109\5,\27\b\u00f1\u00f2\f\6\2\2\u00f2\u00f3\7)\2\2\u00f3\u0109"+
		"\5,\27\6\u00f4\u00f5\f\5\2\2\u00f5\u00f6\7*\2\2\u00f6\u0109\5,\27\5\u00f7"+
		"\u00f8\f\4\2\2\u00f8\u00f9\7+\2\2\u00f9\u0109\5,\27\4\u00fa\u00fb\f\23"+
		"\2\2\u00fb\u0109\t\b\2\2\u00fc\u00fd\f\22\2\2\u00fd\u00ff\7/\2\2\u00fe"+
		"\u0100\5.\30\2\u00ff\u00fe\3\2\2\2\u00ff\u0100\3\2\2\2\u0100\u0101\3\2"+
		"\2\2\u0101\u0109\7\60\2\2\u0102\u0104\f\21\2\2\u0103\u0105\5\62\32\2\u0104"+
		"\u0103\3\2\2\2\u0105\u0106\3\2\2\2\u0106\u0104\3\2\2\2\u0106\u0107\3\2"+
		"\2\2\u0107\u0109\3\2\2\2\u0108\u00d6\3\2\2\2\u0108\u00d9\3\2\2\2\u0108"+
		"\u00dc\3\2\2\2\u0108\u00df\3\2\2\2\u0108\u00e2\3\2\2\2\u0108\u00e5\3\2"+
		"\2\2\u0108\u00e8\3\2\2\2\u0108\u00eb\3\2\2\2\u0108\u00ee\3\2\2\2\u0108"+
		"\u00f1\3\2\2\2\u0108\u00f4\3\2\2\2\u0108\u00f7\3\2\2\2\u0108\u00fa\3\2"+
		"\2\2\u0108\u00fc\3\2\2\2\u0108\u0102\3\2\2\2\u0109\u010c\3\2\2\2\u010a"+
		"\u0108\3\2\2\2\u010a\u010b\3\2\2\2\u010b-\3\2\2\2\u010c\u010a\3\2\2\2"+
		"\u010d\u0112\5,\27\2\u010e\u010f\7\4\2\2\u010f\u0111\5,\27\2\u0110\u010e"+
		"\3\2\2\2\u0111\u0114\3\2\2\2\u0112\u0110\3\2\2\2\u0112\u0113\3\2\2\2\u0113"+
		"/\3\2\2\2\u0114\u0112\3\2\2\2\u0115\u0117\7\67\2\2\u0116\u0115\3\2\2\2"+
		"\u0116\u0117\3\2\2\2\u0117\u0118\3\2\2\2\u0118\u011f\7\65\2\2\u0119\u011f"+
		"\7\26\2\2\u011a\u011f\78\2\2\u011b\u011f\7\f\2\2\u011c\u011f\7\66\2\2"+
		"\u011d\u011f\5\2\2\2\u011e\u0116\3\2\2\2\u011e\u0119\3\2\2\2\u011e\u011a"+
		"\3\2\2\2\u011e\u011b\3\2\2\2\u011e\u011c\3\2\2\2\u011e\u011d\3\2\2\2\u011f"+
		"\61\3\2\2\2\u0120\u0122\7\61\2\2\u0121\u0123\5,\27\2\u0122\u0121\3\2\2"+
		"\2\u0122\u0123\3\2\2\2\u0123\u0124\3\2\2\2\u0124\u0125\7\62\2\2\u0125"+
		"\63\3\2\2\2\369>DP^eoz\u0080\u0086\u0092\u0096\u0099\u00a1\u00ad\u00ba"+
		"\u00bf\u00c7\u00ca\u00d4\u00ff\u0106\u0108\u010a\u0112\u0116\u011e\u0122";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}