/**
 * Created by 章 on 2016/4/6.
 */
public class Printer extends Visitor{
    int deep = 1;
    void visit(String name, String pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        if (pointer.equals("%"))
            System.out.printf(name + "(" + "%%" + ")\n");
        else
            System.out.printf(name + "(" + pointer + ")\n");
    }
    @Override
    void visit(Program pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Program\n");
        for (int i = 0; i < pointer.list.size(); i++) {
            deep++;
            visit(pointer.list.get(i));
            deep--;
        }
    }

    @Override
    void visit(Functional_Declaration pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Functional_Declaration\n");
        deep++;
        visit(pointer.type);
        visit("name", pointer.name);
        for (int i = 0 ; i < pointer.parameter.size(); i++)
            visit(pointer.parameter.get(i));
        visit(pointer.context);
        deep--;
    }

    @Override
    void visit(Class_Declaration pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Class_Declaration ");
  //      System.out.printf(pointer.member.size() + "\n");
        deep++;
        visit("name",pointer.name);
        for (int i = 0; i < pointer.member.size(); i++ )
            visit(pointer.member.get(i));
        deep--;
    }

    @Override
    void visit(Selection_Statement pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Selection_Statement\n");
        deep++;
            visit(pointer.condition);
            visit(pointer.then_context);
            visit(pointer.else_context);
        deep--;
    }

    @Override
    void visit(Block_Statement pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Block_Statement\n");
        deep++;
        for (int i = 0 ; i< pointer.list.size(); i++)
            visit(pointer.list.get(i));
        deep--;
    }

    @Override
    void visit(For_Statement pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("For_Statement\n");
        deep++;
            visit(pointer.initial);
            visit(pointer.condition);
            visit(pointer.update);
            visit(pointer.context);
        deep--;
    }

    @Override
    void visit(While_Statement pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("While_Statement\n");
        deep++;
            visit(pointer.condition);
            visit(pointer.context);
        deep--;
    }

    @Override
    void visit(Jump_Statement pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Jump_Statement\n");
        deep++;
            visit("meaning", pointer.meaning);
        deep--;
    }

    @Override
    void visit(Return_Statement pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Return_Statement\n");
        deep++;
            visit(pointer.context);
        deep--;
    }

    @Override
    void visit(Argument pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Argument\n");
        deep++;
            visit(pointer.type);
            visit("name",pointer.name);
            if (pointer.have_initial_value) visit(pointer.initial_value);
        deep--;
    }

    @Override
    void visit(Declaration_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Declaration_Expression\n");
        deep++;
        for (int i  = 0 ; i < pointer.member.size(); i++)
            visit(pointer.member.get(i));
        deep--;
    }

    @Override
    void visit(Suffix_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Suffix_Expression\n");
        deep++;
            visit("operator",pointer.oper);
            visit(pointer.expression);
        deep--;
    }

    @Override
    void visit(Functional_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Functional_Expression\n");
        deep++;
            visit(pointer.expression);
            for (int i = 0; i < pointer.argument.size(); i++)
                visit(pointer.argument.get(i));
        deep--;
    }

    @Override
    void visit(Subscript_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Subscript_Expression：fullsize(" + pointer.true_size + ")\n");
        deep++;
        visit(pointer.expression);
        for (int i = 0; i < pointer.argument.size(); i++)
            visit(pointer.argument.get(i));
        deep--;
    }

    @Override
    void visit(Member_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Member_Expression\n");
        deep++;
        visit(pointer.expression);
        visit(pointer.member);
        deep--;
    }

    @Override
    void visit(Prefix_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Prefix_Expression\n");
        deep++;
        visit("operator", pointer.oper);
        visit(pointer.expression);
        deep--;
    }

    @Override
    void visit(Binary_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Binary_Expression\n");
        deep++;
            visit(pointer.left_exp);
            visit("operator", pointer.oper);
            visit(pointer.right_exp);
        deep--;
    }

    @Override
    void visit(Int_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Integer(" + pointer.value + ")\n");
    }

    @Override
    void visit(Bool_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Bool(" + pointer.value + ")\n");
    }

    @Override
    void visit(String_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("String(" + pointer.value + ")\n");
    }

    @Override
    void visit(Empty_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Empty_Expression\n");
    }

    @Override
    void visit(Null_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("NULL\n");
    }

    @Override
    void visit(Type pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Type: " + "name(" + pointer.name + ")") ;
        for (int i = 0; i < pointer.true_size; i++)
            if (pointer.used_size <= i)
                System.out.printf("[]");
            else
                System.out.printf("[" +  pointer.size.get(i) + ']');
        System.out.printf("\n");
    }

    @Override
    void visit(Identifier_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("ID(" + pointer.name + ")\n");
    }

    @Override
    void visit(Assign_Expression pointer) {
        for (int i = 1; i <= deep * 2; i++)
            System.out.printf(".");
        System.out.printf("Binary_Expression\n");
        deep++;
        visit(pointer.left_exp);
        visit("operator", "=");
        visit(pointer.right_exp);
        deep--;
    }
}
