import java.util.ArrayList;
import java.util.List;

/**
 * Created by 章 on 2016/4/4.
 */
public abstract class IC_AST {
    boolean is_str = false;
    abstract void accept(Visitor v);
}
class Program extends IC_AST{
    List<IC_AST> list = new ArrayList<IC_AST>();
    void accept(Visitor v){ v.visit(this);}
    void add(IC_AST now){list.add(now);   }

}
abstract class Declaration_Statement extends Statement{
    abstract void accept(Visitor v);
}
class Functional_Declaration extends Declaration_Statement {
    Type type;
    String name;
    List<Argument> parameter = new ArrayList<Argument>();
    Block_Statement context;
    void accept(Visitor v){ v.visit(this);}
}
class Class_Declaration extends Declaration_Statement {
    String name;
    List<Argument> member = new ArrayList<Argument>();
    void accept(Visitor v){ v.visit(this);}
    void add(List<Argument> now){
        if (now != null)
        for (int i = 0; i < now.size(); i++)
            member.add(now.get(i));
    }
}
abstract class Statement extends IC_AST {
    abstract void accept(Visitor v);
}
abstract class Expression extends Statement {
    abstract void accept(Visitor v);
}
class Selection_Statement extends Statement{
    Expression condition;
    Statement then_context,else_context;
    void accept(Visitor v){ v.visit(this);}
}
class Block_Statement extends Statement{
    List<Statement> list = new ArrayList<Statement>();
    void accept(Visitor v){ v.visit(this);}
}
class For_Statement extends Statement{
    Statement initial,condition,update,context;
    void accept(Visitor v){ v.visit(this);}
}
class While_Statement extends Statement{
    Expression condition;
    Statement context;
    void accept(Visitor v){ v.visit(this);}
}
class Jump_Statement extends Statement{
    String meaning;
    void accept(Visitor v){ v.visit(this);}
}
class Return_Statement extends Statement{
    Expression context;
    void accept(Visitor v){ v.visit(this);}
}
/*class Parameter extends IC_AST{
    Type type;
    String name;
    void accept(Visitor v){ v.visit(this);}
}*/

class Argument extends IC_AST {
    Type type;
    String name;
    boolean have_initial_value;
    Expression initial_value;
    void accept(Visitor v){ v.visit(this);}
    void changetype(Type now_type) {type = now_type;}
}
class Declaration_Expression extends Expression{
    List<Argument> member = new ArrayList<Argument>();
    void accept(Visitor v){ v.visit(this);}
}
class Suffix_Expression extends Expression{
    Expression expression;
    String oper;
    void accept(Visitor v){ v.visit(this);}
}
class Functional_Expression extends Expression{
    Expression expression;
    List<Expression> argument = new ArrayList<Expression>();
    boolean is_string = false;
    boolean is_array = false;
    void accept(Visitor v){ v.visit(this);}
}
class Subscript_Expression extends Expression{
    Expression expression;
    int true_size;
    int used_size;
    List<Expression> argument = new ArrayList<Expression>();
    void accept(Visitor v){ v.visit(this);}
}
class Member_Expression extends Expression {
    Expression expression;
    Expression member;
    Actual_Type val;
    void accept(Visitor v){ v.visit(this);}
}
class Prefix_Expression extends Expression{
    String oper;
    Expression expression;
    int par_num = 0;
    void accept(Visitor v){ v.visit(this);}
}
class Binary_Expression extends Expression{
    Expression left_exp,right_exp;
    String oper;
    void accept(Visitor v){ v.visit(this);}
}
class Assign_Expression extends Expression{
    Expression left_exp,right_exp;
    void accept(Visitor v){ v.visit(this);}
}

abstract class Value_Expression extends Expression{
    abstract void accept(Visitor v);
}
class Int_Expression extends Value_Expression{
    long value;
    void accept(Visitor v){ v.visit(this);}
}
class Bool_Expression extends Value_Expression{
    boolean value;
    Bool_Expression (boolean t){value = t;}
    Bool_Expression (){}
    void accept(Visitor v){ v.visit(this);}
}
class String_Expression extends Value_Expression{
    String value;
    void accept(Visitor v){ v.visit(this);}
}
class Empty_Expression extends Value_Expression{
    void accept(Visitor v){ v.visit(this);}
}
class Null_Expression extends Value_Expression {
    void accept(Visitor v){ v.visit(this);}
}
class Type extends IC_AST {
    String name;
    int true_size = 0;
    int used_size = 0;
    List<Integer> size = new ArrayList<Integer>();
    void accept(Visitor v){ v.visit(this);}
}
class Identifier_Expression extends Value_Expression {
    String name;
    void accept(Visitor v){ v.visit(this);}
}

