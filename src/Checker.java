import java.util.*;

/**
 * Created by 章 on 2016/4/7.
 */
public abstract class Checker extends Visitor {
    HashMap<String, Type_Style> string_map = new HashMap<>();
    HashMap<String, Type_Style> map = new HashMap<>();
    Exitcode exitcode = new Exitcode();



    Actual_Type find_type(Type now){
        if (map.get(now.name) == null) {
            exitcode.code = 2;
            exitcode.name = now.name + " At ";
            return null;
        }
        return map.get(now.name).get_Actual(now.true_size);
    }
    //default-type adding...
    void add_default_type(String name){
        map.put(name, new Default_Type(name){});
    }


    //Function_type adding
    void add_function_type(Actual_Type type, String name, List<Actual_Type> parameter,List<String> parametername){
        Function_Type cur = new Function_Type();
        cur.name = name;
        cur.return_Type = type;
        cur.parameter = parameter;
        cur.parameter_name = parametername;
        if (map.get(name) != null){
            exitcode.code = 1;
            exitcode.name = name + " At";
        }
        else
            map.put(name,cur);
    }

    //specially for String_function
    void add_function_type_for_string(Actual_Type type, String name, List<Actual_Type> parameter,List<String>parametername){
        Function_Type cur = new Function_Type();
        cur.name = name;
        cur.return_Type = type;
        cur.parameter = parameter;
        cur.parameter_name = parametername;
        string_map.put(name,cur);
    }


    void add_class_type(String name, HashMap<String, Actual_Type> parameter){
        Class_Type cur = new Class_Type();
        cur.name = name;
        cur.parameter = parameter;
    }
}
