import org.w3c.dom.ls.LSException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by 章 on 2016/4/27.
 */
 public class Translate extends Visitor {
    IR_Block st = new IR_Block();
    int now_scope = 0, tot_scope = 0,deep = 0;
    List<HashMap<String, IR_Argument>> scope_argument_map;
    HashMap<IR_Label,IR_Block> block_map;
    HashMap<String, IR_Func> func_map;
    List<Integer> scope_stack  = new ArrayList<>();;
    List<Integer> scope_argument_num = new ArrayList<>();
    IR_Block now_Block, root;
    HashMap<Object,IR_Argument> value;
    List<IR_Block> block_list;
    List<IR_Label> continue_stack,break_stack;
    HashMap<IR_Argument,Integer> argu_map = new HashMap<>();
    List<String> reg_name = new ArrayList<>();
    List<String> data_block = new ArrayList<>();
    int block_num = 0;
    IR_Func st_func = new IR_Func();
    HashSet<String> global_set;
    HashMap<String, Long> global_map;
    long global_num = 0;
    void init(){
        global_num = 0;
        global_map = new HashMap<>();
        global_set = new HashSet<>();
        scope_stack = new ArrayList<>();
        scope_argument_num = new ArrayList<>();
        scope_argument_map = new ArrayList<>();
        func_map = new HashMap<>();
        scope_stack.add(0);
        now_scope = -1;
        tot_scope = -1;
        deep = 0;
        block_num = 0;
        continue_stack = new ArrayList<>();
        break_stack = new ArrayList<>();
        value = new HashMap<>();
        block_map = new HashMap<>();
        block_list = new ArrayList<>();

        IR_Func st_F = new IR_Func();
        st_F.name = "start";
        now_Block = new_block(st_F.name);
        func_map.put(st_F.name, st_F);
        st_F.start = now_Block.name;
        now_Block.is_func_start = true;

        data_block = new ArrayList<>();
        data_block.add(".data\n");
        //     System.out.printf(reg_name.size() + "\n");
        reg_name.add("$zero");      reg_name.add("$at");
        reg_name.add("$v0");        reg_name.add("$v1");
        reg_name.add("$a0");        reg_name.add("$a1");
        reg_name.add("$a2");        reg_name.add("$a3");
        reg_name.add("$t0");        reg_name.add("$t1");
        reg_name.add("$t2");        reg_name.add("$t3");
        reg_name.add("$t4");        reg_name.add("$t5");
        reg_name.add("$t6");        reg_name.add("$t7");
        reg_name.add("$s0");        reg_name.add("$s1");
        reg_name.add("$s2");        reg_name.add("$s3");
        reg_name.add("$s4");        reg_name.add("$s5");
        reg_name.add("$s6");        reg_name.add("$s7");
        reg_name.add("$t8");        reg_name.add("$t9");
        reg_name.add("$k0");        reg_name.add("$k1");
        reg_name.add("$gp");        reg_name.add("$sp");
        reg_name.add("$fp");        reg_name.add("$ra");
   //     System.out.printf(reg_name.size() + "\n");
    }
    IR_Argument new_Argument(String name, boolean is_create){
        int num = scope_argument_num.get(now_scope) + 1;
        scope_argument_num.set(now_scope, num);
        String pname = name + "_" + now_scope;
        if (is_create) pname = pname + "_" + num;
        pname = pname + "_";
        IR_Argument t = new IR_Argument(!is_create,  pname);
        scope_argument_map.get(now_scope).put(pname, t);
        return t;
    }

    IR_Block new_block(String name){
        IR_Block t = new IR_Block();
        block_num ++;
        t.name = new IR_Label(name + "_" + block_num + "_");
        t.expression = new ArrayList<>();
        block_map.put(t.name, t);
        block_list.add(t);
        if (block_num == 1) root = t;
    //    t.out = new IR_Control();
        return t;
    }

    void intoscope(){
        tot_scope++;
        scope_stack.add(tot_scope);
        deep++;
        now_scope = scope_stack.get(deep);
        scope_argument_map.add(new HashMap<>());
        scope_argument_num.add(0);
    }

    void outofscope(){
        scope_stack.remove(deep);
        deep--;
        if (deep == 1) now_Block = block_map.get(func_map.get("start").start);
        now_scope = scope_stack.get(deep);
    }

    IR_Argument find_Argument(String name){
        for (int i = deep  ; i >= 0; i--) {
            IR_Argument t = scope_argument_map.get(scope_stack.get(i)).get(name + "_" + scope_stack.get(i) + "_");
            if (t != null) return t;
        }
        return new_Argument(name, false);
    }






    @Override
    void visit(Program pointer) {
        intoscope();
        //  System.out.printf(now_scope + "");
        for (int i = 0; i < pointer.list.size(); i++)
            visit(pointer.list.get(i));
        outofscope();
    }

    @Override
    void visit(Functional_Declaration pointer) {
        intoscope();
     //   System.out.printf(deep + " " +  scope_stack.get(1));
        IR_Func func = new IR_Func();
        func.name = pointer.name;
        for (int i = 0; i < pointer.parameter.size(); i++) {
            func.parameter.add(new_Argument(pointer.parameter.get(i).name, false));
            func.parameter.get(i).is_string = pointer.parameter.get(i).is_str;
        }
        IR_Block func_block = new_block(func.name);
        func.start = func_block.name;
        func_block.is_func_start = true;
        now_Block = func_block;
        if (func.name.equals("main"))
        {
            st = now_Block;
            st_func = func;
        }
        func_map.put(func.name,func);
        for (int i = 0 ; i < pointer.context.list.size(); i++)
            visit(pointer.context.list.get(i));
        outofscope();
    }

    @Override
    void visit(Class_Declaration pointer){
    }

    @Override
    void visit(Selection_Statement pointer) {
        IR_Block if_begin = new_block("if_begin"),if_true = new_block("if_true"), if_false = new_block("if_false"), if_end  = new_block("if_end");

        now_Block.out = new IR_Jump(if_begin.name);

        now_Block = if_begin;
        visit(pointer.condition);
        if (pointer.else_context instanceof Empty_Expression)
            now_Block.out = new IR_Branch(value.get(pointer.condition),if_true.name, if_end.name);
        else
            now_Block.out = new IR_Branch(value.get(pointer.condition),if_true.name, if_false.name);
        now_Block = if_true;
        visit(pointer.then_context);
        now_Block.out = new IR_Jump(if_end.name);

        now_Block = if_false;
        visit(pointer.else_context);
        now_Block.out = new IR_Jump(if_end.name);

        now_Block = if_end;
    }

    @Override
    void visit(Block_Statement pointer) {
        intoscope();
    //    IR_Block block_st = new_block("block");
    //    now_Block.out = new IR_Jump(block_st.name);
    //    now_Block = block_st;
        for (int i = 0; i < pointer.list.size();i++)
            visit(pointer.list.get(i));
        outofscope();
    }

    @Override
    void visit(For_Statement pointer) {
        intoscope();
        IR_Block /*for_begin = new_block("for_begin"),*/for_cond = new_block("for_cond"),for_upgrade = new_block("for_upgrade"),for_context = new_block("for_context"),for_end = new_block("for_end");
    //      now_Block.out = new IR_Jump(for_begin.name);
    //      now_Block = for_begin;
        visit(pointer.initial);
        now_Block.out = new IR_Jump(for_cond.name);

        now_Block = for_cond;
        visit(pointer.condition);
        now_Block.out = new IR_Branch(value.get(pointer.condition),for_context.name,for_end.name);

        now_Block = for_context;
        continue_stack.add(for_cond.name);
        break_stack.add(for_end.name);
        visit(pointer.context);
        continue_stack.remove(continue_stack.size() - 1);
        break_stack.remove(break_stack.size() - 1);
        now_Block.out = new IR_Jump(for_upgrade.name);

        now_Block = for_upgrade;
        visit(pointer.update);
        now_Block.out = new IR_Jump(for_cond.name);

        now_Block = for_end;
        outofscope();
    }

    @Override
    void visit(While_Statement pointer) {
        IR_Block while_begin = new_block("while_begin"),while_context = new_block("while_context"),while_end = new_block("while_end");
        now_Block.out = new IR_Jump(while_begin.name);

        now_Block = while_begin;
        visit(pointer.condition);
        now_Block.out = new IR_Branch(value.get(pointer.condition),while_context.name,while_end.name);

        now_Block = while_context;
        continue_stack.add(while_begin.name);
        break_stack.add(while_end.name);
        visit(pointer.context);
        continue_stack.remove(continue_stack.size() - 1);
        break_stack.remove(break_stack.size() - 1);
        now_Block.out = new IR_Jump(while_begin.name);

        now_Block = while_end;
    }

    @Override
    void visit(Jump_Statement pointer) {
        if (pointer.meaning.equals("continue")){
            now_Block.out = new IR_Jump(continue_stack.get(continue_stack.size() - 1));
            now_Block = new_block(now_Block.name.name);
        } else {
            now_Block.out = new IR_Jump(break_stack.get(break_stack.size() - 1));
            now_Block = new_block(now_Block.name.name);
        }
    }

    @Override
    void visit(Return_Statement pointer) {
        visit(pointer.context);
        if (value.get(pointer.context) == null)
            now_Block.out = new IR_Return(new IR_Constant(0));
        else
            now_Block.out = new IR_Return(value.get(pointer.context));
        now_Block = new_block("aft_ret");
    }

    @Override
    void visit(Argument pointer) {
        IR_Argument t = find_Argument(pointer.name);
        if (pointer.have_initial_value)
        {
            visit(pointer.initial_value);
            if (value.get(pointer.initial_value) == null){
                now_Block.expression.add(new IR_Binary("move",t, new IR_Constant(0),new IR_Constant(0)));
            } else
                now_Block.expression.add(new IR_Binary("move",t, value.get(pointer.initial_value),new IR_Constant(0)));
        }
        if (now_Block.equals(root))
            t.is_global = true;
        else
            t.is_global = false;
        //System.out.printf(pointer.name + " " + pointer.is_str + "\n");
        t.is_string = pointer.is_str;
        value.put(pointer,t);
    }

    @Override
    void visit(Declaration_Expression pointer) {
        for (int i = 0 ; i < pointer.member.size(); i++) {
            Argument now = pointer.member.get(i);
            IR_Argument t = new_Argument(now.name,false);
            if (now_Block.equals(root))
                t.is_global = true;
            else
                t.is_global = false;
            t.is_string = pointer.member.get(i).is_str;
            if (now.have_initial_value)
            {
                visit(now.initial_value);
                if (value.get(now.initial_value) == null){
                    now_Block.expression.add(new IR_Binary("move",t, new IR_Constant(0),new IR_Constant(0)));
                } else
                    now_Block.expression.add(new IR_Binary("move",t, value.get(now.initial_value),new IR_Constant(0)));
            } else if (t.is_global){
                if (t.is_string)
                    now_Block.expression.add(new IR_Binary("move",t, new IR_String("\"\""),new IR_Constant(0)));
                else
                    now_Block.expression.add(new IR_Binary("move",t, new IR_Constant(0),new IR_Constant(0)));
            }
            //System.out.printf(t.name + " " + t.is_string + "\n");
        }
    }

    @Override
    void visit(Suffix_Expression pointer) {
        IR_Argument t = new_Argument("t", true);
        visit(pointer.expression);
        now_Block.expression.add(new IR_Binary("move",t,value.get(pointer.expression),new IR_Constant(0)));
        if (pointer.oper.equals("++")) {
            now_Block.expression.add(new IR_Binary("add",value.get(pointer.expression),value.get(pointer.expression), new IR_Constant(1)));
        } else
        if (pointer.oper.equals("--")){
            now_Block.expression.add(new IR_Binary("sub",value.get(pointer.expression),value.get(pointer.expression), new IR_Constant(1)));
        }
        t.is_string = pointer.is_str;
        value.put(pointer, t);
    }

    @Override
    void visit(Functional_Expression pointer) {
        if (pointer.is_string){
        } else
        if (pointer.is_array){
        } else {
            IR_Call t = new IR_Call();
            visit(pointer.expression);
            t.operator = ((Identifier_Expression) pointer.expression).name;
            for (int i = 0; i < pointer.argument.size(); i++) {
                visit(pointer.argument.get(i));
                t.parameter.add(value.get(pointer.argument.get(i)));
            }
            t.target = new_Argument(t.operator + "bv", true);
            now_Block.expression.add(t);
            t.target.is_string = pointer.is_str;
            value.put(pointer, t.target);
        }
    }

    @Override
    void visit(Subscript_Expression pointer) {
        visit(pointer.expression);
        IR_Argument p = value.get(pointer.expression);
        for (int i = 0 ; i < pointer.used_size; i++){
            IR_Argument q = new_Argument("M", true), r = new_Argument("@M", true), s = new_Argument("M", true);
            q.is_string = r.is_string = s.is_string = false;
            visit(pointer.argument.get(i));
            now_Block.expression.add(new IR_Binary("mul",s,value.get(pointer.argument.get(i)),new IR_Constant(4)));
            now_Block.expression.add(new IR_Binary("add",q,p,s));
            now_Block.expression.add(new IR_Binary("link",r,q,new IR_Constant(0)));
            p = r;
        }
        p.is_string = pointer.is_str;
        value.put(pointer,p);
    }

    @Override
    void visit(Member_Expression pointer) {
        visit(pointer.expression);
        Expression pp = pointer.member;
        if (pp instanceof Functional_Expression) {
            IR_Call t = new IR_Call();
            visit(((Functional_Expression) pp).expression);
            t.operator = ((Identifier_Expression) ((Functional_Expression) pp).expression).name;
            t.parameter.add(value.get(pointer.expression));
            for (int i = 0; i < ((Functional_Expression) pp).argument.size(); i++) {
                visit(((Functional_Expression) pp).argument.get(i));
                t.parameter.add(value.get(((Functional_Expression) pp).argument.get(i)));
            }
            t.target = new_Argument(t.operator + "bv", true);
            now_Block.expression.add(t);
            t.target.is_string = pointer.is_str;
            value.put(pointer, t.target);
        //    System.out.printf("1111\n");
            //    visit()
        } else if (pointer.member instanceof Identifier_Expression){
            Virtual_Type vir = pointer.val.type;
            Actual_Type act = ((Class_Type)vir).parameter.get(((Identifier_Expression) pointer.member).name);
            IR_Argument p = new_Argument("member",true);
            now_Block.expression.add(new IR_Binary("add",p,value.get(pointer.expression),new IR_Constant(act.num_in_class * 4)));
            IR_Argument r = new_Argument("@member",true);
            now_Block.expression.add(new IR_Binary("link",r,p,new IR_Constant(0)));
            r.is_string = pointer.is_str;
            value.put(pointer,r);
            //act.num_in_class
        }
    }

    @Override
    void visit(Prefix_Expression pointer) {
        if (pointer.oper.equals("new")){
             IR_Argument p = new_Argument("newspace",true);
            //        System.out.printf("0\n");
            if (pointer.expression instanceof Subscript_Expression){
                int t = ((Subscript_Expression) pointer.expression).used_size - 1;
                visit(((Subscript_Expression) pointer.expression).argument.get(t));
                now_Block.expression.add(new IR_Binary("alloc",p,value.get(((Subscript_Expression) pointer.expression).argument.get(t)), new IR_Constant(0)));
                p.is_string = pointer.is_str;
                value.put(pointer,p);
            } else{
                int num = pointer.par_num;
            //    System.out.printf(num + "");
                now_Block.expression.add(new IR_Binary("alloc", p, new IR_Constant(num), new IR_Constant(0)));
                p.is_string = pointer.is_str;
                value.put(pointer,p);
            }
        } else {
            visit(pointer.expression);
            IR_Argument now = value.get(pointer.expression);
            IR_Argument t = new_Argument("pre_exp(" + pointer.oper + ")", true);
            switch (pointer.oper) {
                case "++":
                    now_Block.expression.add(new IR_Binary("add", now, now, new IR_Constant(1)));
                    now.is_string = pointer.is_str;
                    value.put(pointer, now);
                    break;
                case "--":
                    now_Block.expression.add(new IR_Binary("sub", now, now, new IR_Constant(1)));
                    now.is_string = pointer.is_str;
                    value.put(pointer, now);
                    break;
                case "!":
                    now_Block.expression.add(new IR_Binary("xor", t, now, new IR_Constant(1)));
                    t.is_string = pointer.is_str;
                    now.is_mid_process = true;
                    value.put(pointer, t);
                    break;
                case "~":
                    now_Block.expression.add(new IR_Binary("not", t, now, new IR_Constant(0)));
                    t.is_string = pointer.is_str;
                    now.is_mid_process = true;
                    value.put(pointer, t);
                    break;
                case "-":
                    now_Block.expression.add(new IR_Binary("neg", t, now, new IR_Constant(-1)));
                    t.is_string = pointer.is_str;
                    now.is_mid_process = true;
                    value.put(pointer, t);
                    break;
                case "+":
                    now.is_string = pointer.is_str;
                    value.put(pointer, now);
                    break;
            }
        }
    }

    @Override
    void visit(Binary_Expression pointer) {
        visit(pointer.left_exp);
        IR_Argument t;
        if (pointer.oper.equals("%"))
            t = new_Argument("bi_exp(%%)", true);
        else
            t = new_Argument("bi_exp(" + pointer.oper +")", true);
        IR_Value l = value.get(pointer.left_exp),r = new IR_Constant(0);
        if (!(pointer.oper.equals("&&") || pointer.oper.equals("||"))) {
            visit(pointer.right_exp);
            r = value.get(pointer.right_exp);
            if (r == null) r = new IR_Constant(0);
        }
        if (l == null) l = new IR_Constant(0);
        switch (pointer.oper) {
            case "*":now_Block.expression.add(new IR_Binary("mul",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case "/": now_Block.expression.add(new IR_Binary("div",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case "%":now_Block.expression.add(new IR_Binary("rem",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case "+":now_Block.expression.add(new IR_Binary("add",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case "-":now_Block.expression.add(new IR_Binary("sub",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case "<<":now_Block.expression.add(new IR_Binary("shl",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case ">>":now_Block.expression.add(new IR_Binary("shr",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case "||": {
                IR_Block iff = new_block("or_false"),ift = new_block("or_true"), ife = new_block("or_end");

                now_Block.out = new IR_Branch(l, ift.name, iff.name);
                now_Block = ift;
                now_Block.expression.add(new IR_Binary("=", t, new IR_Constant(1), new IR_Constant(0)));
                now_Block.out = new IR_Jump(ife.name);
                now_Block = iff;
                visit(pointer.right_exp);
                r = value.get(pointer.right_exp);
                now_Block.expression.add(new IR_Binary("=", t, r, new IR_Constant(0)));
                now_Block.out = new IR_Jump(ife.name);

                now_Block = ife;
                break;
            }
            case "&":now_Block.expression.add(new IR_Binary("and",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case "&&": {
                IR_Block ift = new_block("and_true"), iff = new_block("and_false"), ife = new_block("and_end");

                now_Block.out = new IR_Branch(l, ift.name, iff.name);
                now_Block = iff;
                now_Block.expression.add(new IR_Binary("=", t, new IR_Constant(0), new IR_Constant(0)));
                now_Block.out = new IR_Jump(ife.name);

                now_Block = ift;
                visit(pointer.right_exp);
                r = value.get(pointer.right_exp);
                now_Block.expression.add(new IR_Binary("=", t, r, new IR_Constant(0)));
                now_Block.out = new IR_Jump(ife.name);

                now_Block = ife;
                break;
            }
            case"|":now_Block.expression.add(new IR_Binary("or",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case "^":now_Block.expression.add(new IR_Binary("xor",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case ">":now_Block.expression.add(new IR_Binary("sgt",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case "<":now_Block.expression.add(new IR_Binary("slt",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case ">=":now_Block.expression.add(new IR_Binary("sge",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case "<=":now_Block.expression.add(new IR_Binary("sle",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case "!=":now_Block.expression.add(new IR_Binary("sne",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
            case "==":now_Block.expression.add(new IR_Binary("seq",t,l,r));l.is_mid_process = r.is_mid_process = true;break;
        }
        t.is_string = pointer.is_str;
     //   System.out.printf(pointer.oper + " " + t.is_string + "\n");
        value.put(pointer,t);
    }

    @Override
    void visit(Int_Expression pointer) {
        IR_Argument t = new_Argument("int",true);
        now_Block.expression.add(new IR_Binary("=",t,new IR_Constant(pointer.value),new IR_Constant(0)));
        t.is_string = pointer.is_str;
        value.put(pointer,t);
    }

    @Override
    void visit(Bool_Expression pointer) {
        IR_Argument t = new_Argument("bool",true);
        now_Block.expression.add(new IR_Binary("=",t,new IR_Constant(pointer.value ? 1 : 0),new IR_Constant(0)));
        t.is_string = pointer.is_str;
        value.put(pointer,t);
    }

    @Override
    void visit(String_Expression pointer) {
        IR_Argument t = new_Argument("str", true);
        now_Block.expression.add(new IR_Binary("=",t,new IR_String(pointer.value),new IR_Constant(0)));
        t.is_string = pointer.is_str;
        value.put(pointer,t);
    }

    @Override
    void visit(Empty_Expression pointer) {

    }

    @Override
    void visit(Null_Expression pointer) {
     //   IR_Constant t = new IR_Constant(0);
    }

    @Override
    void visit(Type pointer) {

    }

    @Override
    void visit(Identifier_Expression pointer) {
        if (find_Argument(pointer.name) != null)
            pointer.is_str = find_Argument(pointer.name).is_string;
        //System.out.printf(pointer.name + " " + pointer.is_str + "\n");
        value.put(pointer,find_Argument(pointer.name));
    }

    @Override
    void visit(Assign_Expression pointer) {
        visit(pointer.left_exp);
        visit(pointer.right_exp);
        IR_Argument l = value.get(pointer.left_exp), r = value.get(pointer.right_exp);
        if (r == null) {
            l.is_mid_process = true;
            now_Block.expression.add(new IR_Binary("move", l, new IR_Constant(0), new IR_Constant(0)));
        }
        else {
            //      System.out.printf(r.name + "\n");
            l.is_mid_process = r.is_mid_process = true;
            now_Block.expression.add(new IR_Binary("move", l, r, new IR_Constant(0)));
        }
        value.put(pointer,l);
    }









    //load-store
    void load_store(){
        for (int i = 0 ; i < block_list.size(); i++) {
            IR_Block t = block_list.get(i);
            for (int j = 0; j < t.expression.size(); j++) {
                IR_Binary pp = t.expression.get(j);
                if (pp instanceof IR_Call){
                    IR_Call qq = (IR_Call)pp;
                    for (int k = 0; k < qq.parameter.size();k++){
                        IR_Value rr = qq.parameter.get(k);
                        if (rr instanceof IR_Argument && ((IR_Argument) rr).name.startsWith("@")) {
                            IR_Argument ss = new_Argument("ldst", true);
                            t.expression.add(j, new IR_Binary("load", ss, rr, new IR_Constant(0)));
                            j++;
                            qq.parameter.set(k,ss);
                        }
                    }
                } else {
                    if (pp.t1 instanceof IR_Argument && ((IR_Argument) pp.t1).name.startsWith("@")) {
                        IR_Argument qq = new_Argument("ldst", true);
                        t.expression.add(j, new IR_Binary("load", qq, pp.t1, new IR_Constant(0)));
                        j++;
                        pp.t1 = qq;
                    }
                    if (pp.t2 instanceof IR_Argument && ((IR_Argument) pp.t2).name.startsWith("@")) {
                        IR_Argument qq = new_Argument("ldst", true);
                        t.expression.add(j, new IR_Binary("load", qq, pp.t2, new IR_Constant(0)));
                        j++;
                        pp.t2 = qq;
                    }
                }
                if (pp.operator.equals("store") || pp.operator.equals("load")) continue;
                if (pp.target.name.startsWith("@")) {
                    if (pp.operator.equals("link"))
                        pp.operator = "move";
                    else {
                        IR_Argument qq = new_Argument("ldst", true);
                        t.expression.add(j + 1, new IR_Binary("store", pp.target, qq, new IR_Constant(0)));
                        j++;
                        pp.target = qq;
                    }
                }
            }
            if (t.out == null) continue;
            if (t.out instanceof IR_Branch){
                if (((IR_Branch) t.out).condition instanceof IR_Argument &&
                        ((IR_Argument) ((IR_Branch) t.out).condition).name.startsWith("@")){
                    IR_Argument q = new_Argument("ldst", true);
                    t.expression.add(new IR_Binary("load", q, ((IR_Branch) t.out).condition, new IR_Constant(0)));
                    ((IR_Branch) t.out).condition = q;
                }
            }
            else if (t.out instanceof IR_Return){
                if (((IR_Return) t.out).value instanceof IR_Argument &&
                        ((IR_Argument) ((IR_Return) t.out).value).name.startsWith("@")){
                    IR_Argument q = new_Argument("ldst", true);
                    t.expression.add(new IR_Binary("load", q, ((IR_Return) t.out).value, new IR_Constant(0)));
                    ((IR_Return) t.out).value = q;
                }
            }
        }
    }














    // Speeding upppppp!

    //del_none_used
    boolean del_unused_block(){
        boolean now = false;
        HashSet<IR_Block> used_B = new HashSet<>();
        List<IR_Block> list = new ArrayList<>();
        used_B.add(root);
        used_B.add(st);
        list.add(root);
        list.add(st);
        for (int i = 0 ; i < list.size(); i++) {
            IR_Block t = list.get(i);
            for (int j = 0; j < t.expression.size(); j++) {
                IR_Binary tmp = t.expression.get(j);
                if (tmp instanceof IR_Call){
                    if (func_map.get(tmp.operator) == null) continue;
                    if(used_B.add(block_map.get(func_map.get(tmp.operator).start)))
                        list.add(block_map.get(func_map.get(tmp.operator).start));
                }
            }
            if (t.out instanceof IR_Jump) {
                if (used_B.add(block_map.get(((IR_Jump) t.out).position)))
                    list.add(block_map.get(((IR_Jump) t.out).position));
            }
            else if (t.out instanceof IR_Branch) {
                if (used_B.add(block_map.get(((IR_Branch) t.out).false_position))) list.add(block_map.get(((IR_Branch) t.out).false_position));
                if (used_B.add(block_map.get(((IR_Branch) t.out).true_position))) list.add(block_map.get(((IR_Branch) t.out).true_position));
            }
        }
        for (int i = 0; i < block_list.size(); i++) {
            if (used_B.contains(block_list.get(i))) continue;
            block_list.remove(i);
            i--;
            now = true;
        }
        return now;
    }


    //del_unused_Argument
    boolean del_unused_Argu(){
        HashSet<IR_Argument> used_B = new HashSet<>();
        boolean now = false;
        boolean flag = true;
        while (flag) {
            used_B.clear();
            flag = false;
            for (int i = 0; i < block_list.size(); i++) {
                IR_Block t = block_list.get(i);
                for (int j = 0; j < t.expression.size(); j++) {
                    IR_Binary tmp = t.expression.get(j);
                    if (tmp instanceof IR_Call) {
                        for (int k = 0; k < ((IR_Call) tmp).parameter.size(); k++) {
                            if (((IR_Call) tmp).parameter.get(k) instanceof IR_Argument)
                                used_B.add((IR_Argument) ((IR_Call) tmp).parameter.get(k));
                        }
                    } else {
                        if (tmp.operator.equals("load") || tmp.operator.equals("store"))
                            used_B.add(tmp.target);
                        if (tmp.t1 instanceof IR_Argument)
                            used_B.add((IR_Argument) tmp.t1);
                        if (tmp.t2 instanceof IR_Argument)
                            used_B.add((IR_Argument) tmp.t2);
                    }
                }
                if (t.out instanceof IR_Branch)
                    used_B.add((IR_Argument) ((IR_Branch) t.out).condition);
                if (t.out instanceof IR_Return)
                    if (((IR_Return) t.out).value instanceof IR_Argument)
                        used_B.add((IR_Argument) ((IR_Return) t.out).value);
            }
            for (int i = 0; i < block_list.size(); i++) {
                IR_Block t = block_list.get(i);
                for (int j = 0; j < t.expression.size(); j++) {
                    if (t.expression.get(j) instanceof IR_Call) continue;
                    if (!used_B.contains(t.expression.get(j).target)){
              //          System.out.printf("del " + t.expression.get(j).target.name + "\n");
                        flag = true;
                        t.expression.remove(j);
                        j--;
                    }
                }
            }
            if (flag) now = true;
        }
        return now;
    }

    //replace constant
    boolean replace_const() {
        HashMap<IR_Argument, Long> const_map = new HashMap<>();
        HashMap<IR_Argument, IR_String> string_map = new HashMap<>();
        HashSet<IR_Argument> rubbish = new HashSet<>(),rubbish_str = new HashSet<>();
        boolean now = false;
        boolean flag = true;
        while (flag) {
            //       System.out.printf("SB!\n");
            const_map.clear();
            string_map.clear();
            rubbish.clear();
            rubbish_str.clear();
            flag = false;
            for (int i = 0; i < block_list.size(); i++) {
                IR_Block t = block_list.get(i);
                for (int j = 0; j < t.expression.size(); j++) {
                    IR_Binary tmp = t.expression.get(j);
                    if (!rubbish.contains(tmp.target)) {
                        long num;
                        boolean now_flag = true;
                        if (value_judge(tmp)) {
                            num = value_calc(tmp);
                            if (const_map.get(tmp.target) == null)
                                const_map.put(tmp.target, num);
                            else if (const_map.get(tmp.target) != num) now_flag = false;
                        } else now_flag = false;
                        if (!now_flag)
                            rubbish.add(tmp.target);
                    }
                    if (!rubbish_str.contains(tmp.target)) {
                        IR_String str;
                        boolean now_flag = true;
                        if (value_string_judge(tmp)) {
                            //     System.out.printf("SB!\n");
                            str = value_string_calc(tmp);
                            if (string_map.get(tmp.target) == null)
                                string_map.put(tmp.target, str);
                            else if (!string_map.get(tmp.target).equals(str)) now_flag = false;
                        } else now_flag = false;
                        if (!now_flag)
                            rubbish_str.add(tmp.target);
                    }
                }
            }
            for (int i = 0; i < block_list.size(); i++){
                IR_Block t = block_list.get(i);
                for (int j = 0; j < t.expression.size(); j++) {
                    IR_Binary tmp = t.expression.get(j);
                    if (tmp instanceof IR_Call){
                        for (int k = 0; k < ((IR_Call) tmp).parameter.size(); k ++) {
                            if ((((IR_Call) tmp).parameter.get(k) instanceof IR_Argument) && (const_map.get(((IR_Call) tmp).parameter.get(k)) != null) && (!rubbish.contains(((IR_Call) tmp).parameter.get(k)))) {
                                flag = true;
                                ((IR_Call) tmp).parameter.set(k, new IR_Constant(const_map.get(((IR_Call) tmp).parameter.get(k))));
                            }
                            if ((((IR_Call) tmp).parameter.get(k) instanceof IR_Argument) && (string_map.get(((IR_Call) tmp).parameter.get(k)) != null) && (!rubbish_str.contains(((IR_Call) tmp).parameter.get(k)))){
                                flag = true;
                                ((IR_Call) tmp).parameter.set(k, string_map.get(((IR_Call) tmp).parameter.get(k)));
                            }
                        }
                        //continue;
                    }
                    if (tmp.t1 instanceof IR_Argument && const_map.get(tmp.t1) != null && !rubbish.contains(tmp.t1)){
                        flag = true;
                        tmp.t1 = new IR_Constant(const_map.get(tmp.t1));
                    }
                    if (tmp.t2 instanceof IR_Argument && const_map.get((tmp.t2)) != null && !rubbish.contains(tmp.t2)){
                        flag = true;
                        tmp.t2 = new IR_Constant(const_map.get(tmp.t2));
                    }
                    if (tmp.t1 instanceof IR_Argument && string_map.get(tmp.t1) != null && !rubbish_str.contains(tmp.t1)){
                        flag = true;
                        tmp.t1 = string_map.get(tmp.t1);
                    }
                    if (tmp.t2 instanceof IR_Argument && string_map.get(tmp.t2) != null && !rubbish_str.contains(tmp.t2)){
                        flag = true;
                        tmp.t2 = string_map.get(tmp.t2);
                    }
                }
                if (t.out instanceof IR_Branch){
                    if (((IR_Branch) t.out).condition instanceof IR_Argument && const_map.get(((IR_Branch) t.out).condition) != null && !rubbish.contains(((IR_Branch) t.out).condition)){
                        flag = true;
                        ((IR_Branch) t.out).condition = new IR_Constant(const_map.get(((IR_Branch) t.out).condition));
                    }
                    if (((IR_Branch) t.out).condition instanceof IR_Argument && string_map.get(((IR_Branch) t.out).condition) != null && !rubbish_str.contains(((IR_Branch) t.out).condition)){
                        flag = true;
                        ((IR_Branch) t.out).condition = string_map.get(((IR_Branch) t.out).condition);
                    }
                    if (((IR_Branch) t.out).condition instanceof IR_Constant){
                        if (((IR_Constant) ((IR_Branch) t.out).condition).value == 0)
                            t.out = new IR_Jump(((IR_Branch) t.out).false_position);
                        else
                            t.out = new IR_Jump(((IR_Branch) t.out).true_position);
                    }
                }
                if (t.out instanceof IR_Return){
                    if(((IR_Return) t.out).value instanceof IR_Argument && const_map.get(((IR_Return)t.out).value) != null && !rubbish.contains(((IR_Return)t.out).value)){
                        flag = true;
                        ((IR_Return) t.out).value = new IR_Constant(const_map.get(((IR_Return) t.out).value));
                    }
                    if(((IR_Return) t.out).value instanceof IR_Argument && string_map.get(((IR_Return)t.out).value) != null && !rubbish_str.contains(((IR_Return)t.out).value)){
                        flag = true;
                        ((IR_Return) t.out).value = string_map.get(((IR_Return) t.out).value);
                    }
                }
            }
            if (flag) now = true;
        }
        return now;
    }

    boolean value_judge(IR_Binary tmp){
        if (tmp instanceof IR_Call) return false;
        if (tmp.operator.equals("alloc") || tmp.operator.equals("store") || tmp.operator.equals("load")) return false;
        if (tmp.t1 instanceof IR_Constant && tmp.t2 instanceof IR_Constant) return true;
        //   if (tmp.t1 instanceof IR_String && tmp.t1 instanceof IR_String ) return true;
        return false;
    }
    boolean value_string_judge(IR_Binary tmp){
        if (tmp instanceof IR_Call) return false;
        if (tmp.operator.equals("alloc") || tmp.operator.equals("store") || tmp.operator.equals("load")) return false;
        if (tmp.t1 instanceof IR_String && (tmp.operator.equals("=") || tmp.operator.equals("move") || (tmp.operator.equals("+") && tmp.t2 instanceof IR_String))) return true;
        //   if (tmp.t1 instanceof IR_String && tmp.t1 instanceof IR_String ) return true;
        return false;
    }
    IR_String value_string_calc(IR_Binary tmp){
        String t1 = ((IR_String)tmp.t1).name;
        if (tmp.operator.equals("=")) return new IR_String(t1);
        if (tmp.operator.equals("move")){tmp.operator = "="; return new IR_String(t1);}
        String t2 = ((IR_String)tmp.t2).name;
        return new IR_String(t1 + t2);
    }
    long value_calc(IR_Binary tmp) {
        long t1 = ((IR_Constant)tmp.t1).value, t2 = ((IR_Constant)tmp.t2).value;
        switch (tmp.operator){
            case "add" : return t1 + t2;
            case "sub" : return t1 - t2;
            case "mul" : return t1 * t2;
            case "div" : return t1 / t2;
            case "rem" : return t1 % t2;
            case "shl" : return t1 << t2;
            case "shr" : return t1 >> t2;
            case "and" : return t1 & t2;
            case "or" : return t1 | t2;
            case "xor" : return t1 ^ t2;
            case "sgt" : return t1 > t2 ? 1 : 0;
            case "slt" : return t1 < t2 ? 1 : 0;
            case "sge" : return t1 >= t2 ? 1 : 0;
            case "sle" : return t1 <= t2 ? 1 : 0;
            case "sne" : return t1 != t2 ? 1 : 0;
            case "seq" : return t1 == t2 ? 1 : 0;
            case "=" : return t1;
            case "neg" : return -t1;
            case "not" : return ~t1;
            case "move": tmp.operator = "=";return t1;
        }
        return 0;
    }

    //del_empty_jump_block
    boolean del_empty_jump_block(){
        boolean now = false;
        HashMap<IR_Label, IR_Label> empty_map = new HashMap<>();
        for (int i = 0 ; i < block_list.size(); i++){
            IR_Block t = block_list.get(i);
            if (t.expression.size() == 0 && t.out instanceof IR_Jump && !t.is_func_start)
                empty_map.put(t.name,((IR_Jump) t.out).position);
        }
        for (int i = 0; i < block_list.size(); i++) {
            IR_Block t = block_list.get(i);
            if (empty_map.get(t.name) != null) {
                block_list.remove(i);
                i--;
                now = true;
                continue;
            }
            if (t.out == null) continue;
            if (t.out instanceof IR_Jump)
                while (empty_map.get(((IR_Jump) t.out).position) != null)
                    ((IR_Jump) t.out).position = empty_map.get(((IR_Jump) t.out).position);
            else
            if (t.out instanceof IR_Branch){
                if (((IR_Branch) t.out).true_position != null)
                while (empty_map.get(((IR_Branch) t.out).true_position) != null){
                    ((IR_Branch) t.out).true_position = empty_map.get(((IR_Branch) t.out).true_position);
                }
                if (((IR_Branch) t.out).false_position != null)
                while (empty_map.get(((IR_Branch) t.out).false_position) != null){
                    ((IR_Branch) t.out).false_position = empty_map.get(((IR_Branch) t.out).false_position);
                }
            }
        }
        return now;
    }

    boolean combine_jump_block(){
        boolean now = false;
        for (int i = 0; i < block_list.size(); i++){
            IR_Block tmp = block_list.get(i);
            if (tmp.out instanceof IR_Jump){
                IR_Block towards = block_map.get(((IR_Jump) tmp.out).position);
                for (int j = 0; j < towards.expression.size(); j++)
                    if(towards.expression.get(j) instanceof IR_Call)
                        tmp.expression.add(new IR_Call((IR_Call)towards.expression.get(j)));
                    else
                        tmp.expression.add(new IR_Binary(towards.expression.get(j)));
                if (towards.out == null) continue;
                if (towards.out instanceof IR_Jump)
                    tmp.out = new IR_Jump((IR_Jump)towards.out);
                else if (towards.out instanceof IR_Return)
                    tmp.out = new IR_Return((IR_Return)towards.out);
                else
                    tmp.out = new IR_Branch((IR_Branch)towards.out);
                now = true;
            }
        }
        return now;
    }
    boolean replace_move_binary(){
        boolean now = false;
        for (int i = 0 ; i < block_list.size(); i++){
            IR_Block tmp = block_list.get(i);
            for (int j = 0; j < tmp.expression.size(); j++){
                IR_Binary binary = tmp.expression.get(j);
                if (binary.operator.equals("=") || binary.operator.equals("move") ){
                    boolean bo = true;
                    for (int k = j + 1; k < tmp.expression.size(); k++){
                        IR_Binary change = tmp.expression.get(k);
                        if (change instanceof IR_Call){
                            for (int l = 0; l < ((IR_Call) change).parameter.size();l++)
                                if (binary.target.equals(((IR_Call) change).parameter.get(l))) {
                                    ((IR_Call) change).parameter.set(l, binary.t1);
                               //     System.out.printf(change.target.name + "\n");
                                    now = true;
                                }
                          //  System.out.printf("----SB!\n");
                            bo = false;
                            break;
                        } else {
                            if (binary.target.equals(change.t1)) {
                        //        System.out.printf(change.target.name + "\n");
                                change.t1 = binary.t1;
                                now = true;
                            }
                            if (binary.target.equals(change.t2)) {
                       //         System.out.printf(change.target.name + "\n");
                                change.t2 = binary.t1;
                                now = true;
                            }
                            if (change.target.equals(binary.target) || change.target.equals(binary.t1)) {
                                bo = false;
                                break;
                            }
                        }
                    }
                    if (bo){
                        if (tmp.out instanceof IR_Return && binary.target.equals(((IR_Return) tmp.out).value)) {
                            now = true;
                            ((IR_Return) tmp.out).value = binary.t1;
                        }
                        if (tmp.out instanceof IR_Branch && binary.target.equals(((IR_Branch) tmp.out).condition)) {
                            ((IR_Branch) tmp.out).condition = binary.t1;
                            now = true;
                        }
                    }
                }
            }
        }
        return now;
    }

    boolean replace_move_up() {
        boolean now = false;
        HashMap prev = new HashMap();

        for(int i = 0; i < this.block_list.size(); ++i) {
            prev.clear();
            IR_Block tmp = (IR_Block)this.block_list.get(i);

            for(int j = 0; j < tmp.expression.size(); ++j) {
                IR_Binary binary = (IR_Binary)tmp.expression.get(j);
                if((binary.operator.equals("=") || binary.operator.equals("move")) && binary.t1 instanceof IR_Argument) {
                    IR_Argument p = (IR_Argument)binary.t1;
                    if(prev.containsKey(p)) {
                        IR_Argument t = binary.target;
                        binary = new IR_Binary((IR_Binary)prev.get(p));
                        binary.target = t;
                        now = true;
                        tmp.expression.set(j, binary);
                    }
                }
                if(!(binary instanceof IR_Call) && !binary.operator.equals("alloc") && !binary.operator.equals("load") && !binary.operator.equals("store") && !binary.target.equals(binary.t1) && !binary.target.equals(binary.t2))
                    prev.put(binary.target, binary);
            }
        }
        return now;
    }


    boolean remove_equal(){
        boolean now = false;
        for (int i = 0 ; i < block_list.size(); i++) {
            IR_Block tmp = block_list.get(i);
            for (int j = 0; j < tmp.expression.size(); j++) {
                IR_Binary binary = tmp.expression.get(j);
                if ((binary.operator.equals("=") || binary.operator.equals("move")) && binary.target.equals(binary.t1)) {
                    tmp.expression.remove(j);
                    j--;
                //    System.out.printf("SB!\n");
                    now = true;
                }
        //       System.out.printf("");
            }
        }
        return now;
    }














    //TRANSLATE MIPS
    //calc_func
    void calc_func(){
        HashSet<IR_Func> used_F = new HashSet<>();
        List<IR_Func> list = new ArrayList<>();
        HashSet<IR_Block> used_B = new HashSet<>();
        List<IR_Block> bl = new ArrayList<>();
        list.add(st_func);
        used_F.add(st_func);

        for (int i = 0 ; i < root.expression.size(); i++) {
            IR_Binary tmp = root.expression.get(i);
            argu_map.put(tmp.target,0);
            tmp.target.offset = 0;
        }
        for (int i = 0 ; i < list.size(); i++){
            IR_Func now_func = list.get(i);
            now_func.argu_num = 0;
            for (int j = 0; j < now_func.parameter.size(); j++){
                if (!now_func.parameter.get(j).is_global && !argu_map.containsKey((now_func.parameter.get(j)))){
                    argu_map.put(now_func.parameter.get(j), ++now_func.argu_num);
                    now_func.argu_map.put(now_func.parameter.get(j), now_func.argu_num);
                    now_func.parameter.get(j).offset = now_func.argu_num;
                }
            }
            IR_Block nb = block_map.get(now_func.start);
            bl.clear();
            bl.add(nb);
            used_B.add(nb);
            for (int j = 0 ; j < bl.size(); j++){
                nb = bl.get(j);
                for (int k = 0 ; k < nb.expression.size(); k++) {
                    if (nb.expression.get(k) instanceof IR_Call){
                        if (func_map.get(nb.expression.get(k).operator) != null && used_F.add(func_map.get(nb.expression.get(k).operator)))
                            list.add(func_map.get(nb.expression.get(k).operator));
                    }
                    if (!nb.expression.get(k).target.is_global && !argu_map.containsKey(nb.expression.get(k).target)) {
                  //      System.out.printf(nb.expression.get(k).target.name + "\n");
                        argu_map.put(nb.expression.get(k).target, ++now_func.argu_num);
                        now_func.argu_map.put(nb.expression.get(k).target, now_func.argu_num);
                        nb.expression.get(k).target.offset = now_func.argu_num;
                    }
                }
                if (nb.out instanceof IR_Jump) {
                    IR_Block tmp = block_map.get(((IR_Jump) nb.out).position);
                    if (used_B.add(tmp)) bl.add(tmp);
                }
                else if (nb.out instanceof IR_Branch){
                    IR_Block tmp = block_map.get(((IR_Branch) nb.out).true_position);
                    if (used_B.add(tmp)) bl.add(tmp);
                    tmp = block_map.get(((IR_Branch) nb.out).false_position);
                    if (used_B.add(tmp)) bl.add(tmp);
                }
            }
        }
    }






    //translate to MIPS....



    //Library
    private void save_reg(List<String> now, int reg){
        now.add("sw " + reg_name.get(reg) + ", " + (4 * reg) +"($sp)\n");
    }
    private void load_reg(List<String> now, int reg){
        now.add("lw " + reg_name.get(reg) + ", " + (4 * reg) +"($sp)\n");
    }
    private void move_reg(List<String> now, int a, int b){
       now.add("move " + reg_name.get(a) + ", " + reg_name.get(b) + "\n");
    }



    //load & save
    private void load_data(List<String> now,int reg, IR_Value data){
        if (data == null) return;
        if (data instanceof IR_Argument)
            load_data(now,reg,(IR_Argument)data);
        else
        if (data instanceof IR_Constant)
            load_data(now,reg,((IR_Constant) data));
        else
        if (data instanceof IR_String)
            load_data(now,reg,((IR_String) data));
    }
    private void save_data(List<String> now,int reg, IR_Value pos){
        if (pos == null) return;
        if (pos instanceof IR_Argument) save_data(now,reg,(IR_Argument)pos);
    }
    private void load_data(List<String> now,int reg, IR_Argument data){
        if (data == null) return;
        if (data.is_global){
            now.add("lw " + reg_name.get(reg) + ", " + "Global__" + data.name + "\n");
        } else
            load_data(now,reg,data.offset);
    }
    private void save_data(List<String> now,int reg, IR_Argument pos){
        if (pos == null) return;
        if (pos.is_global){
            now.add("sw " + reg_name.get(reg) + ", " + "Global__" + pos.name + "\n");
        } else
            save_data(now,reg,pos.offset);
    }
    private void load_data(List<String> now,int reg, int offset){
        now.add("lw " +  reg_name.get(reg) + ", " + offset * (-4) + "($gp)\n");
    }
    private void save_data(List<String> now,int reg, int offset){
        now.add("sw " +  reg_name.get(reg) + ", " + offset * (-4) + "($gp)\n");
    }
    private void load_data(List<String> now,int reg, IR_String data){
        if (data == null) return;
        if (!data.put_data && !global_map.containsKey(data.name) ) {
            data_block.add(".word " + data.getlength + "\n");
            data_block.add("Globals__" + (++global_num) + ": .asciiz " + data.name + "\n");
            data_block.add(".align 2\n");
            global_map.put(data.name,global_num);
            data.put_data = true;
        }
        now.add("la " + reg_name.get(reg) + ", " + "Globals__" + global_map.get(data.name) + "\n");
    }
    private void load_data(List<String> now,int reg, IR_Constant imme){
        if (imme == null) return;
        now.add("li "+ reg_name.get(reg) + ", " + imme.value + "\n");
    }
    private void save_data(List<String> now,IR_Constant imme,int offset){
        if (imme == null) return;
        //do- not - sure!!!!! $v1 can be used ?
        load_data(now,3,imme);
        save_data(now,3,offset);
    }

    private void save_data_to_reg(List<String> now,int reg_data,int reg_address) {
        now.add("sw "+ reg_name.get(reg_data) + ", 0(" + reg_name.get(reg_address) + ")\n");
    }
    private void load_data_from_reg(List<String> now,int reg_data,int reg_address) {
        now.add("lw "+ reg_name.get(reg_data) + ", 0(" + reg_name.get(reg_address) + ")\n");
    }

    private void trans_Jump(List<String> now, IR_Jump tmp){
        if (tmp.position == root.name)
            now.add("b main\n");
        else
            now.add("b " + tmp.position.name + "\n");
    }

    private void trans_Branch_CISC(List<String> now, IR_Branch tmp, int reg){
        load_data(now,reg,tmp.condition);
        now.add("beq " + reg_name.get(reg) + ", 1, " + tmp.true_position.name + "\n");
        now.add("beq " + reg_name.get(reg) + ", 0, " + tmp.false_position.name + "\n");
    }

    private void trans_Return_CISC(List<String> now, IR_Return tmp){
        load_data(now, 2, tmp.value);
        now.add("jr " + reg_name.get(31) + "\n");
    }

    void str_func(List<String> now,String name){
        save_reg(now, 28);
        save_reg(now, 31);
        now.add("jal " + name + "\n");
        load_reg(now, 28);
        load_reg(now, 31);
    }


    //translate to MIPS






    //CISC
    void trans_CISC(){
    root.name = new IR_Label("main");
    //    root.out = new IR_Jump(st.name);
    for (int i = 0 ; i < block_list.size(); i++){
        IR_Block t = block_list.get(i);
        t.MIPS.add(t.name.name + ":\n");
        for (int j = 0 ; j < t.expression.size(); j++)
            trans_Binary_CISC(t.MIPS,t.expression.get(j));
        if (t.out == null){trans_Return_CISC(t.MIPS, new IR_Return(new IR_Constant(0)));}
        else if (t.out instanceof IR_Jump)
            trans_Jump(t.MIPS, (IR_Jump) t.out);
        else if (t.out instanceof IR_Branch)
            trans_Branch_CISC(t.MIPS, (IR_Branch) t.out, 8);
        else if (t.out instanceof IR_Return)
            trans_Return_CISC(t.MIPS, (IR_Return) t.out);
        t.MIPS.add("\n");
    }
}

    void alloc_new_space_CISC(List<String> now,IR_Binary binary){
        load_data(now,4,binary.t1);
        now.add("add $a0, $a0, 1\n");
        now.add("mul $a0, $a0, 4\n");
        load_data(now,2,new IR_Constant(9));
        now.add("syscall\n");
        now.add("div $a0, $a0, 4\n");
        now.add("sub $a0, $a0, 1\n");
        now.add("sw $a0, 0($v0)\n");
        now.add("add $v0, $v0, 4\n");
        save_data(now,2,binary.target);
    }

    private void calc_3reg_CISC(List<String> now, String oper, int target, int t1, int t2){
        now.add( oper + " "+ reg_name.get(target) + ", " + reg_name.get(t1) + ", " + reg_name.get(t2) + "\n");
    }
    private void calc_2reg_1const_CISC(List<String> now, String oper, int target, int t1, long t2){
        now.add( oper + " "+ reg_name.get(target) + ", " + reg_name.get(t1) + ", " + t2 + "\n");
    }
    private void calc_2reg_CISC(List<String> now, String oper, int target, int t1){
        now.add(oper + " "+ reg_name.get(target) + ", " + reg_name.get(t1) + "\n");
    }

    private  void move_data_call_CISC(List<String> now, IR_Value from,IR_Argument towards){
        load_data(now,8,from);
        now.add("sw $t0, " + towards.offset * (-4) + "($sp)\n");
    }

    private void trans_Call_CISC(List<String> now, IR_Call binary){
        IR_Func now_func = func_map.get(binary.operator);


   //     now.add("CALL___(" + binary.operator + "):\n");
        save_reg(now, 31);

        if (now_func == null) {
            //.... 内置函数
            for (int i = 0 ; i < binary.parameter.size(); i++)
                load_data(now, i + 4,binary.parameter.get(i));
            String str = "";
       //     System.out.printf(binary.operator+"\n");
            switch (binary.operator){
                case "print":str = "func__print";break;
                case "println":str = "func__println";break;
                case "getInt":str = "func__getInt";break;
                case "getString":str = "func__getString";break;
                case "toString":str = "func__toString";break;
                case "length":str = "func__string.length";break;
                case "substring":str = "func__string.substring";break;
                case "parseInt":str = "func__string.parseInt";break;
                case "ord":str = "func__string.ord";break;
                case "size":str = "func__array.size";break;
            }
            now.add("jal " + str + "\n");
        } else {
            save_reg(now, 28);
            for (int i = 0; i < now_func.parameter.size(); i++)
                move_data_call_CISC(now, binary.parameter.get(i), now_func.parameter.get(i));
            move_reg(now, 28, 29);  ///sp<---fp
            now.add("subu $sp, $sp, " + (now_func.argu_num + 33) * 4 + "\n");
            now.add("jal " + now_func.start.name + "\n");
            move_reg(now, 29, 28);
            load_reg(now, 28);
        }
        load_reg(now, 31);
        save_data(now, 2, binary.target);
    }


    private void trans_Binary_CISC(List<String> now, IR_Binary binary){
        if (binary instanceof IR_Call){
            trans_Call_CISC(now, (IR_Call)binary);
        } else if (binary.operator.equals("alloc")){
            alloc_new_space_CISC(now,binary);
        } else if (binary.operator.equals("load")){
            load_data(now,8,binary.t1);
            load_data_from_reg(now, 9, 8);
            save_data(now,9,binary.target);
        } else if (binary.operator.equals("store")){
            load_data(now,8,binary.t1);
            load_data(now,9,binary.target);
            save_data_to_reg(now, 8, 9);
        }
        else{
            load_data(now, 8, binary.t1);
            if ((!binary.t1.is_string) && (binary.t2 instanceof IR_Constant)){
                long p = ((IR_Constant) binary.t2).value;
                switch (binary.operator){
                    case "move":    break;
                    case "=":       break;

                    case "add": calc_2reg_1const_CISC(now,"add",8,8,p); break;
                    case "sub": calc_2reg_1const_CISC(now,"sub",8,8,p); break;
                    case "mul": calc_2reg_1const_CISC(now,"mul",8,8,p); break;
                    case "div": calc_2reg_1const_CISC(now,"div",8,8,p); break;
                    case "rem": calc_2reg_1const_CISC(now,"rem",8,8,p); break;

                    case "and": calc_2reg_1const_CISC(now,"and",8,8,p); break;
                    case "nor": calc_2reg_1const_CISC(now,"nor",8,8,p); break;
                    case "or": calc_2reg_1const_CISC(now,"or",8,8,p); break;
                    case "xor": calc_2reg_1const_CISC(now,"xor",8,8,p); break;
                    case "shl": calc_2reg_1const_CISC(now,"sll",8,8,p); break;
                    case "shr": calc_2reg_1const_CISC(now,"sra",8,8,p); break;

                    case "slt": calc_2reg_1const_CISC(now,"slt",8,8,p); break;
                    case "sgt": calc_2reg_1const_CISC(now,"sgt",8,8,p); break;
                    case "sle": calc_2reg_1const_CISC(now,"sle",8,8,p); break;
                    case "sge": calc_2reg_1const_CISC(now,"sge",8,8,p); break;
                    case "seq": calc_2reg_1const_CISC(now,"seq",8,8,p); break;
                    case "sne": calc_2reg_1const_CISC(now,"sne",8,8,p); break;

                    case "neg": calc_2reg_CISC(now,"neg",8,8);break;
                    case "not": calc_2reg_CISC(now,"not",8,8);break;
                }
                save_data(now,8,binary.target);
            } else
            {
            if (!(binary.operator.equals("=") || binary.operator.equals("move") ||binary.operator.equals("neg") || binary.operator.equals("not"))) load_data(now, 9, binary.t2);
            if (binary.t1.is_string){
                if (binary.operator.equals("move") || binary.operator.equals("=")){
                    save_data(now,8,binary.target);
                } else {
                    move_reg(now, 4, 8);
                    move_reg(now, 5, 9);
                    switch (binary.operator) {
                        case "add":
                            str_func(now, "func__stringConcatenate");
                            break;
                        case "seq":
                            str_func(now, "func__stringIsEqual");
                            break;
                        case "slt":
                            str_func(now, "func__stringLess");
                            break;
                        case "sle":
                            str_func(now, "func__stringLeq");
                            break;
                        case "sge":
                            str_func(now, "func__stringGeq");
                            break;
                        case "sne":
                            str_func(now, "func__stringNeq");
                            break;
                        case "sgt":
                            str_func(now, "func__stringLarge");
                            break;
                    }
                    save_data(now, 2, binary.target);
                }
            } else{
                switch (binary.operator){
                    case "move":    break;
                    case "=":       break;
                    case "add": calc_3reg_CISC(now,"add",8,8,9); break;
                    case "sub": calc_3reg_CISC(now,"sub",8,8,9); break;
                    case "mul": calc_3reg_CISC(now,"mul",8,8,9); break;
                    case "div": calc_3reg_CISC(now,"div",8,8,9); break;
                    case "rem": calc_3reg_CISC(now,"rem",8,8,9); break;

                    case "and": calc_3reg_CISC(now,"and",8,8,9); break;
                    case "nor": calc_3reg_CISC(now,"nor",8,8,9); break;
                    case "or": calc_3reg_CISC(now,"or",8,8,9); break;
                    case "xor": calc_3reg_CISC(now,"xor",8,8,9); break;
                    case "shl": calc_3reg_CISC(now,"sll",8,8,9); break;
                    case "shr": calc_3reg_CISC(now,"sra",8,8,9); break;

                    case "slt": calc_3reg_CISC(now,"slt",8,8,9); break;
                    case "sgt": calc_3reg_CISC(now,"sgt",8,8,9); break;
                    case "sle": calc_3reg_CISC(now,"sle",8,8,9); break;
                    case "sge": calc_3reg_CISC(now,"sge",8,8,9); break;
                    case "seq": calc_3reg_CISC(now,"seq",8,8,9); break;
                    case "sne": calc_3reg_CISC(now,"sne",8,8,9); break;

                    case "neg": calc_2reg_CISC(now,"neg",8,8);break;
                    case "not": calc_2reg_CISC(now,"not",8,8);break;
                }
                save_data(now,8,binary.target);
            }
            }
        }
        if (binary.target.is_global) {
            if (global_set.add(binary.target.name)) {
                data_block.add("Global__" + binary.target.name + ": .space 4" + "\n");
                data_block.add(".align 2\n");
            }
        }
     //   if (binary.target.name.equals("M(1_3)")) System.out.printf(binary.target.offset + "\n");
    }








    //SIMI_RISC




    List<Integer> reg_used_page, changed_sign;
    HashMap<Integer, IR_Value> reg_para_name;
    int now_reg = 0;

    private void RISC_init(){
        now_reg = 25;
        reg_para_name = new HashMap<>();
        reg_used_page = new ArrayList<>();
        changed_sign = new ArrayList<>();
        for (int i = 0; i <= 100; i++){
            reg_used_page.add(i,0);
            reg_para_name.put(i,new IR_Constant(0));
            changed_sign.add(i,0);
        }
    }

    void reg_clear_RISC(List<String> now, int reg){
        //not finished yet
        if (reg_used_page.get(reg) == 1) {
            if (reg_para_name.get(reg) instanceof IR_Argument && changed_sign.get(reg) == 1) {
                save_data(now, reg, reg_para_name.get(reg));
            }
        }
        changed_sign.set(reg,0);
   //     if (reg_para_name.get(reg) instanceof IR_Argument && ((IR_Argument) reg_para_name.get(reg)).is_global && reg > 14) return;
        reg_para_name.put(reg, new IR_Constant(0));
        reg_used_page.set(reg, 0);
    }
    private void save_used_reg_RISC(List<String> now, boolean is_func_call){
        for (int i = 8; i <= 27; i++)
            if(reg_used_page.get(i) == 1) {
                IR_Value val = reg_para_name.get(i);
                if (val instanceof IR_Argument && changed_sign.get(i) == 1 && (!val.is_mid_process || ((IR_Argument) val).is_data || is_func_call))
                    save_data(now, i, reg_para_name.get(i));
                changed_sign.set(i, 0);
                reg_para_name.put(i, new IR_Constant(0));
                reg_used_page.set(i, 0);
            }
    }

    private boolean have_to_use_reg_RISC(List<String> now, int reg){
        if (changed_sign.get(reg) == 0) return false;
        //haven't finished yet
        return true;
    }

    private int get_best_reg_RISC(int ban1,int ban2){
        //haven't finished yet
        for (int i = 8; i <= 25; i++) {
            if (i == ban1 || i == ban2) continue;
            if (changed_sign.get(i) == 0) return i;
        }
        now_reg++;
        if (now_reg > 25) now_reg = 8;
        while (now_reg == ban1 || now_reg == ban2) {
            now_reg++;
            if (now_reg > 25) now_reg = 8;
        }
        return now_reg;
    }
    private int find_new_reg_RISC(List<String> now,int ban1, int ban2) {
        for (int i = 8; i <= 25; i++) {
            if (i == ban1 || i == ban2) continue;
            if (reg_used_page.get(i) == 0 || !(reg_para_name.get(i) instanceof IR_Argument))
                return i;
        }
        int t = get_best_reg_RISC(ban1, ban2);
        if (have_to_use_reg_RISC(now, t)) {
            save_data(now,t,reg_para_name.get(t));
        }
        return t;
    }
    private void changed_reg_RISC(int reg){
        changed_sign.set(reg, 1);
    }
    private void sign_reg_RISC(List<String> now,int reg, IR_Value val){
        reg_used_page.set(reg, 1);
        reg_para_name.put(reg,val);
        //      if (val instanceof IR_Argument)
        //     System.out.printf(((IR_Argument) val).name + "\n");
    }
    private int read_reg_RISC(List<String> now, IR_Value val, int ban1, int ban2){
        for (int i = 8; i <= 25; i++)
            if (reg_used_page.get(i) == 1 && reg_para_name.get(i).equals(val))
                return i;
        int t = find_new_reg_RISC(now, ban1, ban2);
        load_data(now,t,val);
        sign_reg_RISC(now,t,val);
        return t;
    }
    private int find_reg_RISC(List<String> now, IR_Value val, int ban1, int ban2){
        if (reg_para_name.get(ban1).equals(val))
            return ban1;
        if (reg_para_name.get(ban2).equals(val))
            return ban2;
        int t = find_new_reg_RISC(now, ban1, ban2);
        sign_reg_RISC(now,t,val);
        return t;
    }

    void trans_RISC(){
        RISC_init();
        root.name = new IR_Label("main");
        //    root.out = new IR_Jump(st.name);
        for (int i = 0 ; i < block_list.size(); i++){
            IR_Block t = block_list.get(i);
            t.MIPS.add(t.name.name + ":\n");
            for (int j = 0 ; j < t.expression.size(); j++)
                trans_Binary_RISC(t.MIPS,t.expression.get(j));
            save_used_reg_RISC(t.MIPS,false);
            if (t.out == null){trans_Return_RISC(t.MIPS, new IR_Return(new IR_Constant(0)));}
            else if (t.out instanceof IR_Jump)
                trans_Jump(t.MIPS, (IR_Jump) t.out);
            else if (t.out instanceof IR_Branch)
                trans_Branch_RISC(t.MIPS, (IR_Branch) t.out);
            else if (t.out instanceof IR_Return)
                trans_Return_RISC(t.MIPS, (IR_Return) t.out);
            t.MIPS.add("\n");
        }
    }
    private void calc_2reg_RISC(List<String> now, String oper, int target, int t1){
        now.add(oper + " "+ reg_name.get(target) + ", " + reg_name.get(t1) + "\n");
        changed_reg_RISC(target);
    }
    private void calc_2reg_1const_RISC(List<String> now, String oper, int target, int t1, long t2){
        if (t2 == 0){
            now.add( oper + " "+ reg_name.get(target) + ", " + reg_name.get(t1) + ", $zero" + "\n");
        } else if (t2 == 4){
            now.add( oper + " "+ reg_name.get(target) + ", " + reg_name.get(t1) + ", " + reg_name.get(26) + "\n");
        } else if (t2 == 1){
            now.add( oper + " "+ reg_name.get(target) + ", " + reg_name.get(t1) + ", " + reg_name.get(27) + "\n");
        } else
        now.add( oper + " "+ reg_name.get(target) + ", " + reg_name.get(t1) + ", " + t2 + "\n");
        changed_reg_RISC(target);
    }
    private void calc_3reg_RISC(List<String> now, String oper, int target, int t1, int t2){
        now.add( oper + " "+ reg_name.get(target) + ", " + reg_name.get(t1) + ", " + reg_name.get(t2) + "\n");
        changed_reg_RISC(target);
    }
    private void trans_Return_RISC(List<String> now, IR_Return tmp){
        int t = read_reg_RISC(now, tmp.value,0 ,0);
        move_reg(now,2,t);
        reg_clear_RISC(now,t);
        now.add("jr " + reg_name.get(31) + "\n");
    }

    private void trans_Branch_RISC(List<String> now, IR_Branch tmp){
        int t = read_reg_RISC(now,tmp.condition,0, 0);
        now.add("beq " + reg_name.get(t) + ", 1, " + tmp.true_position.name + "\n");
        now.add("beq " + reg_name.get(t) + ", $zero, " + tmp.false_position.name + "\n");
    }


    private void alloc_new_space_RISC(List<String> now,IR_Binary binary){
        boolean bo = false;
        for (int i = 8; i <= 25; i++)
            if (reg_used_page.get(i) == 1 && reg_para_name.get(i).equals(binary.t1)) {
                move_reg(now, 4, i);
                bo = true;
                break;
            }
        if (binary.t1 instanceof IR_Constant) {
            long con = ((IR_Constant) binary.t1).value;
            long con2 = con * 4 + 4;
            now.add("li $a0, " + con2 + "\n");
            now.add("li $v0, 9\n");
            now.add("syscall\n");
            now.add("li $a0, " + con + "\n");
            now.add("sw $a0, 0($v0)\n");
            now.add("add $v0, $v0, 4\n");
            save_data(now, 2, binary.target);
        }
        else{
            if (!bo) load_data(now, 4, binary.t1);
            now.add("add $a0, $a0, 1\n");
            now.add("mul $a0, $a0, 4\n");
            load_data(now, 2, new IR_Constant(9));
            now.add("syscall\n");
            now.add("div $a0, $a0, 4\n");
            now.add("sub $a0, $a0, 1\n");
            now.add("sw $a0, 0($v0)\n");
            now.add("add $v0, $v0, 4\n");
            save_data(now, 2, binary.target);
        }
    }

    private  void move_data_call_RISC(List<String> now, IR_Value from,IR_Argument towards){
        int t = read_reg_RISC(now,from,0,0);
        now.add("sw "+ reg_name.get(t) + ", " + towards.offset * (-4) + "($sp)\n");
    }

    private void trans_Call_RISC(List<String> now, IR_Call binary){
        IR_Func now_func = func_map.get(binary.operator);
        //     now.add("CALL___(" + binary.operator + "):\n");
        save_reg(now, 31);

        if (now_func == null) {
            //.... 内置函数
        //    System.out.printf(binary.operator + " " + binary.parameter.size() + "\n");

            save_used_reg_RISC(now,true);
            for (int i = 0 ; i < binary.parameter.size(); i++) {
             //   System.out.printf((i + 4) + "\n");
                load_data(now, i + 4, binary.parameter.get(i));
            }
            String str = "";
            //     System.out.printf(binary.operator+"\n");
            switch (binary.operator){
                case "print":str = "func__print";break;
                case "println":str = "func__println";break;
                case "getInt":str = "func__getInt";break;
                case "getString":str = "func__getString";break;
                case "toString":str = "func__toString";break;
                case "length":str = "func__string.length";break;
                case "substring":str = "func__string.substring";break;
                case "parseInt":str = "func__string.parseInt";break;
                case "ord":str = "func__string.ord";break;
                case "size":str = "func__array.size";break;
            }
            now.add("jal " + str + "\n");
        } else {
            save_reg(now, 28);
            for (int i = 0; i < now_func.parameter.size(); i++)
                move_data_call_RISC(now, binary.parameter.get(i), now_func.parameter.get(i));
            save_used_reg_RISC(now,true);
            move_reg(now, 28, 29);  ///sp<---fp
            now.add("subu $sp, $sp, " + (now_func.argu_num + 33) * 4 + "\n");
            now.add("jal " + now_func.start.name + "\n");
            move_reg(now, 29, 28);
            load_reg(now, 28);
        }
        load_reg(now, 31);
        save_data(now, 2, binary.target);
    }

    private void trans_Binary_RISC(List<String> now, IR_Binary binary){
        now.add("#binary\n");
        if (binary instanceof IR_Call){
            trans_Call_RISC(now, (IR_Call)binary);
        } else if (binary.operator.equals("alloc")){
            alloc_new_space_RISC(now, binary);
        } else if (binary.operator.equals("load")){
            int reg_1 = read_reg_RISC(now, binary.t1,0,0);
            int reg_2 = find_reg_RISC(now,binary.target,reg_1,0);
            load_data_from_reg(now, reg_2, reg_1);
            changed_reg_RISC(reg_2);
        } else if (binary.operator.equals("store")){
            int reg_1 = read_reg_RISC(now,binary.t1, 0,0), reg_2 = read_reg_RISC(now, binary.target, reg_1,0);
            save_data_to_reg(now, reg_1, reg_2);
        }
        else{
            int reg_1 = read_reg_RISC(now, binary.t1, 0, 0),reg_2 = 0, tar = 0;
            if ((!binary.t1.is_string) && (binary.t2 instanceof IR_Constant)){
                long p = ((IR_Constant) binary.t2).value;
                reg_2 = find_reg_RISC(now,binary.target,reg_1,0);
                switch (binary.operator){
                    case "move":
                    case "=":
                        if (reg_1 != reg_2) {
                            move_reg(now, reg_2, reg_1);
                            changed_reg_RISC(reg_2);
                        }
                        break;
                    case "add": calc_2reg_1const_RISC(now,"add",reg_2,reg_1,p); break;
                    case "sub": calc_2reg_1const_RISC(now,"sub",reg_2,reg_1,p); break;
                    case "mul": calc_2reg_1const_RISC(now,"mul",reg_2,reg_1,p); break;
                    case "div": calc_2reg_1const_RISC(now,"div",reg_2,reg_1,p); break;
                    case "rem": calc_2reg_1const_RISC(now,"rem",reg_2,reg_1,p); break;

                    case "and": calc_2reg_1const_RISC(now,"and",reg_2,reg_1,p); break;
                    case "nor": calc_2reg_1const_RISC(now,"nor",reg_2,reg_1,p); break;
                    case "or": calc_2reg_1const_RISC(now,"or",reg_2,reg_1,p); break;
                    case "xor": calc_2reg_1const_RISC(now,"xor",reg_2,reg_1,p); break;
                    case "shl": calc_2reg_1const_RISC(now,"sll",reg_2,reg_1,p); break;
                    case "shr": calc_2reg_1const_RISC(now,"sra",reg_2,reg_1,p);break;

                    case "slt": calc_2reg_1const_RISC(now,"slt",reg_2,reg_1,p);break;
                    case "sgt": calc_2reg_1const_RISC(now,"sgt",reg_2,reg_1,p); break;
                    case "sle": calc_2reg_1const_RISC(now,"sle",reg_2,reg_1,p); break;
                    case "sge": calc_2reg_1const_RISC(now,"sge",reg_2,reg_1,p); break;
                    case "seq": calc_2reg_1const_RISC(now,"seq",reg_2,reg_1,p); break;
                    case "sne": calc_2reg_1const_RISC(now,"sne",reg_2,reg_1,p); break;

                    case "neg": calc_2reg_RISC(now,"neg",reg_2,reg_1);break;
                    case "not": calc_2reg_RISC(now,"not",reg_2,reg_1);break;
                }
            } else
            {
                if (!(binary.operator.equals("=") || binary.operator.equals("move") ||binary.operator.equals("neg") || binary.operator.equals("not")))
                    reg_2 = read_reg_RISC(now, binary.t2, reg_1, 0);
                if (binary.t1.is_string){
                    if (binary.operator.equals("move") || binary.operator.equals("=")){
                        tar = find_reg_RISC(now,binary.target,reg_1,0);
                        if (reg_1 != tar) {
                            move_reg(now, tar, reg_1);
                            changed_reg_RISC(tar);
                        }
                    } else {
                        move_reg(now, 4, reg_1);
                        move_reg(now, 5, reg_2);
                        save_used_reg_RISC(now,true);
                        switch (binary.operator) {
                            case "add":
                                str_func(now, "func__stringConcatenate");
                                break;
                            case "seq":
                                str_func(now, "func__stringIsEqual");
                                break;
                            case "slt":
                                str_func(now, "func__stringLess");
                                break;
                            case "sle":
                                str_func(now, "func__stringLeq");
                                break;
                            case "sge":
                                str_func(now, "func__stringGeq");
                                break;
                            case "sne":
                                str_func(now, "func__stringNeq");
                                break;
                            case "sgt":
                                str_func(now, "func__stringLarge");
                                break;
                        }
                        tar = find_reg_RISC(now,binary.target,0,0);
                        move_reg(now,tar,2);
                        changed_reg_RISC(tar);
                    }
                } else{
                    tar = find_reg_RISC(now,binary.target,reg_1,reg_2);
                    switch (binary.operator){
                        case "move":
                        case "=":
                            if (tar != reg_1) {
                                move_reg(now, tar, reg_1);
                                changed_reg_RISC(tar);
                            }
                            break;
                        case "add": calc_3reg_RISC(now,"add",tar,reg_1,reg_2); break;
                        case "sub": calc_3reg_RISC(now,"sub",tar,reg_1,reg_2); break;
                        case "mul": calc_3reg_RISC(now,"mul",tar,reg_1,reg_2); break;
                        case "div": calc_3reg_RISC(now,"div",tar,reg_1,reg_2); break;
                        case "rem": calc_3reg_RISC(now,"rem",tar,reg_1,reg_2); break;

                        case "and": calc_3reg_RISC(now,"and",tar,reg_1,reg_2); break;
                        case "nor": calc_3reg_RISC(now,"nor",tar,reg_1,reg_2); break;
                        case "or": calc_3reg_RISC(now,"or",tar,reg_1,reg_2); break;
                        case "xor": calc_3reg_RISC(now,"xor",tar,reg_1,reg_2); break;
                        case "shl": calc_3reg_RISC(now,"sll",tar,reg_1,reg_2); break;
                        case "shr": calc_3reg_RISC(now,"sra",tar,reg_1,reg_2); break;

                        case "slt": calc_3reg_RISC(now,"slt",tar,reg_1,reg_2); break;
                        case "sgt": calc_3reg_RISC(now,"sgt",tar,reg_1,reg_2); break;
                        case "sle": calc_3reg_RISC(now,"sle",tar,reg_1,reg_2); break;
                        case "sge": calc_3reg_RISC(now,"sge",tar,reg_1,reg_2); break;
                        case "seq": calc_3reg_RISC(now,"seq",tar,reg_1,reg_2); break;
                        case "sne": calc_3reg_RISC(now,"sne",tar,reg_1,reg_2); break;

                        case "neg": calc_2reg_RISC(now,"neg",tar,reg_1);break;
                        case "not": calc_2reg_RISC(now,"not",tar,reg_1);break;
                    }
                }
            }
        }
        if (binary.target.is_global) {
            if (global_set.add(binary.target.name)) {
                data_block.add("Global__" + binary.target.name + ": .space 4" + "\n");
                data_block.add(".align 2\n");
            }
        }
        //   if (binary.target.name.equals("M(1_3)")) System.out.printf(binary.target.offset + "\n");
    }



    void opt_MIPS_sw(){
        for (int i = 0; i < block_list.size(); i++){
            IR_Block t = block_list.get(i);
            for (int j = 0; j < t.MIPS.size(); j++){
                String s = t.MIPS.get(j);
                if (s.startsWith("sw")) {
                //    System.out.printf(s);
                    int num = get_pos_MIPS(s);
                    if (num == 1) continue;
                    String reg = get_reg_MIPS(s);
                    if (reg.equals("$gp") && no_use_sw_MIPS(t,j,num))
                    {
                        t.MIPS.remove(j);
                        j--;
                    }
                }
            }
        }
    }

    private int get_pos_MIPS(String str){
        String now = "";
        int k = 0;
        for (int i = 0; i < str.length(); i++)
            if (str.charAt(i) == ','){
                k = i;
                break;
            }
        if (str.charAt(k + 2) != '-' && (str.charAt(k + 2) < '0' || str.charAt(k + 2) > '9')) return 1;
        for (int i = k + 2; i < str.length(); i++){
            if (str.charAt(i) == '(')
                return Integer.valueOf(now);
            now = now + str.charAt(i);
        }
        return Integer.valueOf(now);
    }

    private String get_reg_MIPS(String str){
        String now = "";
        int k = 0;
        for (int i = 0; i < str.length(); i++)
            if (str.charAt(i) == '('){
                k = i;
                break;
            }
        for (int i = k + 1; i < str.length(); i++){
            if (str.charAt(i) == ')')
                return now;
            now = now + str.charAt(i);
        }
        return now;
    }

    boolean no_use_sw_MIPS(IR_Block start_block, int start_pos, int save_pos){
        List<IR_Block> list = new ArrayList<>();
        HashSet<IR_Block> used = new HashSet<>();
        used.clear();
        list.clear();
        for (int i = start_pos + 1; i < start_block.MIPS.size(); i++){
            String str = start_block.MIPS.get(i);
            if (str.startsWith("lw") && get_pos_MIPS(str) == save_pos && get_reg_MIPS(str).equals("$gp"))
                return false;
            if (str.startsWith("sw") && get_pos_MIPS(str) == save_pos && get_reg_MIPS(str).equals("$gp"))
                return true;
        }
        if (start_block.out == null) return true;
        if (start_block.out instanceof IR_Branch){
            if (used.add(block_map.get(((IR_Branch)start_block.out).true_position))) list.add(block_map.get(((IR_Branch)start_block.out).true_position));
            if (used.add(block_map.get(((IR_Branch)start_block.out).false_position))) list.add(block_map.get(((IR_Branch)start_block.out).false_position));
        }
        else if (start_block.out instanceof IR_Jump)
            if (used.add(block_map.get(((IR_Jump)start_block.out).position))) list.add(block_map.get(((IR_Jump)start_block.out).position));
        int lt = 0;
        while (lt < list.size()){
            IR_Block t = list.get(lt);
            boolean bo = true;
            for (int i = 0; i < t.MIPS.size(); i++){
                String str = t.MIPS.get(i);
                if (str.startsWith("lw") && get_pos_MIPS(str) == save_pos && get_reg_MIPS(str).equals("$gp"))
                    return false;
                if (str.startsWith("sw") && get_pos_MIPS(str) == save_pos && get_reg_MIPS(str).equals("$gp")) {
                    bo = false;
                    break;
                }
            }
            if (bo) {
                if (t.out instanceof IR_Branch) {
                    if (used.add(block_map.get(((IR_Branch) t.out).true_position)))
                        list.add(block_map.get(((IR_Branch) t.out).true_position));
                    if (used.add(block_map.get(((IR_Branch) t.out).false_position)))
                        list.add(block_map.get(((IR_Branch) t.out).false_position));
                } else if (t.out instanceof IR_Jump)
                    if (used.add(block_map.get(((IR_Jump) t.out).position)))
                        list.add(block_map.get(((IR_Jump) t.out).position));
            }lt++;
        }
        return true;
    }





































    //MIPS- printer
    void printer_MIPS(){
        root.MIPS.remove(root.MIPS.size() - 2);
        root.MIPS.add(root.MIPS.size() - 1,"li $k0, 4\n");
        root.MIPS.add(root.MIPS.size() - 1,"li $k1, 1\n");
        root.MIPS.add(root.MIPS.size() - 1,"move $gp, $sp\n");
        root.MIPS.add(root.MIPS.size() - 1,"subu $sp, $sp " + (st_func.argu_num + 34) * 4 + "\n");
        root.MIPS.add(root.MIPS.size() - 1,"subu $gp, $gp, 4\n");
        root.MIPS.add(root.MIPS.size() - 1,"sw $ra, 4($gp)\n");
        root.MIPS.add(root.MIPS.size() - 1,"jal " + st.name.name + "\n");
        root.MIPS.add(root.MIPS.size() - 1,"lw $ra, 4($gp)\n");
        root.MIPS.add(root.MIPS.size() - 1,"jr $ra\n");
        for (int i = 0 ; i < block_list.size(); i++){
            for (int j = 0 ; j < block_list.get(i).MIPS.size(); j++)
                System.out.printf(block_list.get(i).MIPS.get(j));
        }
        for (int i = 0; i < data_block.size(); i++)
            System.out.printf(data_block.get(i));
    }
}