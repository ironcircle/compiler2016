import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.io.*;
import java.util.HashMap;
import java.util.List;

public class Getstart {
    public static void main(String[] args) throws Exception {
      //   new Getstart().compile(new FileInputStream("p.txt"), System.out);
        new Getstart().compile(System.in, System.out);
    }
    public void compile(InputStream is, OutputStream output) throws Exception {
       ANTLRInputStream input = new ANTLRInputStream(/*System.in*/ is);
        ComplierLexer lexer = new ComplierLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ComplierParser parser = new ComplierParser(tokens);
        ParseTree tree = parser.program(); // calc is the starting rule
//        System.out.println("LISP:");
//       System.out.println(tree.toStringTree(parser));
//      System.out.println();

//        System.out.println("Listener:");
        ParseTreeWalker walker = new ParseTreeWalker();
        IC_Listener evalByListener = new IC_Listener();
        walker.walk(evalByListener, tree);
        Program Root = evalByListener.root;

      //  Printer_visitor;
//      Printer t = new Printer();
 //      t.visit(Root);

     //Fisrt_Round;
        First_Round FR= new First_Round();
        Exitcode exit_code = FR.exitcode;
        FR.initial();
        FR.visit(Root);
        if (!exit_code.check()) System.exit(1);

     //Second_Round
        Second_Round SR = new Second_Round();
        SR.initial(FR);
        if (!exit_code.check()) System.exit(1);
        SR.visit(Root);
        if (!exit_code.check()) System.exit(1);
     //   System.out.printf("!!!\n");


     //Third_Round
        Third_Round TR = new Third_Round();
        TR.initial(SR);
        if (!exit_code.check()) System.exit(1);
        TR.visit(Root);
        if (!exit_code.check()) System.exit(1);
 //       exit_code.outp();



     //AST --> IR
        Translate tr_IR = new Translate();
        tr_IR.init();
        tr_IR.visit(Root);


     // load-store
        tr_IR.load_store();



    //    System.out.printf("SB!\n");


        boolean flag = true;
        while (flag) {
            flag = tr_IR.replace_const();
            //   System.out.printf("SB!\n");
            boolean flag2 = true;
            while(flag2) {
                flag2 = false;
                flag2 = tr_IR.del_unused_block();
                //   System.out.printf("SB!\n");
                flag2 = flag2 || tr_IR.del_unused_Argu();
                //    System.out.printf("SB!\n");
                flag2 = flag2 || tr_IR.del_empty_jump_block();
                //tr_IR.reduce_immediate_move();
                flag2 = flag2 || tr_IR.combine_jump_block();
                //IR_Printer
                flag = flag || flag2;
            }
            flag = flag || tr_IR.replace_move_binary();

            flag = flag || tr_IR.replace_move_up();

            flag = flag || tr_IR.remove_equal();
        }
  /*      HashMap<String,IR_Func> func_map = tr_IR.func_map;
        Printer_IR printerIR = new Printer_IR();
        printerIR.init(func_map);
        List<IR_Block> block_list = tr_IR.block_list;
        for (int i = 0 ; i < block_list.size(); i++)
            printerIR.visit(block_list.get(i));*/


        tr_IR.calc_func();
    //    tr_IR.sign_useless_var();
        tr_IR.trans_RISC();
        tr_IR.opt_MIPS_sw();

        //MIPS-Printer
        FileReader yyr_MIPS = new FileReader("buildinfunctions.txt");
        BufferedReader br=new BufferedReader(yyr_MIPS);
        String line = "";
        int now_line = 0;
        while ((line = br.readLine()) != null) tr_IR.root.MIPS.add(now_line++,line + "\n");
        tr_IR.root.MIPS.add(now_line,"\n");
        tr_IR.printer_MIPS();
        System.exit(0);
    }
}