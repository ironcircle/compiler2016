import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 章 on 2016/4/7.
 */
public abstract class Type_Style {
    abstract Actual_Type get_Actual();
    abstract boolean is_Virtual();
    abstract Actual_Type get_Actual(int x);
    abstract Actual_Type get_Actual(boolean bo, int x);
    abstract Virtual_Type get_Virtual();
}

abstract class Virtual_Type extends Type_Style{
    String name;
    Actual_Type get_Actual() {
        return new Actual_Type(this);
    }
    Actual_Type get_Actual(int x){Actual_Type cur = new Actual_Type(this.get_Actual()); cur.true_size = x;return cur;}
    Actual_Type get_Actual(boolean bo,int x){Actual_Type cur = new Actual_Type(this.get_Actual()); cur.num_in_class = x;return cur;}
    boolean is_Virtual(){ return true;}
    boolean equal(Virtual_Type p){
        if (p == null) return false;
        return name.equals(p.name);
    }
    Virtual_Type get_Virtual(){return this;}
}
class Class_Type extends Virtual_Type{
 //   String name;
    int par_num;
    HashMap<String, Actual_Type> parameter = new HashMap<String, Actual_Type>();
    Class_Type(String new_name){
        name = new_name;
        HashMap<String, Actual_Type> parameter = new HashMap<String, Actual_Type>();
    }
    Class_Type(){}
    int add_parameter(String name, Type_Style type, int n){
            if (parameter.get(name) != null)
                return 1;
            else
                parameter.put(name, type.get_Actual(true, n));
        return 0;
    }
}
class Default_Type extends Virtual_Type{
 //   String name;
    Default_Type(String add_name){name = add_name;}
}
class Function_Type extends Virtual_Type{
//    String name;
    Actual_Type return_Type;
    List<Actual_Type> parameter = new ArrayList<Actual_Type>();
    List<String> parameter_name = new ArrayList<String>();
}

class Actual_Type extends  Type_Style {
    Virtual_Type type;
    int true_size = 0;
    int num_in_class = 0;
    Actual_Type get_Actual(){return this;}
    Actual_Type get_Actual(int x){Actual_Type cur = new Actual_Type(this); cur.true_size = x;return cur;}
    Actual_Type get_Actual(boolean bo,int x){Actual_Type cur = new Actual_Type(this); cur.num_in_class = x;return cur;}
    Actual_Type(Virtual_Type now_type){type = now_type;}
    Actual_Type(Actual_Type now_type){type = now_type.type; true_size = now_type.true_size;}
    Actual_Type(){}
    boolean equal(Actual_Type p){
        if (p == null) return false;
        if (!type.equal(p.type)) return false;
        if (true_size != p.true_size) return false;
        return true;
    }
    boolean is_Virtual(){return (true_size == 0);}
    Virtual_Type get_Virtual(){return type;}
}