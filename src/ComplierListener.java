// Generated from Complier.g4 by ANTLR 4.5
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ComplierParser}.
 */
public interface ComplierListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ComplierParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(ComplierParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(ComplierParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(ComplierParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(ComplierParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#program_detail}.
	 * @param ctx the parse tree
	 */
	void enterProgram_detail(ComplierParser.Program_detailContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#program_detail}.
	 * @param ctx the parse tree
	 */
	void exitProgram_detail(ComplierParser.Program_detailContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(ComplierParser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(ComplierParser.DeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#class_declaration}.
	 * @param ctx the parse tree
	 */
	void enterClass_declaration(ComplierParser.Class_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#class_declaration}.
	 * @param ctx the parse tree
	 */
	void exitClass_declaration(ComplierParser.Class_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#noneinit_declaration}.
	 * @param ctx the parse tree
	 */
	void enterNoneinit_declaration(ComplierParser.Noneinit_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#noneinit_declaration}.
	 * @param ctx the parse tree
	 */
	void exitNoneinit_declaration(ComplierParser.Noneinit_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#noneinit_identifiers}.
	 * @param ctx the parse tree
	 */
	void enterNoneinit_identifiers(ComplierParser.Noneinit_identifiersContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#noneinit_identifiers}.
	 * @param ctx the parse tree
	 */
	void exitNoneinit_identifiers(ComplierParser.Noneinit_identifiersContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#function_definition}.
	 * @param ctx the parse tree
	 */
	void enterFunction_definition(ComplierParser.Function_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#function_definition}.
	 * @param ctx the parse tree
	 */
	void exitFunction_definition(ComplierParser.Function_definitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#parameters}.
	 * @param ctx the parse tree
	 */
	void enterParameters(ComplierParser.ParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#parameters}.
	 * @param ctx the parse tree
	 */
	void exitParameters(ComplierParser.ParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(ComplierParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(ComplierParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#init_Identifiers}.
	 * @param ctx the parse tree
	 */
	void enterInit_Identifiers(ComplierParser.Init_IdentifiersContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#init_Identifiers}.
	 * @param ctx the parse tree
	 */
	void exitInit_Identifiers(ComplierParser.Init_IdentifiersContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#init_Identifier}.
	 * @param ctx the parse tree
	 */
	void enterInit_Identifier(ComplierParser.Init_IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#init_Identifier}.
	 * @param ctx the parse tree
	 */
	void exitInit_Identifier(ComplierParser.Init_IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#type_specifier}.
	 * @param ctx the parse tree
	 */
	void enterType_specifier(ComplierParser.Type_specifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#type_specifier}.
	 * @param ctx the parse tree
	 */
	void exitType_specifier(ComplierParser.Type_specifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#pair_Bracket}.
	 * @param ctx the parse tree
	 */
	void enterPair_Bracket(ComplierParser.Pair_BracketContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#pair_Bracket}.
	 * @param ctx the parse tree
	 */
	void exitPair_Bracket(ComplierParser.Pair_BracketContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(ComplierParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(ComplierParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#simple_statement}.
	 * @param ctx the parse tree
	 */
	void enterSimple_statement(ComplierParser.Simple_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#simple_statement}.
	 * @param ctx the parse tree
	 */
	void exitSimple_statement(ComplierParser.Simple_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#expression_statement}.
	 * @param ctx the parse tree
	 */
	void enterExpression_statement(ComplierParser.Expression_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#expression_statement}.
	 * @param ctx the parse tree
	 */
	void exitExpression_statement(ComplierParser.Expression_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#compound_statement}.
	 * @param ctx the parse tree
	 */
	void enterCompound_statement(ComplierParser.Compound_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#compound_statement}.
	 * @param ctx the parse tree
	 */
	void exitCompound_statement(ComplierParser.Compound_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#selection_statement}.
	 * @param ctx the parse tree
	 */
	void enterSelection_statement(ComplierParser.Selection_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#selection_statement}.
	 * @param ctx the parse tree
	 */
	void exitSelection_statement(ComplierParser.Selection_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code while_statement}
	 * labeled alternative in {@link ComplierParser#iteration_statement}.
	 * @param ctx the parse tree
	 */
	void enterWhile_statement(ComplierParser.While_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code while_statement}
	 * labeled alternative in {@link ComplierParser#iteration_statement}.
	 * @param ctx the parse tree
	 */
	void exitWhile_statement(ComplierParser.While_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code for_statement}
	 * labeled alternative in {@link ComplierParser#iteration_statement}.
	 * @param ctx the parse tree
	 */
	void enterFor_statement(ComplierParser.For_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code for_statement}
	 * labeled alternative in {@link ComplierParser#iteration_statement}.
	 * @param ctx the parse tree
	 */
	void exitFor_statement(ComplierParser.For_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#jump_statement}.
	 * @param ctx the parse tree
	 */
	void enterJump_statement(ComplierParser.Jump_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#jump_statement}.
	 * @param ctx the parse tree
	 */
	void exitJump_statement(ComplierParser.Jump_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code binary_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBinary_expression(ComplierParser.Binary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code binary_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBinary_expression(ComplierParser.Binary_expressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functional_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFunctional_expression(ComplierParser.Functional_expressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functional_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFunctional_expression(ComplierParser.Functional_expressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Member_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMember_expression(ComplierParser.Member_expressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Member_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMember_expression(ComplierParser.Member_expressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code value_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterValue_expression(ComplierParser.Value_expressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code value_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitValue_expression(ComplierParser.Value_expressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bracket_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBracket_expression(ComplierParser.Bracket_expressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bracket_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBracket_expression(ComplierParser.Bracket_expressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code suffix_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSuffix_expression(ComplierParser.Suffix_expressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code suffix_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSuffix_expression(ComplierParser.Suffix_expressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subscript_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSubscript_expression(ComplierParser.Subscript_expressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subscript_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSubscript_expression(ComplierParser.Subscript_expressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code prefix_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPrefix_expression(ComplierParser.Prefix_expressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code prefix_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPrefix_expression(ComplierParser.Prefix_expressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assign_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAssign_expression(ComplierParser.Assign_expressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assign_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAssign_expression(ComplierParser.Assign_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#arguments}.
	 * @param ctx the parse tree
	 */
	void enterArguments(ComplierParser.ArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#arguments}.
	 * @param ctx the parse tree
	 */
	void exitArguments(ComplierParser.ArgumentsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code int_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 */
	void enterInt_value(ComplierParser.Int_valueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code int_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 */
	void exitInt_value(ComplierParser.Int_valueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bool_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 */
	void enterBool_value(ComplierParser.Bool_valueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bool_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 */
	void exitBool_value(ComplierParser.Bool_valueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code string_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 */
	void enterString_value(ComplierParser.String_valueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code string_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 */
	void exitString_value(ComplierParser.String_valueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code null_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 */
	void enterNull_value(ComplierParser.Null_valueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code null_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 */
	void exitNull_value(ComplierParser.Null_valueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identifier_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier_value(ComplierParser.Identifier_valueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identifier_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier_value(ComplierParser.Identifier_valueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code type_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 */
	void enterType_value(ComplierParser.Type_valueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code type_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 */
	void exitType_value(ComplierParser.Type_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link ComplierParser#bracket_clude_expression}.
	 * @param ctx the parse tree
	 */
	void enterBracket_clude_expression(ComplierParser.Bracket_clude_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link ComplierParser#bracket_clude_expression}.
	 * @param ctx the parse tree
	 */
	void exitBracket_clude_expression(ComplierParser.Bracket_clude_expressionContext ctx);
}