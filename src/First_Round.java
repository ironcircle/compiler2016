import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 章 on 2016/4/7.
 */
public class First_Round extends Checker{
    //default = 0, class = 1, function = 2;
    void initial(){
        add_default_type("int");
        add_default_type("bool");
        add_default_type("void");
        add_default_type("string");
        List<Actual_Type> now = new ArrayList<>();
        List<String> now_name = new ArrayList<>();
        add_function_type_for_string(map.get("int").get_Actual(),"length",now,now_name);;
        add_function_type_for_string(map.get("int").get_Actual(),"parseInt",now,now_name);
        add_function_type(map.get("string").get_Actual(),"getString",now,now_name);
        add_function_type(map.get("int").get_Actual(),"getInt",now,now_name);
        now = new ArrayList<>();
        now_name = new ArrayList<>();
        now.add(map.get("string").get_Actual());
        now_name.add("str");
        add_function_type(map.get("void").get_Actual(),"print", now,now_name);
        add_function_type(map.get("void").get_Actual(),"println", now,now_name);
        now = new ArrayList<>();
        now_name = new ArrayList<>();
        now.add(map.get("int").get_Actual());
        now_name.add("i");
        add_function_type(map.get("string").get_Actual(),"toString",now,now_name);
        now_name = new ArrayList<>();
        now_name.add("pos");
        add_function_type_for_string(map.get("int").get_Actual(),"ord",now,now_name);
        now = new ArrayList<>();
        now_name = new ArrayList<>();
        now.add(map.get("int").get_Actual());
        now.add(map.get("int").get_Actual());
        now_name.add("left");
        now_name.add("right");
        add_function_type_for_string(map.get("string").get_Actual(),"substring", now,now_name);
    }
    @Override
    void visit(Program pointer) {
        for (int i = 0; i < pointer.list.size(); i++) {
            visit(pointer.list.get(i));
            if (exitcode.code != 0) return;
        }
    }

    @Override
    void visit(Functional_Declaration pointer) {
        return;
    }

    @Override
    void visit(Class_Declaration pointer) {
        if (map.get(pointer.name) != null) {
            exitcode.code = 1;
            exitcode.name = pointer.name;
        }
        else
            map.put(pointer.name,new Class_Type(pointer.name));
    }

    @Override
    void visit(Selection_Statement pointer) {
    }

    @Override
    void visit(Block_Statement pointer) {
        return;
    }

    @Override
    void visit(For_Statement pointer) {

    }

    @Override
    void visit(While_Statement pointer) {

    }

    @Override
    void visit(Jump_Statement pointer) {

    }

    @Override
    void visit(Return_Statement pointer) {

    }

    @Override
    void visit(Argument pointer) {

    }
    @Override
    void visit(Declaration_Expression pointer) {

    }

    @Override
    void visit(Suffix_Expression pointer) {

    }

    @Override
    void visit(Functional_Expression pointer) {

    }

    @Override
    void visit(Subscript_Expression pointer) {

    }

    @Override
    void visit(Member_Expression pointer) {
    }

    @Override
    void visit(Prefix_Expression pointer) {

    }

    @Override
    void visit(Binary_Expression pointer) {

    }

    @Override
    void visit(Int_Expression pointer) {

    }

    @Override
    void visit(Bool_Expression pointer) {

    }

    @Override
    void visit(String_Expression pointer) {

    }

    @Override
    void visit(Empty_Expression pointer) {

    }

    @Override
    void visit(Null_Expression pointer) {

    }

    @Override
    void visit(Type pointer) {

    }

    @Override
    void visit(Identifier_Expression pointer) {

    }

    @Override
    void visit(Assign_Expression pointer) {

    }
}
