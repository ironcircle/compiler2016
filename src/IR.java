import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 章 on 2016/4/30.
 */
abstract public class IR {
    abstract void accept(IR_Visitor v);
}
class IR_Block extends IR{
    int income = 0;

    boolean is_func_start = false;
    IR_Label name;
    List<String> MIPS = new ArrayList<>();
    List<IR_Binary> expression = new ArrayList<>();
    IR_Control out;
    void accept(IR_Visitor v){ v.visit(this);}
}
class IR_Label extends IR{
    String name;
    IR_Label (String Name){name = Name;}
    void accept(IR_Visitor v){ v.visit(this);}
}
class IR_Binary extends IR{
    String operator;
    IR_Argument target;
    IR_Value t1, t2;
    IR_Binary (String p, IR_Argument t, IR_Value p1, IR_Value p2){
        operator = p;
        target = t;
        t1 = p1;
        t2 = p2;
    }
    IR_Binary(IR_Binary t){
        operator = t.operator;
        target = t.target;
        t1 = t.t1;
        t2 = t.t2;
    }
    IR_Binary(){}
    void accept(IR_Visitor v){ v.visit(this);}
}
abstract class IR_Control extends IR{
    void accept(IR_Visitor v){}
}
class IR_Call extends IR_Binary{
    List<IR_Value> parameter = new ArrayList<>();
    IR_Call(IR_Call t){
        operator = t.operator;
        target = t.target;
        t1 = t.t1;
        t2 = t.t2;
        parameter = new ArrayList<>();
        for (int i = 0; i < t.parameter.size(); i++) parameter.add(t.parameter.get(i));
    }
    IR_Call(){};
    void accept(IR_Visitor v){ v.visit(this);}
}

class IR_Func extends IR{
    String name;
    List<IR_Argument> parameter = new ArrayList<>();
    IR_Label start;
    int argu_num = 0;
    HashMap<IR_Argument,Integer> argu_map = new HashMap<>();
    void accept(IR_Visitor v){ v.visit(this);}
}
class IR_Jump extends IR_Control{
    IR_Label position;
    IR_Jump(IR_Jump jump){position = jump.position;}
    IR_Jump(IR_Label name){
        position = name;
    }
    void accept(IR_Visitor v){ v.visit(this);}
}
class IR_Branch extends IR_Control{
    IR_Value condition;
    IR_Label true_position;
    IR_Label false_position;
    IR_Branch(IR_Value cond,IR_Label t, IR_Label f){
        condition = cond;
        true_position = t;
        false_position = f;
    }
    IR_Branch(IR_Branch br){
        condition = br.condition;
        true_position = br.true_position;
        false_position = br.false_position;
    }
    void accept(IR_Visitor v){ v.visit(this);}
}
class IR_Return extends IR_Control{
    IR_Value value;
    IR_Return (IR_Return re){
        value = re.value;
    }
    IR_Return (IR_Value v){value = v;}
    void accept(IR_Visitor v){ v.visit(this);}
}
abstract class IR_Value extends IR{
    void accept(Visitor v){}
    boolean is_string;
    boolean is_mid_process = false;
}
class IR_Constant extends IR_Value{
    long value;
    IR_Constant(long p){value = p;is_string = false;is_mid_process = true;}
    void accept(IR_Visitor v){ v.visit(this);}
}
class IR_String extends IR_Value{
    String name;
    int getlength = 0;
    boolean put_data = false;
    String true_name = "";
    IR_String(String p) {
        name = p;
        is_string = true;
        int now = name.length();
        getlength = now;
        true_name = "";
        is_mid_process = true;
        put_data = false;
        for (int i = 0 ;i < now; i ++) {
            if (name.charAt(i) == '\\') getlength--;
            if (i != 0 && i != now - 1)
                true_name = true_name + name.charAt(i);
        }
        getlength -= 2;
    }
    void accept(IR_Visitor v){ v.visit(this);}
}
class IR_Argument extends IR_Value{
    boolean is_data;
    boolean is_global = false;
    String name;
    int offset = 0;
    String get_addr(){
        return offset * (-4) + "$fp";
    }
    IR_Argument(boolean b,String a){
        is_string = false;
        is_data = b;
        is_mid_process = false;
        name = a;
    }
    void accept(IR_Visitor v){ v.visit(this);}
}

