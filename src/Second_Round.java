import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 章 on 2016/4/7.
 */
public class Second_Round extends Checker{
    Class_Type now_Class;
    void initial(First_Round FR){
        map = FR.map;
        string_map = FR.string_map;
        exitcode = FR.exitcode;
    }
    @Override
    void visit(Program pointer) {
        for (int i = 0; i<pointer.list.size(); i++) {
            visit(pointer.list.get(i));
            if (exitcode.code != 0) return;
        }
    }
    @Override
    void visit(Functional_Declaration pointer) {
        Actual_Type now_type = find_type(pointer.type);
        List<Actual_Type> parameter_type = new ArrayList<>() ;
        List<String> parameter_name = new ArrayList<>();
        if (exitcode.code == 0) {
            for (int i = 0; i < pointer.parameter.size(); i++) {
                Argument new_parameter = pointer.parameter.get(i);
                Actual_Type new_parameter_type = find_type(new_parameter.type);
                if (exitcode.code == 0){
                    parameter_name.add(new_parameter.name);
                    parameter_type.add(new_parameter_type);
                } else break;
            }
        }
        if (exitcode.code == 0)
            add_function_type(now_type,pointer.name,parameter_type,parameter_name);
        if (exitcode.code != 0)
            exitcode.name = exitcode.name + " Function " + pointer.name;
    }

    @Override
    void visit(Class_Declaration pointer) {
        Class_Type cur = (Class_Type)map.get(pointer.name);
        //now_Class = cur;
        //System.out.printf(pointer.member.size()+"");
        for (int i = 0; i < pointer.member.size(); i++) {
            Argument now = pointer.member.get(i);
            Type_Style now_type = find_type(now.type);
            if (exitcode.code == 0) exitcode.code = cur.add_parameter(now.name, now_type, i);
            if (exitcode.code != 0){
                exitcode.name = exitcode.name + "At Class " + pointer.name;
                return ;
            }
        }
        cur.par_num = pointer.member.size();
     //   System.out.printf(pointer.member.get(i).name);
        map.remove(pointer.name);
        map.put(pointer.name, cur);
    }

    @Override
    void visit(Selection_Statement pointer) {

    }

    @Override
    void visit(Block_Statement pointer) {

    }

    @Override
    void visit(For_Statement pointer) {

    }

    @Override
    void visit(While_Statement pointer) {

    }

    @Override
    void visit(Jump_Statement pointer) {

    }

    @Override
    void visit(Return_Statement pointer) {

    }

    @Override
    void visit(Argument pointer) {

    }
    @Override
    void visit(Declaration_Expression pointer) {

    }

    @Override
    void visit(Suffix_Expression pointer) {

    }

    @Override
    void visit(Functional_Expression pointer) {

    }

    @Override
    void visit(Subscript_Expression pointer) {

    }

    @Override
    void visit(Member_Expression pointer) {
    }

    @Override
    void visit(Prefix_Expression pointer) {

    }

    @Override
    void visit(Binary_Expression pointer) {

    }

    @Override
    void visit(Int_Expression pointer) {

    }

    @Override
    void visit(Bool_Expression pointer) {

    }

    @Override
    void visit(String_Expression pointer) {

    }

    @Override
    void visit(Empty_Expression pointer) {

    }

    @Override
    void visit(Null_Expression pointer) {

    }

    @Override
    void visit(Type pointer) {

    }

    @Override
    void visit(Identifier_Expression pointer) {

    }

    @Override
    void visit(Assign_Expression pointer) {

    }
}

