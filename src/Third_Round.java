import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by 章 on 2016/4/7.
 */
public class Third_Round extends Checker {
    List<HashMap<String, Type_Style> > type_page = new ArrayList<>();
    List<Actual_Type> block_Type = new ArrayList<>();
    HashMap<Object,Actual_Type> values = new HashMap<>();
    int break_Statement = 0;
    boolean lvalue = false;
    boolean string_function = false, array_function = false;
    boolean make_new = false,is_function_block = false;
    void initial(Second_Round SR){
        lvalue = false;
        type_page = new ArrayList<>();
        type_page.add(SR.map);
        string_map = SR.string_map;
        exitcode =SR.exitcode;
        values = new HashMap<>();
        make_new = false;
        break_Statement = 0;
        string_function = false;
        array_function = false;
        is_function_block = false;
        if (type_page.get(0).get("main") == null) {
            get_error(9, "");
            return;
        } else{
            Virtual_Type cur = type_page.get(0).get("main").get_Virtual();
            if (!(cur instanceof Function_Type)) {
                get_error(9, "");
                return;
            }
            if (((Function_Type) cur).parameter.size() != 0 || !(((Function_Type) cur).return_Type.type.name.equals("int"))){
                get_error(9,"");
                return;
            }
        }

    }

    Actual_Type make_type(Type type){
        Actual_Type cur = new Actual_Type();
        cur.type = find_type(type.name).get_Virtual();
        cur.true_size = type.true_size;
        return cur;
    }



    //get/find/add type to/from the type_page
    Type_Style get_type(String now){
        int deep = type_page.size() - 1;
        while (deep >= 0 && type_page.get(deep).get(now) == null) deep--;
        if (deep < 0)
            return null;
        else
            return type_page.get(deep).get(now);
    }  // the whole map
    Type_Style get_type(String now, int x){
        return type_page.get(type_page.size() - 1).get(now);
    } // only one map
    Type_Style find_type_in_string_map(String now){
        if (string_map.get(now) == null) {
            exitcode.code = 2;
            exitcode.name = now + " At";
            return null;
        }
        return string_map.get(now);
    }
    Actual_Type find_type(String now){
        if (get_type(now) == null) {
            exitcode.code = 2;
            exitcode.name = now + " At";
            return null;
        }
        return new Actual_Type(get_type(now).get_Actual());
    }
    Actual_Type find_type(Type now){
        if (get_type(now.name) == null) {
            exitcode.code = 2;
            exitcode.name = now.name + " At";
            return null;
        }
        return new Actual_Type(get_type(now.name).get_Actual(now.true_size));
    }
    Type_Style find_type_any(Type now){
        if (get_type(now.name) == null) {
            exitcode.code = 2;
            exitcode.name = now.name + " At";
            return null;
        }
        return get_type(now.name);
    }
    Type_Style find_type_any(String now){
        if (get_type(now) == null) {
            exitcode.code = 2;
            exitcode.name = now + " At";
            return null;
        }
        return get_type(now);
    }
    void add_type(String name, Actual_Type type) {
        if (exitcode.code != 0) return;
        if (get_type(name,0) != null){
            exitcode.code = 1;
            exitcode.name = name + " At";
            return;
        }
        type_page.get(type_page.size() - 1).put(name,type);
    }


    //compare-two-type
    boolean compare_type(Actual_Type type1, String type2){
        if (type1 == null) return false;
        return (type1.equal(find_type(type2)));
    }
    boolean compare_type(Type_Style type1, String type2){
        if (type1 == null) return false;
        return (type1.get_Actual().equal(find_type(type2)));
    }
    boolean compare_type(String type1, String type2){
        if (find_type(type1) == null) return false;
        return (find_type(type1).equal(find_type(type2)));
    }
    boolean compare_type(Type_Style type1, Type_Style type2){
        if (type1 == null) return false;
        if (type2 == null) return false;
        return (type1.get_Actual().equal(type2.get_Actual()));
    }



    //check- lvalue
    void check_lvalue(){
        if (!lvalue) {
            exitcode.code = 7;
            exitcode.name = " At";
        }
    }
  /*  void check_new(){
        if (make_new) {
            exitcode.code = 8;
            exitcode.name = " At";
        }
    }*/



    //get-in/outof the Block
    void getInBlock(Actual_Type type){
        block_Type.add(type);
        type_page.add(new HashMap<>());
    }
    void getInBlock(){
        type_page.add(new HashMap<>());
    }
    void getOutofBlock(boolean have_Block_Type){
        if (have_Block_Type) block_Type.remove(block_Type.size() - 1);
        type_page.remove(type_page.size() - 1);
    }


    //check-exitcode
    boolean check(String name){
        if (exitcode.code != 0) {
            exitcode.name += " " + name;
            return false;
        }
        return true;
    }
    void get_error(int x, String str){
        exitcode.code = x;
        exitcode.name = str + " At";
    }

    //visitor-begin
    @Override
    void visit(Program pointer) {
        getInBlock(find_type("void"));
        for (int i = 0; i<pointer.list.size(); i++) {
            visit(pointer.list.get(i));
            if (exitcode.code != 0) return;
        }
        values.put(pointer,find_type("void"));
        getOutofBlock(true);
    }

    @Override
    void visit(Functional_Declaration pointer) {
        getInBlock(find_type(pointer.type));
       // System.out.printf(block_Type.get(block_Type.size()-1).type.name);
        String name = "Function " + pointer.name;
        if (!check(name)) return;
        Function_Type cur = (Function_Type)find_type(pointer.name).type;
        if (!check(name)) return;
        for (int i = 0 ; i < cur.parameter.size(); i++) {
        //    System.out.printf(cur.parameter_name.get(i)+"");
            add_type(cur.parameter_name.get(i), cur.parameter.get(i));
            if (compare_type(cur.parameter.get(i),"string")) pointer.parameter.get(i).is_str = true; else pointer.parameter.get(i).is_str = false;
            if (!check(name)) return;
        }
        is_function_block = true;
        visit(pointer.context);
        if (!check(name)) return;

        pointer.is_str = pointer.type.is_str;
        values.put(pointer,find_type(pointer.type));
        if (!check(name)) return;
        getOutofBlock(true);
    }

    @Override
    void visit(Class_Declaration pointer) {
    }

    @Override
    void visit(Selection_Statement pointer) {
        getInBlock();
        String name = "Selection_Statement ";
        if (!check(name)) return;
        visit(pointer.condition);
        if (!check(name)) return;
        if (!compare_type(values.get(pointer.condition),"bool")) {
            if (!check(name)) return;
            get_error(3,"");
        }
        if (!check(name)) return;
        visit(pointer.then_context);
        if (!check(name)) return;
        if (!(pointer.else_context instanceof Empty_Expression))
            visit(pointer.else_context);
        if (!check(name)) return;
        pointer.is_str = false;
        values.put(pointer,find_type("void"));
        if (!check(name)) return;
        getOutofBlock(false);
    }

    @Override
    void visit(Block_Statement pointer) {
        int p = 0;
        if (is_function_block) p = 1;
        if (p == 0) getInBlock();
        is_function_block = false;
        String name = "Block_Statement ";
        if (!check(name)) return;
        for (int i = 0 ; i < pointer.list.size(); i++) {
            visit(pointer.list.get(i));
            if (!check(name)) return;
        }
        pointer.is_str = false;
        values.put(pointer,find_type("void"));
        if (!check(name)) return;
        if (p == 0) getOutofBlock(false);

    }

    @Override
    void visit(For_Statement pointer) {
        getInBlock();
        String name = "For_Statement ";
        if (!check(name)) return;
        visit(pointer.initial);
        if (!check(name)) return;
        if (!(pointer.condition instanceof Empty_Expression)) {
            visit(pointer.condition);
            if (!check(name)) return;
            if (!compare_type(values.get(pointer.condition), "bool")) {
                if (!check(name)) return;
                get_error(3,"");
            }
            if (!check(name)) return;
        } else pointer.condition = new Bool_Expression(true);
        visit(pointer.update);
        if (!check(name)) return;
        break_Statement ++;
        visit(pointer.context);
        break_Statement --;
        if (!check(name)) return;
        pointer.is_str = false;
        values.put(pointer,find_type("void"));
        if (!check(name)) return;
        getOutofBlock(false);

    }

    @Override
    void visit(While_Statement pointer) {
        getInBlock();
        String name = "While_Statement ";
        if (!check(name)) return;
        visit(pointer.condition);
        if (!check(name)) return;
        if (!compare_type(values.get(pointer.condition), "bool")) {
            if (!check(name)) return;
            get_error(3, "");
        }
        if (!check(name)) return;
        break_Statement++;
        visit(pointer.context);
        break_Statement--;
        pointer.is_str = false;
        values.put(pointer,find_type("void"));
        if (!check(name)) return;
        getOutofBlock(false);
    }

    @Override
    void visit(Jump_Statement pointer) {
        if (break_Statement == 0){
            get_error(4,pointer.meaning);
        }
        if (!check("")) return;
        pointer.is_str = false;
        values.put(pointer,find_type("void"));
    }

    @Override
    void visit(Return_Statement pointer) {
        if (type_page.size() <= 1) {
            get_error(5, "");
        }
        String name = "Return Statement";
        if (!check(name)) return;
        visit(pointer.context);
        if (!check(name)) return;
        Actual_Type return_type = values.get(pointer.context);
        if (!compare_type(block_Type.get(block_Type.size() - 1),return_type)) {
            if (!check(name)) return;
            get_error(3, "");
        }
        if (!check(name)) return;
        pointer.is_str = false;
        values.put(pointer,find_type("void"));
    }

    @Override
    void visit(Argument pointer) {
        String name = pointer.name;
        if (pointer.have_initial_value) {
            visit(pointer.initial_value);
       //     System.out.printf(values.get(pointer.initial_value).type.name);
            if (!check(name)) return;
            if (pointer.initial_value instanceof Null_Expression){
                if (!(pointer.type.true_size > 0 || find_type(pointer.type).type instanceof Class_Type)){
                    if (!check(name)) return;
                    get_error(3,"Initial value");
                }
                if (!check(name)) return;
            } else
            if (!compare_type(find_type(pointer.type), values.get(pointer.initial_value))){
                if (!check(name)) return;
           //    System.out.printf(values.get(pointer.initial_value).type.name /*+ " " + find_type(pointer.type).type.name */ );
                get_error(3,"Initial value");
            }
            if (!check(name)) return;
        }
        lvalue = false;
        pointer.is_str = compare_type(pointer.type.name,"string");
        values.put(pointer,find_type(pointer.type));
    }

    @Override
    void visit(Declaration_Expression pointer) {
        String name = "Declaration Expression";
        for (int i = 0; i < pointer.member.size(); i++){
            visit(pointer.member.get(i));
            if (pointer.member.get(i).type.name.equals("void")) get_error(3,"void type ");
            if (!check(name)) return;
            add_type(pointer.member.get(i).name, find_type(pointer.member.get(i).type));
            if (!check(name)) return;
            //System.out.printf(pointer.member.get(i).name + " "+find_type(pointer.member.get(i).name).true_size);
        }
        lvalue = false;
        pointer.is_str = false;
        values.put(pointer,find_type("void"));
    }

    @Override
    void visit(Suffix_Expression pointer) {
        String name = "Suffix Expression";
        visit(pointer.expression);
        check_lvalue();
        if (!check(name)) return;
        if (!compare_type(values.get(pointer.expression),"int")){
            if (!check(name)) return;
            get_error(3,"");
        }
        if (!check(name)) return;
        pointer.is_str = false;
        values.put(pointer,find_type("int"));
        lvalue = true;
    }

    @Override
    void visit(Functional_Expression pointer) {
        String name = "Functional Expression";
        if (pointer.expression instanceof Identifier_Expression){
         //   System.out.printf("Yes\n");
            Type_Style cur;
            if (array_function){
                pointer.is_array = true;
                pointer.is_string = false;
                if (((Identifier_Expression) pointer.expression).name.equals("size") && pointer.argument.size() == 0) {
                    pointer.is_str = false;
                    values.put(pointer, find_type("int"));
                }
                else
                    get_error(2,((Identifier_Expression) pointer.expression).name);
                if (!check(name)) return;
                lvalue = false;
                return;
            } else {
                if (string_function) {
                    pointer.is_array = false;
                    pointer.is_string = true;
                    cur = find_type_in_string_map(((Identifier_Expression) pointer.expression).name);
                    //System.out.printf("SB!\n");
                    if (!check(name)) return;
                    if (!(cur instanceof Function_Type))
                        get_error(2, ((Identifier_Expression) pointer.expression).name);
                    if (!check(name)) return;
                    lvalue = false;
                } else {
                    pointer.is_array = false;
                    pointer.is_string = false;
                    cur = find_type_any(((Identifier_Expression) pointer.expression).name);
                    if (!check(name)) return;
                    if (!(cur instanceof Function_Type))
                        get_error(2, ((Identifier_Expression) pointer.expression).name);
                //    System.out.printf("SB1!\n");
                    if (!check(name)) return;
               //     System.out.printf("SB2!\n");
                    lvalue = false;
                }
                if (!check(name)) return;
             //   System.out.printf("SB3!\n");
                Function_Type cur_type = (Function_Type) cur;

                for (int i = 0; i < pointer.argument.size(); i++) {
                    boolean now_bool =string_function;
                    string_function = false;
                    visit(pointer.argument.get(i));
                    string_function = now_bool;
                    //   System.out.printf(values.get(pointer.argument.get(i)).type.name);
                    if (!check(name)) return;
                    if (!compare_type(cur_type.parameter.get(i), values.get(pointer.argument.get(i)))) {
                        if (!check(name)) return;
                        get_error(3, cur_type.name);
                    }
                    if (!check(name)) return;
                }
                pointer.is_str = compare_type(cur_type.return_Type,"string");

           //     System.out.printf(((Function_Type) cur).name + " "  + pointer.is_str + "\n");
                values.put(pointer, cur_type.return_Type);
            }
        }
        else{
       //     System.out.printf("SB!\n");
            get_error(5,"");
            if (!check(name)) return;
        }
        if (!check(name)) return;
    }

    @Override
    void visit(Subscript_Expression pointer) {
        String name = "Subscript Expression";
        visit(pointer.expression);
        //System.out.printf(make_new+"");
        if (!check(name)) return;
        if (make_new) {
            make_new = false;
            if (pointer.expression instanceof Identifier_Expression) {
                Actual_Type cur_type = new Actual_Type();
                cur_type.true_size = pointer.true_size;
                //System.out.printf(pointer.true_size + " " +  pointer.used_size + "");
                Identifier_Expression cur = (Identifier_Expression) pointer.expression;
                if (find_type_any(cur.name) instanceof Class_Type || find_type_any(cur.name) instanceof Default_Type) {
                    cur_type.type = find_type_any(cur.name).get_Virtual();
                    for (int i = 0; i < pointer.used_size; i++) {
                        visit(pointer.argument.get(i));
                        if (!check(name)) return;
                        if (!compare_type(values.get(pointer.argument.get(i)),"int")) get_error(3, "new");
                        if (!check(name)) return;
                    }
                } else get_error(8, "");
                pointer.is_str = compare_type(cur_type,"string");
                values.put(pointer, cur_type);
                lvalue = false;
            } else get_error(8, "");
        } else{
            visit(pointer.expression);
            if (!check(name)) return;
            //System.out.printf(values.get(pointer.expression).true_size+" " + pointer.true_size);
            if (pointer.used_size != pointer.true_size) get_error(3,"new");
            if (values.get(pointer.expression).true_size < pointer.used_size) get_error(3,"new");
            if (!check(name)) return;
            for (int i = 0;  i < pointer.used_size; i++){
                visit(pointer.argument.get(i));
                if (!check(name)) return;
                if (!compare_type(values.get(pointer.argument.get(i)),"int")) get_error(3, "new");
                if (!check(name)) return;
            }
            Actual_Type cur = new Actual_Type(values.get(pointer.expression));
            cur.true_size -= pointer.used_size;
            //       System.out.printf(cur.true_size+"");
            pointer.is_str = compare_type(cur,"string");
            values.put(pointer, cur);
            lvalue = true;
        }
    }

    @Override
    void visit(Member_Expression pointer) {
        String name = "Member Expression";
        visit(pointer.expression);
        pointer.val = values.get(pointer.expression);
        if (!check(name)) return;
        if (values.get(pointer.expression).true_size > 0) {
         //   System.out.printf("SB!\n");
            if (!check(name)) return;
            array_function = true;
            if (pointer.member instanceof Functional_Expression){
                visit(pointer.member);
            } else get_error(6,"");
            if (!check(name)) return;
            array_function = false;
            pointer.is_str = pointer.member.is_str;
            values.put(pointer,values.get(pointer.member));
            lvalue = false;
        } else
        if (compare_type(values.get(pointer.expression),"string")) {
         //   System.out.printf("SB!\n");
            if (!check(name)) return;
            string_function = true;
            if (pointer.member instanceof Functional_Expression)
                visit(pointer.member);
            else get_error(6,"");
            if (!check(name)) return;
            string_function = false;
            pointer.is_str = pointer.member.is_str;
            values.put(pointer,values.get(pointer.member));
            lvalue = false;
        }
        else{
            if (!check(name)) return;
            Type_Style cur = values.get(pointer.expression);
            Type_Style cur2 = new Actual_Type();
            if (cur.is_Virtual())
                cur2 = cur.get_Virtual();
            else get_error(6,"");
            if (!check(name)) return;
            if (pointer.member instanceof Identifier_Expression && cur2 instanceof Class_Type) {
            //    System.out.printf(cur.get_Actual().true_size+"");
                if (!check(name)) return;
                Class_Type cur_type = (Class_Type)cur2;
             //   System.out.printf((cur_type.parameter.get("key") == null) + "");
                if (cur_type.parameter.get(((Identifier_Expression) pointer.member).name) != null) {
                    pointer.is_str = compare_type(cur_type.parameter.get(((Identifier_Expression) pointer.member).name), "string");
                    values.put(pointer, cur_type.parameter.get(((Identifier_Expression) pointer.member).name));
                }
                else get_error(6,"");
                if (!check(name)) return;
            } else get_error(6,"");
            if (!check(name)) return;
            lvalue = true;
        }
    }

    @Override
    void visit(Prefix_Expression pointer) {
        String name = "Prefix Expression";
        if (pointer.oper.equals("new")){
            if (make_new) get_error(8,"");
        //    System.out.printf(make_new+"");
            if (!check(name)) return;
            make_new = true;
            visit(pointer.expression);
            if (!check(name)) return;
            make_new = false;
            if (pointer.expression instanceof Identifier_Expression) pointer.par_num = ((Class_Type)(find_type(((Identifier_Expression) pointer.expression).name).type)).par_num;
            pointer.is_str = pointer.expression.is_str;
            values.put(pointer,values.get(pointer.expression));
        }
        if (pointer.oper.equals("++") || pointer.oper.equals("--") ){
            visit(pointer.expression);
            if (!check(name)) return;
            check_lvalue();
            if (!check(name)) return;
            if (!compare_type(values.get(pointer.expression),"int")){
                if (!check(name)) return;
                get_error(3,pointer.oper);
            }
            if (!check(name)) return;
            pointer.is_str = pointer.expression.is_str;
            values.put(pointer,values.get(pointer.expression));
        }
        if (pointer.oper.equals("+") || pointer.oper.equals("-") || pointer.oper.equals("~")){
            visit(pointer.expression);
            if (!check(name)) return;
            if (!compare_type(values.get(pointer.expression),"int")){
                if (!check(name)) return;
                get_error(3,pointer.oper);
            }
            if (!check(name)) return;
            pointer.is_str = pointer.expression.is_str;
            values.put(pointer,values.get(pointer.expression));
        }
        if (pointer.oper.equals("!")){
            visit(pointer.expression);
            if (!check(name)) return;
            if (!compare_type(values.get(pointer.expression),"bool")){
                if (!check(name)) return;
                get_error(3,pointer.oper);
            }
            if (!check(name)) return;
            pointer.is_str = pointer.expression.is_str;
            values.put(pointer,values.get(pointer.expression));
        }
    }

    @Override
    void visit(Binary_Expression pointer) {
        String name = "Binary Expression";
        visit(pointer.left_exp);
        if (!check(name)) return;
        visit(pointer.right_exp);
        if (!check(name)) return;
        if (pointer.oper.equals("*") || pointer.oper.equals("/")
                || pointer.oper.equals("%") || pointer.oper.equals("<<") || pointer.oper.equals(">>")
                || pointer.oper.equals("&") || pointer.oper.equals("|") || pointer.oper.equals("^")) {
            if (!compare_type(values.get(pointer.left_exp),"int")){
                if (!check(name)) return;
                get_error(3,pointer.oper);
            }
            if (!check(name)) return;
            if (!compare_type(values.get(pointer.right_exp),"int")){
                if (!check(name)) return;
                get_error(3,pointer.oper);
            }
            if (!check(name)) return;
            pointer.is_str = false;
            values.put(pointer,find_type("int"));
        } else
        if (pointer.oper.equals("&&") || pointer.oper.equals("||") ){
            if (!compare_type(values.get(pointer.left_exp),"bool")){
                if (!check(name)) return;
                get_error(3,pointer.oper);
            }
            if (!check(name)) return;
            if (!compare_type(values.get(pointer.right_exp),"bool")){
                if (!check(name)) return;
                get_error(3,pointer.oper);
            }
            if (!check(name)) return;
            pointer.is_str = false;
            values.put(pointer,find_type("bool"));
        } else
        if (pointer.oper.equals("<") || pointer.oper.equals(">")  || pointer.oper.equals("+") ||pointer.oper.equals("-")  ||pointer.oper.equals("<=") || pointer.oper.equals(">=")){
            if (!compare_type(values.get(pointer.left_exp),values.get(pointer.right_exp))){
                if (!check(name)) return;
                get_error(3,pointer.oper);
            }
            if (!check(name)) return;
            if (!compare_type(values.get(pointer.left_exp),"string")
                    && !compare_type(values.get(pointer.left_exp),"int")) {
                if (!check(name)) return;
                get_error(3,pointer.oper);
            }
            if (!check(name)) return;
            if (pointer.oper.equals("+") ||pointer.oper.equals("-")) {
                pointer.is_str = pointer.left_exp.is_str;
                values.put(pointer, values.get(pointer.right_exp));
            }else {
                pointer.is_str = false;
                values.put(pointer, find_type("bool"));
            }
        } else
        if (pointer.oper.equals("==") || pointer.oper.equals("!=")){
            if (pointer.left_exp instanceof Null_Expression) {
                if (!(values.get(pointer.right_exp).true_size > 0 || values.get(pointer.right_exp).type instanceof Class_Type))
                    get_error(3, "");
                if (!check(name)) return;
            } else
            if (pointer.right_exp instanceof Null_Expression) {
                if (!(values.get(pointer.left_exp).true_size > 0 || values.get(pointer.left_exp).type instanceof Class_Type))
                    get_error(3, "");
                if (!check(name)) return;
            } else
            if (!compare_type(values.get(pointer.left_exp),values.get(pointer.right_exp))){
                if (!check(name)) return;
                get_error(3,pointer.oper);
            }
            if (!check(name)) return;
            pointer.is_str = false;
            values.put(pointer,find_type("bool"));
        }
        lvalue = false;
    }

    @Override
    void visit(Int_Expression pointer) {
        pointer.is_str = false;
        values.put(pointer,find_type("int"));
        lvalue = false;
    }

    @Override
    void visit(Bool_Expression pointer) {
        pointer.is_str = false;
        values.put(pointer,find_type("bool"));
        lvalue = false;
    }

    @Override
    void visit(String_Expression pointer) {
        pointer.is_str = true;
        values.put(pointer,find_type("string"));
        lvalue = false;
    }

    @Override
    void visit(Empty_Expression pointer) {
        pointer.is_str = false;
        values.put(pointer,find_type("void"));
        lvalue = false;
    }

    @Override
    void visit(Null_Expression pointer) {
        pointer.is_str = false;
        values.put(pointer,find_type("void"));
        lvalue = false;
    }

    @Override
    void visit(Type pointer) {
        if (find_type(pointer.name) == null)
            return;
        if (find_type(pointer.name).is_Virtual()) {
            Actual_Type t = make_type(pointer);
            pointer.is_str = compare_type(t, "string");
            values.put(pointer, t);
        }
        else
            get_error(2,pointer.name);
        lvalue = false;
    }

    @Override
    void visit(Identifier_Expression pointer) {
   //     System.out.printf(pointer.name + " " + find_type(pointer.name).type.name + "\n");
        pointer.is_str = compare_type(find_type(pointer.name),"string");
        values.put(pointer,find_type(pointer.name));
        lvalue = true;
    }

    @Override
    void visit(Assign_Expression pointer) {
        String name = "Assign Expression";
        visit(pointer.left_exp);
        if (!check(name)) return;
        check_lvalue();
        if (!check(name)) return;
        visit(pointer.right_exp);
        if (!check(name)) return;
        if (pointer.right_exp instanceof Null_Expression){
            if (!(values.get(pointer.left_exp).true_size > 0 || values.get(pointer.left_exp).type instanceof Class_Type))
                get_error(3,"");
            if (!check(name)) return;
        } else
        if (!compare_type(values.get(pointer.left_exp),values.get(pointer.right_exp))){
            if (!check(name)) return;
            get_error(3,"");
        }
        if (!check(name)) return;
        pointer.is_str = pointer.right_exp.is_str;
        values.put(pointer,values.get(pointer.right_exp));
    }
}

