grammar Complier;

//Comment

Line_Comment
	: '//' .*? '\n' -> skip
    ;

// Keywords

Kw_Bool : 'bool' ;
Kw_Int : 'int' ;
Kw_String : 'string' ;
Kw_Null : 'null' ;
Kw_Void : 'void' ;
fragment Kw_True : 'true' ;
fragment Kw_False : 'false' ;
Kw_If : 'if' ;
Kw_For : 'for' ;
Kw_While : 'While' ;
Kw_Break : 'break' ;
Kw_Continue : 'Continue' ;
Kw_Return : 'return' ;
Kw_New : 'new' ;
Kw_Class : 'class' ;

//Operators

//Constant

Bool : 'true' | 'false' ;




Add : '+' ;
Sub : '-' ;
Mul : '*' ;
Div : '/' ;
Mod : '%' ;

Less : '<' ;
Great : '>' ;
Leq : '<=' ;
Geq : '>=' ;
Equal : '==' ;
NotEqual : '!=' ;

And : '&' ;
Or : '|' ;
Tilde : '~' ;
Caret : '^' ;
LShift : '<<' ;
RShift : '>>' ;

Not : '!' ;
AndAnd : '&&' ;
OrOr : '||' ;

Assignment : '=' ;

SelfInc: '++' ;
SelfDec: '--' ;


Dot : '.';


LParen : '(' ;
RParen : ')' ;
LBracket : '[' ;
RBracket : ']' ;
LBrace : '{' ;
RBrace : '}' ;
//General

fragment Letter : [a-zA-Z_] ;
fragment Digit : [0-9] ;

Int : Digit+ ;
Identifier : Letter (Letter | Digit)* ;

Sign : '-' | '+';
String : '"' (ESC | .)*? '"' ;
ESC : '\\' [btnr"\\] ;

//Whitespace

WS : [ \t\n\r] -> skip ;


type : t = 'bool' | t = 'int' | t = 'string' | t = 'void' | Identifier;

//Program

program : (program_detail)* ;

program_detail :declaration | class_declaration | function_definition ;

declaration : type_specifier init_Identifiers ';' ;

class_declaration : 'class' Identifier '{' (noneinit_declaration)* '}' ;

noneinit_declaration : type_specifier noneinit_identifiers ';' ;

noneinit_identifiers : Identifier (',' Identifier)* ;

function_definition : type_specifier Identifier '(' parameters? ')' compound_statement ;

parameters : parameter (',' parameter)* ;

parameter : type_specifier Identifier ;

init_Identifiers : init_Identifier (',' init_Identifier)* ;
	
init_Identifier : Identifier ('=' expression)? ;

type_specifier : type (pair_Bracket)* ;

pair_Bracket : '[' ']' ;

//statement

statement 	: expression_statement
			| compound_statement
			| selection_statement
			| iteration_statement
			| jump_statement
			| declaration
			;

simple_statement : expression_statement | declaration;

expression_statement	: expression? ';' ;

compound_statement	: '{' statement* '}' ;

selection_statement	: 'if' '(' expression ')' a = statement ('else' b = statement)?	;

iteration_statement	: 'while' '(' expression ')' statement		#while_statement
					| 'for' '(' simple_statement expression_statement expression? ')' statement #for_statement
					;

jump_statement  : t = 'continue' ';'
				| t = 'break' ';'
				| t = 'return' expression? ';'
				;

//expression


expression  : '(' expression ')'	 #bracket_expression
            | expression t = ('++' | '--')	    #suffix_expression
            | expression '(' arguments? ')'	#functional_expression
            | expression bracket_clude_expression+ #subscript_expression
            | a = expression '.' b = expression 	#Member_expression
//
            | <assoc=right> t =('++' | '--' | '-' | '+' | '!' | '~' | 'new') expression	#prefix_expression
//
            | l = expression t =('*' | '/' | '%') r = expression	#binary_expression
            | l = expression t = ('+' | '-') r = expression			#binary_expression
            | l = expression t =('>>' | '<<') r = expression		#binary_expression
            | l = expression t = ('<' | '>' | '<=' | '>=') r = expression	#binary_expression
            | l = expression t =('==' | '!=') r = expression	#binary_expression
            | l = expression t = '&' r = expression				#binary_expression
            | l = expression t = '^' r = expression				#binary_expression
            | l = expression t = '|' r = expression				#binary_expression
            | <assoc=right>l = expression t = '&&' r = expression			#binary_expression
            | <assoc=right>l = expression t ='||' r = expression			#binary_expression
//
            | <assoc=right> l = expression t = '=' r = expression				#assign_expression
            | value									#value_expression
            ;
arguments: expression (',' expression)*	;
value       : Sign? Int		#int_value
            | Bool			#bool_value
            | String		#string_value
			| Kw_Null 		#null_value
            | Identifier	#identifier_value
            | type			#type_value
            ;
			
bracket_clude_expression :	('[' expression? ']');
			
// comma
//            | expression ',' expression

/*
expression	: assignment_expression	;

assignment_expression	: logical_or_expression
						| unary_expression assignment_operator assignment_expression
						;

assignment_operator: '=' ;

logical_or_expression: logical_and_expression ('||' logical_and_expression)*	;

logical_and_expression: inclusive_or_expression ('&&' inclusive_or_expression)*	;

inclusive_or_expression: exclusive_or_expression ('|' exclusive_or_expression)*	;

exclusive_or_expression: and_expression ('^' and_expression)*	;

and_expression: equality_expression ('&' equality_expression)*	;

equality_expression: relational_expression (equality_operator relational_expression)*	;

equality_operator: '==' | '!='	;

relational_expression: shift_expression (relational_operator shift_expression)*	;

relational_operator: '<' | '>' | '<=' | '>='	;

shift_expression: additive_expression (shift_operator additive_expression)*	;

shift_operator: '<<' | '>>'	;

additive_expression: multiplicative_expression (additive_operator multiplicative_expression)*	;

additive_operator: '+' | '-'	;

multiplicative_expression: cast_expression (multiplicative_operator cast_expression)*	;

multiplicative_operator: '*' | '/' | '%'	;

cast_expression: unary_expression
                | '(' type ')' cast_expression
				;

unary_expression: postfix_expression
                | '++' unary_expression
                | '--' unary_expression
                | unary_operator cast_expression
				;

unary_operator: 'new' | '+' | '-' | '~' | '!' ;

postfix_expression: primary_expression postfix* ;

postfix	: '[' expression ']'
		| '(' arguments? ')'
		| '.' Identifier
		| '++'
		| '--'
		;



primary_expression	: Bool
					| Int
					| String
					| '(' expression ')'
					|Identifier
					;
constant: Int
        | String
		| Kw_Null
		;*/