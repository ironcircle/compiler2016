// Generated from C:/Users/��/Desktop/ACM/������/zx_mx/src\Complier.g4 by ANTLR 4.5.1
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ComplierParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ComplierVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ComplierParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(ComplierParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(ComplierParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#program_detail}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram_detail(ComplierParser.Program_detailContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(ComplierParser.DeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#class_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_declaration(ComplierParser.Class_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#noneinit_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoneinit_declaration(ComplierParser.Noneinit_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#noneinit_identifiers}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoneinit_identifiers(ComplierParser.Noneinit_identifiersContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#function_definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_definition(ComplierParser.Function_definitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#parameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameters(ComplierParser.ParametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter(ComplierParser.ParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#init_Identifiers}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit_Identifiers(ComplierParser.Init_IdentifiersContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#init_Identifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit_Identifier(ComplierParser.Init_IdentifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#type_specifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_specifier(ComplierParser.Type_specifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#pair_Bracket}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPair_Bracket(ComplierParser.Pair_BracketContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(ComplierParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#simple_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimple_statement(ComplierParser.Simple_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#expression_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression_statement(ComplierParser.Expression_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#compound_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_statement(ComplierParser.Compound_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#selection_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelection_statement(ComplierParser.Selection_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code while_statement}
	 * labeled alternative in {@link ComplierParser#iteration_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_statement(ComplierParser.While_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code for_statement}
	 * labeled alternative in {@link ComplierParser#iteration_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_statement(ComplierParser.For_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#jump_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJump_statement(ComplierParser.Jump_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code binary_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinary_expression(ComplierParser.Binary_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functional_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctional_expression(ComplierParser.Functional_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Member_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMember_expression(ComplierParser.Member_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code value_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue_expression(ComplierParser.Value_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bracket_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBracket_expression(ComplierParser.Bracket_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code suffix_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSuffix_expression(ComplierParser.Suffix_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code subscript_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubscript_expression(ComplierParser.Subscript_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code prefix_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrefix_expression(ComplierParser.Prefix_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assign_expression}
	 * labeled alternative in {@link ComplierParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign_expression(ComplierParser.Assign_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#arguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArguments(ComplierParser.ArgumentsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code int_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInt_value(ComplierParser.Int_valueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bool_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBool_value(ComplierParser.Bool_valueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code string_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString_value(ComplierParser.String_valueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code null_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNull_value(ComplierParser.Null_valueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code identifier_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifier_value(ComplierParser.Identifier_valueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code type_value}
	 * labeled alternative in {@link ComplierParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_value(ComplierParser.Type_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link ComplierParser#bracket_clude_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBracket_clude_expression(ComplierParser.Bracket_clude_expressionContext ctx);
}